﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Board))]
public class BoardFactory : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if(GUILayout.Button("Create Board"))
        {
            Board b = (Board)target;

            b.Create(b.template);

            Repaint();
        }
    }

 
}

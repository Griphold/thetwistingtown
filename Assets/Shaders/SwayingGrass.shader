﻿// Upgrade NOTE: upgraded instancing buffer 'Props' to new syntax.

Shader "Custom/SwayingGrass" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_NormalMap("Normal Map", 2D) = "white" {}
		_NormalStrength("Normal Map Strength", Range(0, 50)) = 1 // strength of normal mapping
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0

		_Speed("MoveSpeed", Range(20, 50)) = 25 // speed of the swaying
		_Rigidness("Rigidness", Range(1, 50)) = 25 // lower makes it look more "liquid" higher makes it look rigid
		_SwayMax("Sway Max", Range(0, 0.1)) = .005 // how far the swaying goes
		_ZOffset("Y offset", float) = 0.5// z offset, below this is no animation
		_ShearVector("Shear Vector", Vector) = (0,0,0,0) //the vector to use for shearing
	}

	SubShader {
		Tags{ "RenderType" = "Opaque" "DisableBatching" = "True" }// disable batching lets us keep object space
		LOD 200
		Cull Off
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert addshadow

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _NormalMap;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		half _NormalStrength;
		fixed4 _Color;
		float4 _FootPos;

		float _Speed;
		float _SwayMax;
		float _ZOffset;
		fixed4 _ShearVector;
		float _Rigidness;

		void vert(inout appdata_full v)
		{
			float3 wpos = mul(unity_ObjectToWorld, v.vertex).xyz;// world position

			//Swaying
			float x = sin(wpos.x / _Rigidness + (_Time.x * _Speed)) *(v.vertex.z - _ZOffset) * 5;// x axis movements
			float y = sin(wpos.y / _Rigidness + (_Time.x * _Speed)) *(v.vertex.z - _ZOffset) * 5;// y axis movements

			v.vertex.x += step(0, v.vertex.z - _ZOffset) * (x * _SwayMax);// apply the movement if the vertex's z above the ZOffset
			v.vertex.y += step(0, v.vertex.z - _ZOffset) * (y * _SwayMax);

			//add Shear
			fixed3 shearLocal = mul(unity_WorldToObject, _ShearVector).xyz;

			v.vertex.x -= (v.vertex.z - _ZOffset) * shearLocal.x;
			v.vertex.y -= (v.vertex.z - _ZOffset) * shearLocal.y;
		}

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;

			//Normal Mapping
			fixed3 n = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex));
			n.xy *= _NormalStrength;
			o.Normal = normalize(n);

			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}

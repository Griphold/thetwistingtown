﻿Shader "Custom/ToonShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Ramp ("Toon Ramp", 2D) = "white" {}
		_SpecularPow("Shininess", Float) = 0.1
		_SpecularIntensity("Specular Intensity", Float) = 0.1
		_AmbientCol("Ambient Color", Color) = (1,1,1,1)
		_AmbientIntensity("Ambient Intensity", Float) = 0.1

		_OutlineCol("Outline Color", Color) = (0,0,0,1)
		_OutlineSize("Outline Size", Float) = 0.2
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		//#pragma surface surf Ramp fullforwardshadows
		#pragma surface surf Ramp noambient

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Ramp;

		float _SpecularPow;
		float _SpecularIntensity;

		
		fixed4 _AmbientCol;
		float _AmbientIntensity;

		fixed4 _OutlineCol;
		float _OutlineSize;

		half4 LightingRamp(SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
			
			//Outlines
			half NdotV = dot(s.Normal, viewDir);
			if (NdotV < _OutlineSize) return _OutlineCol;

			//Diffuse intensity
			half NdotL = dot(s.Normal, lightDir);

			//Specular intensity
			float3 halfDir = normalize(viewDir + lightDir);
			half NdotH = dot(s.Normal, halfDir);

			float spec = pow(NdotH, _SpecularPow);

			half intensity = ((1.0f - _SpecularIntensity)*NdotL + _SpecularIntensity*spec) * 0.5 + 0.5;
			half3 ramp = tex2D(_Ramp, float2(intensity, 0.5)).rgb;
			half4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * ramp * atten + _AmbientCol * _AmbientIntensity;
			c.a = s.Alpha;

			return c;
		}

		struct Input {
			float2 uv_MainTex;
		};

		fixed4 _Color;
		

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)


		void surf (Input IN, inout SurfaceOutput o) 
		{
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
		}
		ENDCG
	}
	FallBack "Diffuse"
}

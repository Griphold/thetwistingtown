﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// forwards events to consumer scripts so those scripts can be placed on objects not hit by raycasts.
/// Should be placed behind UI elements so those can intercept events first.
/// </summary>
public class EventForwarder : MonoBehaviour, IScrollHandler, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{

    // set in Start() using GO.Find()
    private BirdsEyeCamera birdsEyeCamera;
    private Board board;

    private bool isMouseAvailableForGameWorld = false;
    /// <summary>
    /// returns true if the mouse is free to interact with the game elements.
    /// false if it's off screen or interacting with screen UI elements.
    /// </summary>
    public bool IsMouseAvailableForGameWorld { get { return isMouseAvailableForGameWorld; } }

    void Start()
    {
        if (birdsEyeCamera == null)
            birdsEyeCamera = GameObject.FindObjectOfType<BirdsEyeCamera>();

        if (board == null)
            board = FindObjectOfType<Board>();

        board.eventForwarder = this;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        // first send event to build manager, then to player input.
        // note that build manager may release the input context in response to the event
        // so in that case both it and playerInput react to the same event.
        // if more than 2 scripts need these events we can talk about a more general approach.
        BuildManager.Instance.ReceivePointerDownEvent(eventData.button);
        PlayerInput.Instance.ReceivePointerDownEvent(eventData.button);
        board.ReceivePointerDownEvent(eventData.button);


    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isMouseAvailableForGameWorld = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isMouseAvailableForGameWorld = false;
    }

    public void OnScroll(PointerEventData eventData)
    {
        birdsEyeCamera.ReceiveScrollEvent(eventData.scrollDelta);
    }


    public interface ManagedPointerDownReceiver
    {
        /// <summary>
        /// invoked if an OnPointerDownEvent reaches the forwarder. Returns true if the receiver consumes the event.
        /// </summary>
        /// <param name="button">identifies the button that is pressed down</param>
        /// <returns>true if the event was consumed</returns>
        bool ReceivePointerDownEvent(PointerEventData.InputButton button);
    }

    public interface ManagedScrollEventReceiver
    {
        /// <summary>
        /// invoked if a ScrollEvent reaches the forwarder. Returns true if the receiver consumes the event.
        /// </summary>
        /// <param name="scrollDelta">specifies the direction and amount (=> -1, 0, 1) of scroll.</param>
        /// <returns>true if the event was consumed</returns>
        bool ReceiveScrollEvent(Vector2 scrollDelta);
    }

    public interface ManagedPointerEnterReceiver
    {
        void OnPointerEnter(PointerEventData eventData);
    }

    public interface ManagedPointerExitReceiver
    {
        void OnPointerExit(PointerEventData eventData);
    }
}

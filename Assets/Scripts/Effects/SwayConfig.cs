﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SwayConfig", menuName ="TheTwistingTown/SwayConfig", order = 1) ]
public class SwayConfig : ScriptableObject {
    public float Mass = 10f;

    public float DisplacementMax = 0.65f;
    public float Damping = 3f;
    public float MaxTimeStepMultiplier = 2f;
    public float MovementForNormalTimeStep = 5f;
    
    public float MovementForce = 400f;
    public float SpringForce = 300f;
}

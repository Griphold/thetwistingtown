﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class Swaying : MonoBehaviour {
    public bool Active;
    [Header("Properties")]
    public SwayConfig config;
    public Transform SwayDirection;

    private float displacement = 0;
    private float velocity = 0;
    private float force = 0;

    private Renderer swayRenderer;
    private Vector3 lastFramePosition;

	// Use this for initialization
	void Start () {
        swayRenderer = GetComponent<Renderer>();
        lastFramePosition = transform.position;
    }
	
	// Update is called once per frame
	void Update () {

        PerformTimestep(Time.deltaTime);
        
        SetShaderVariables(displacement * SwayDirection.right);
        
        velocity -= velocity * Time.deltaTime * config.Damping;

        lastFramePosition = transform.position;
    }

    void PerformTimestep(float t)
    {
        //force from moving
        Vector3 movementSinceLastFrame = transform.position - lastFramePosition;
        float projectedLength = Vector3.Dot(movementSinceLastFrame, SwayDirection.right);

        //make sure a minimum force is always applied
        float timeMultiplier = Mathf.Lerp(config.MaxTimeStepMultiplier, 1, projectedLength / config.MovementForNormalTimeStep);
        t *= timeMultiplier;
        
        force += projectedLength * config.MovementForce;

        //spring force back to origin
        force += -displacement * config.SpringForce;

        velocity += t * force / config.Mass;
        //velocity = Mathf.Clamp(velocity,-5f, 5f);

        displacement += t * velocity;
        if(displacement < -config.DisplacementMax)
        {
            displacement = -config.DisplacementMax;
            velocity = 0;
        }
        else if(displacement > config.DisplacementMax)
        {
            displacement = config.DisplacementMax;
            velocity = 0;
        }

        force = 0;
    }

    private void SetShaderVariables(Vector3 movementDirection)
    {
        swayRenderer.material.SetVector("_ShearVector", new Vector4(movementDirection.x, movementDirection.y, movementDirection.z, 0));
    }
}

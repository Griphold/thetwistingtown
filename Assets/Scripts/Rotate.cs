﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public float Speed = 5.0f;
    public Vector3 axis = Vector3.up;

	
	// Update is called once per frame
	void Update ()
    {
        transform.Rotate(axis * Speed * Time.deltaTime, Space.Self);
		
	}
}

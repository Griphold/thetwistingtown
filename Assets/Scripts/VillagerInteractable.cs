﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface VillagerInteractable {
    void OnInteract(Villager villager);
    bool IsAssigned();
    void Assign(Villager villager);
}

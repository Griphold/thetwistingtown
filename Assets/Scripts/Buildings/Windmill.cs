﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Windmill : Building, RingListener
{
    [System.Serializable]
    public class RingProductivity
    {
        public float ProductivityAtMinimum = 2;
        public float ProductivityAtMaximum = 4;

        public float GetProductivity(float t)
        {
            if (t <= 0)
                return 0;

            return Mathf.Lerp(ProductivityAtMinimum, ProductivityAtMaximum, t);
        }
    }

    [SerializeField]
    private int WorkerLimit = 1;
    [SerializeField]
    private WindmillUI UI;

    [Header("Productivity")]
    public int MinimumProductivityOverall = 1;
    public List<RingProductivity> ProductivitiesPerRing = new List<RingProductivity>();
    private float BonusProductivity = 0;
    private RingProductivity currentProductivity = new RingProductivity();

    [Header("Animation")]
    public GameObject Blades;
    [Tooltip("The speed when Bonus Productivity reaches 1. Twice this speed when Bonus Productivity reaches 2 and so on.")]
    public float BaseBladeSpeed;
    public float AccelerationDuringRotationPhase = 10;
    public float Brakingspeed = 2;
    private float targetRotationSpeed = 0;
    private float currentRotationSpeed = 0;
    private bool isRotationPhase;
    private float acceleration = 1;

    [Tooltip("Locations at which Villagers will farm.")]
    public Transform[] AnimationSpots;

    void Update()
    {
        currentRotationSpeed = Mathf.Lerp(currentRotationSpeed, targetRotationSpeed, acceleration * Time.deltaTime);
        Blades.transform.Rotate(0, currentRotationSpeed * Time.deltaTime, 0);
    }

    public override WorldSpaceUI GetUI()
    {
        return UI;
    }

    public override void OnCompletedConstruction()
    {
        base.OnCompletedConstruction();
        Initialize();
    }

    public override void OnEvaluateBuildingAction()
    {
        int remainingSpace = Inventory.RemainingSpace(Resource.WHEAT);
        if(remainingSpace == 0)
        {
            // windmill is already full. show notification.
            InventoryFullPopup.Create(Inventory.GetPopupSpawnPosition());
        }

        List<Villager> potentialMillers = OwnCell.GetRegisteredVillagers();
        List<Transform> spots = new List<Transform>(AnimationSpots);

        int jobsToAssign = Mathf.Min(remainingSpace, potentialMillers.Count, WorkerLimit);

        for (int i = 0; i < jobsToAssign; i++)
        {
            if (spots.Count < 1) { Debug.LogWarning(gameObject.name + ": Not enough Animation Spots!"); break; }

            Transform spot = spots[Random.Range(0, spots.Count)];
            spots.Remove(spot);

            Villager.AnimationItem farmAnimation = new Villager.AnimationItem(this, "Farm", spot.position, spot.rotation, null, AddWheat);
            potentialMillers[i].QueueAnimation(farmAnimation);
        }
    }

    public void AddWheat(Villager villager, VillagerInteractable interactable)
    {
        int wheatProduced = Mathf.RoundToInt(GetCurrentProductivity());

        if(wheatProduced < MinimumProductivityOverall)
        {
            wheatProduced = MinimumProductivityOverall;
        }

        Inventory.Add(Resource.WHEAT, wheatProduced);

        if(Inventory.RemainingSpace(Resource.WHEAT) == 0)
        {
            // windmill was just filled up. show notification
            StartCoroutine(InventoryFullPopup.CreateDelayedCoroutine(Inventory.GetPopupSpawnPosition()));
        }
    }

    public override TypeID Type()
    {
        return Building.TypeID.Windmill;
    }

    public float GetCurrentProductivity()
    {
        return MinimumProductivityOverall + BonusProductivity;
    }

    public float GetMaximumProductivity()
    {
        return MinimumProductivityOverall + currentProductivity.ProductivityAtMaximum;
    }

    public float GetMinimumProductivity()
    {
        return MinimumProductivityOverall;
    }

    public float GetBonusProductivity()
    {
        return BonusProductivity;
    }

    public void OnRingTurned(float degreesClockwise)
    {
        BonusProductivity =  currentProductivity.GetProductivity(OwnCell.Ring.TurnAmountToReachMaximum());

        //update animation stuff
        targetRotationSpeed = BonusProductivity * BaseBladeSpeed;
        acceleration = AccelerationDuringRotationPhase;

        UI.UpdateUIElements();
    }

    public override void OnEnterRotationState()
    {
        BonusProductivity = 0;
        targetRotationSpeed = 0;
        acceleration = Brakingspeed;
    }

    public void OnRingReset()
    {
        BonusProductivity = 0;
        targetRotationSpeed = 0;
        acceleration = Brakingspeed;
    }

    private void Initialize()
    {
        OwnCell.Ring.AddListener(this);

        if (OwnCell.Ring.RingIndex > ProductivitiesPerRing.Count)
        {
            Debug.LogError("Windmill has no productivity setting registered for this ring. Using default: 2 - 4");
        }
        else
        {
            currentProductivity = ProductivitiesPerRing[OwnCell.Ring.RingIndex];
        }
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
        if(OwnCell && OwnCell.Ring)
            OwnCell.Ring.RemoveListener(this);
    }

    //Serialization
    public override void LoadData()
    {
        base.LoadData();

        Initialize();
    }
}

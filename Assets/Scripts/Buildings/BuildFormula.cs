﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "Build Formula", menuName = "TheTwistingTown/Build Formula", order = 1)]
public class BuildFormula : ScriptableObject{

    /// <summary>
    /// type of building this formula is for
    /// </summary>
    public Building.TypeID type;
    public Building completedBuilding;

    public string displayName;
    public string description;

    public List<ResourceRequirement> neededResources = new List<ResourceRequirement>();

    /// <summary>
    /// number of end-of-turn-actions required to build this
    /// </summary>
    public int requiredActions;
}

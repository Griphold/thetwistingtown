﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Storage : Building {

    [SerializeField]
    private StorageUI UI;


    public override WorldSpaceUI GetUI()
    {
        return UI;
    }

    public override void OnCompletedConstruction()
    {
        base.OnCompletedConstruction();
    }

    public override void OnEvaluateBuildingAction()
    {
        
    }

    public override TypeID Type()
    {
        return TypeID.Storage;
    }

}

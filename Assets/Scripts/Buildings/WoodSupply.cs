﻿using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodSupply : NaturalResourceSupply
{
    public List<Tree> Trees;

    public override TypeID GetTypeID()
    {
        return NaturalResourceSupply.TypeID.Forest;
    }

    protected override void Start()
    {
        //initialize inventory
        Inventory i = GetInventory();
        i.CreateNewSlot(Trees.Count, Resource.WOOD);

        base.Start();

        LoadData();
    }

    public int GetAmount()
    {
        return GetInventory().GetTotal(Resource.WOOD);
    }

    protected override void Refill()
    {
        GetInventory().Add(Resource.WOOD, Trees.Count, false);

        foreach (Tree tree in Trees)
        {
            tree.Grow();
        }
    }

    public List<Tree> GetChoppableTrees()
    {
        return Trees.FindAll(x => x.IsGrown && !x.IsAssigned());
    }

    public void OnChopTree(Tree tree)
    {
        GetInventory().Remove(Resource.WOOD, 1);
    }

    public override bool IsDepleted()
    {
        return GetInventory().IsEmpty();
    }

    //Serialisation
    public override void OnSave()
    {
        base.OnSave();
        SaveData.Add("Amount", GetAmount());
        if (!gameObject.activeSelf)
        {
            Debug.Log(SaveData);
        }
    }

    public override void LoadData()
    {
        if (SaveData == null)
            return;

        base.LoadData();
        int amount = (int) SaveData["Amount"];

        GetInventory().Clear();
        GetInventory().Add(Resource.WOOD, amount, false);

        for (int i = 0; i < Trees.Count - amount; i++)
        {
            Trees[i].UnGrow();
        }
    }
}

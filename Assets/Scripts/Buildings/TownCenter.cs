﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownCenter : Building {

    public TownCenterUI UI;

    public override WorldSpaceUI GetUI()
    {
        return UI;
    }

    public override void OnCompletedConstruction()
    {
    }

    public override void OnEvaluateBuildingAction()
    {
    }

    public override TypeID Type()
    {
        return Building.TypeID.TownCenter;
    }
}

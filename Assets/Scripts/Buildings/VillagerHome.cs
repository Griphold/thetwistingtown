﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillagerHome : Building
{
    [SerializeField]
    private VillagerHomeUI UI;
    [SerializeField]
    private Transform VillagerSpawnPoint;
    [SerializeField]
    private Villager VillagerPrefab;
    public Villager MyVillager;

    public override void OnEvaluateBuildingAction()
    {
       
    }

    public override TypeID Type()
    {
        return TypeID.VillagerHome;
    }

    public override void OnEnterCommandState()
    {
    }

    public override void OnExitCommandState()
    {
    }

    public override void OnEnterRotationState()
    {
    }

    public override void OnExitRotationState()
    {
    }

    public override WorldSpaceUI GetUI()
    {
        return UI;
    }

    public override void OnCompletedConstruction()
    {
        base.OnCompletedConstruction();
        //create a villager
        if(MyVillager == null)
        {
            Villager v = Instantiate(VillagerPrefab, VillagerSpawnPoint.position, VillagerSpawnPoint.rotation);
            MyVillager = v;
            v.home = this;
            
        }

        TownManager.Instance.AddVillager(MyVillager);
        MyVillager.lockAndDeactivateAgent();
    }

    public Transform GetVillagerSpawnPoint()
    {
        return VillagerSpawnPoint;
    }

    //serialisation
    public override void OnSave()
    {
        base.OnSave();
        SaveData.Add("VillagerID", MyVillager.ID);
    }

    public override void LoadData()
    {
        base.LoadData();
        int villagerID = (int)SaveData["VillagerID"];
        MyVillager = TownManager.Instance.GetVillager(villagerID);
        MyVillager.home = this;
    }
}

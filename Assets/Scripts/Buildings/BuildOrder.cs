﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BuildOrder : Building
{
    public UnityEvent<Building> OnCompleted;


    [SerializeField]
    private BuildOrderUI UI;

    [SerializeField]
    private BuildFormula formula;
    /// <summary>
    /// construction formula of the building this order is for
    /// </summary>
    public BuildFormula Formula
    {
        get { return formula; }
        set { formula = value; }
    }
    
    private bool startedConstruction;
    private int actionsTaken = 0;
    public int ActionsTaken { get { return actionsTaken; } }

    [Header("Animation")]
    public GameObject CompleteEffect;

    [Tooltip("Locations at which Villagers will build.")]
    public Transform[] AnimationSpots;

    protected override void OnStart()
    {
        if (SaveData == null)
        {
            //set up inventory
            foreach (ResourceRequirement r in Formula.neededResources)
            {
                Inventory.CreateNewSlot(r.amount, r.type);
            }

            //player shouldnt be able to take out resource once put in to the build order
            for (int i = 0; i < Inventory.GetNumberOfSlots(); i++)
            {
                Inventory.SetDraggable(i, false);
            }
        }

        UI.Initialize();

        // lumber-check:
        // if no resources are needed start construction immediately
        if (Formula.neededResources.Count == 0)
        {
            startedConstruction = true;
        }


        if (startedConstruction)
        {
            UI.OnStartConstruction();
        }

        SFX.PlayAtPosition(SFXCategory.Buildorder_Appear, transform.position);
    }

    public override WorldSpaceUI GetUI()
    {
        return UI;
    }

    public override TypeID Type()
    {
        return TypeID.BuildOrder;
    }

    public override void OnEvaluateBuildingAction()
    {
        //if resources present and at least 1 villager is present
        List<Villager> villagers = OwnCell.GetRegisteredVillagers();
        if (!startedConstruction && Inventory.IsFull() && villagers.Count >= 1)
        {
            //then start building
            Inventory.Clear(); //consume resources

            startedConstruction = true;
            UI.OnStartConstruction();
        }

        if(startedConstruction)
        {
            int villagersAssigned = 0;
            int actionsNeeded = actionsUntilComplete();

            List<Transform> spots = new List<Transform>(AnimationSpots);

            foreach (Villager villager in villagers)
            {
                //only assign as many villagers as actions needed
                if (actionsNeeded - villagersAssigned <= 0)
                    break;

                if (spots.Count < 1) { Debug.LogWarning(gameObject.name + ": Not enough Animation Spots!"); break; }

                Transform spot = spots[Random.Range(0, spots.Count)];
                spots.Remove(spot);

                Villager.AnimationItem animation = new Villager.AnimationItem(this, "Build", spot.position, spot.rotation, null, Construct);

                villager.QueueAnimation(animation);
                villagersAssigned++;
            }
        }
    }

    private void Construct(Villager villager, VillagerInteractable interactable)
    {
        actionsTaken++;
        UI.UpdateUIElements();

        if(actionsUntilComplete() <= 0)
        {
            Complete();
        }
    }

    private int actionsUntilComplete()
    {
        return formula.requiredActions - actionsTaken;
    }

    private void Complete()
    {
        // deselect if necessary
        if(isSelected)
        {
            PlayerInput.Instance.Deselect();
        }
        // close UI if necessary
        UI.HideViaManager();
        
        // those two checks prevent errors when other systems try to do stuff after Destroy

        Instantiate(CompleteEffect, transform.position, CompleteEffect.transform.rotation);

        //set the building
        Building b = BuildManager.Instance.SetBuilding(formula.type, OwnCell);

        if (OnCompleted != null)
        {
            OnCompleted.Invoke(b);
        }

        Destroy(gameObject);
    }

    public override void OnCompletedConstruction()
    {
        base.OnCompletedConstruction();
    }

    //Serialization
    public override void OnSave()
    {
        base.OnSave();

        SaveData.Add("ActionsTaken", actionsTaken);
        SaveData.Add("StartedConstruction", startedConstruction);
        SaveData.Add("BuildFormula", (int)formula.type);
    }

    public override void LoadData()
    {
        base.LoadData();

        actionsTaken = (int)SaveData["ActionsTaken"];
        startedConstruction = (bool)SaveData["StartedConstruction"];
        formula = BuildManager.Instance.formulaDict[(Building.TypeID)(int)SaveData["BuildFormula"]];
    }
}

﻿using System.Collections.Generic;
using UnityEngine;

public class Lumberjack : Building, Board.IBoardListener
{
    [SerializeField]
    private float Range = 10;
    [SerializeField]
    private int WorkerLimit = 1;
    [SerializeField]
    private LumberjackUI ui;

    protected override void OnStart()
    {
    }

    public override WorldSpaceUI GetUI()
    {
        return ui;
    }

    public override void OnEvaluateBuildingAction()
    {
        List<Villager> mahBois =  OwnCell.GetRegisteredVillagers();
        
        int remainingSpace = Inventory.RemainingSpace(Resource.WOOD);
        if (remainingSpace == 0)
        {
            // lumberjack is already full. show notification.
            InventoryFullPopup.Create(Inventory.GetPopupSpawnPosition());
        }

        List<Tree> mahTrees = GetTreesInRange();
        //only assign one villager to an available tree
        //limit number of workers
        int jobsToAssign = Mathf.Min(WorkerLimit, mahBois.Count, mahTrees.Count, remainingSpace);
        
        for (int i = 0; i < jobsToAssign; i++)
        {
            mahTrees = GetTreesInRange();

            Transform t = mahTrees[i].transform;

            Vector3 animPos = t.position - t.transform.forward;
            Quaternion animRot = Quaternion.LookRotation(t.position - animPos, mahBois[i].transform.up);

            Villager.AnimationItem chopAnimation = new Villager.AnimationItem(this, "Chop", animPos, animRot, mahTrees[i], ChopWood);
            Villager.AnimationItem addWood = new Villager.AnimationItem(this, "NONE", animPos, animRot, null, AddWood);//todo determine add wood position

            mahBois[i].QueueAnimation(chopAnimation, addWood);
            mahTrees[i].Assign(mahBois[i]);
        }
    }

    private void ChopWood(Villager villager, VillagerInteractable interactable)
    {
        if (interactable != null)
        {
            interactable.OnInteract(villager);
            ui.UpdateUIElements();
        }
    }

    private void AddWood(Villager villager, VillagerInteractable interactable)
    {
        Inventory.Add(Resource.WOOD, 1);

        if (Inventory.RemainingSpace(Resource.WOOD) == 0)
        {
            // lumberjack was just filled up. show notification
            StartCoroutine(InventoryFullPopup.CreateDelayedCoroutine(Inventory.GetPopupSpawnPosition()));
        }
    }

    public override TypeID Type()
    {
        return TypeID.Lumberjack;
    }

    /*
    public void OnDrawGizmos()
    {
        base.OnDrawGizmos();

        Gizmos.color = new Color(0,1,0, 0.75f);
        Gizmos.DrawSphere(transform.position, Range);
    }*/

    public override void OnCompletedConstruction()
    {
        base.OnCompletedConstruction();
        Initialize();
    }

    public List<Tree> GetTreesInRange()
    {
        //get all forests in range
        List<WoodSupply> woodSupplies = TownManager.Instance.GetNaturalResourceSupplies<WoodSupply>(transform.position, Range);
        List<Tree> mahTrees = new List<Tree>();
        if (woodSupplies.Count <= 0) return mahTrees;

        //get all trees
        foreach (WoodSupply woodSupply in woodSupplies)
        {
           mahTrees.AddRange(woodSupply.GetChoppableTrees());
        }

        return mahTrees;
    }

    public void OnAnyRingChanged()
    {
        ui.UpdateUIElements();
    }

    private void Initialize()
    {
        if(ui)
            TownManager.Instance.gameManager.board.AddListener(this);
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
        if(TownManager.Instance)
            TownManager.Instance.gameManager.board.RemoveListener(this);
    }

    public override void LoadData()
    {
        base.LoadData();
        Initialize();
    }
}

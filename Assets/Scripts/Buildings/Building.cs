﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Events;

public abstract class Building : MonoBehaviour, GameStateListener, Selectable, Attackable, TwistingSerializable {

    public delegate void OnVillagerCompleteAction(Villager villager, VillagerInteractable interactable);
    public UnityEvent OnSelectBuilding;

    public enum TypeID
    {
        // non-buildables
        Forest,
        DestroyedBuilding,
        TownCenter,
        WallSide,

        // buildables
        BuildOrder,
        VillagerHome,
        Windmill,
        Lumberjack,

        Storage,
        Bakery,
        StoneMine,
        Well,
        Tower,
        WallCenter,
        Bridge
    };

    [Header("Health")]
    public int MaxHealth = 1;

    /// <summary>
    /// health the building has left. Building is destroyed if this falls to 0
    /// </summary>
    public int currentHealth = 1;

    /// <summary>
    /// reference to the health display on the building's UI.
    /// this is NOT a separate UI window.
    /// </summary>
    [SerializeField]
    protected HealthUI healthUI;

    /// <summary>
    /// identifies what type of building this is
    /// </summary>
    public abstract TypeID Type();

    protected bool isSelected = false;

    protected Cell cell;
    /// <summary>
    /// reference to the cell on which the building stands
    /// </summary>
    public Cell OwnCell
    {
        get { return cell; }
        set { cell = value; }
    }

    public bool Demolishable = true;


    [SerializeField]
    public Inventory Inventory;

    [Header("Highlighting")]
    public Color HighlightedColor = Color.yellow;
    public Renderer[] RenderersToHighlight;

    void Start()
    {
        if(Inventory)
            Inventory.Owner = this.gameObject;

        healthUI.Initialize(MaxHealth);
        healthUI.SetDisplayedHealth(currentHealth);

        OnStart();
    }

    protected virtual void OnStart()
    {

    }
    
    protected void OnDrawGizmos()
    {
        if(isSelected)
        {
            Bounds b = GetComponent<Collider>().bounds;

            Gizmos.color = new Color(1, 0, 0, .7f);
            Gizmos.DrawCube(b.center, b.extents * 2);
        }
    }

    public virtual void OnCompletedConstruction()
    {
        StartCoroutine("MoveActorToNavMeshAfterNavMeshUpdate");
    }

    private IEnumerator MoveActorToNavMeshAfterNavMeshUpdate()
    {
        yield return new WaitForSeconds(GetComponent<UnityEngine.AI.NavMeshObstacle>().carvingTimeToStationary + 0.05f);
        foreach (Villager v in TownManager.Instance.AllVillagers)
        {
            v.GetAgent().enabled = true;
            UnityEngine.AI.NavMeshHit hit;
            UnityEngine.AI.NavMesh.SamplePosition(v.GetAgent().transform.position, out hit, 3, UnityEngine.AI.NavMesh.AllAreas);
            v.GetAgent().transform.position = hit.position;
        }
        foreach (Enemy v in GlobalEnemySpawner.Instance.GetEnemies(this.transform.position, 10, false))
        {
            v.GetAgent().enabled = true;
            UnityEngine.AI.NavMeshHit hit;
            UnityEngine.AI.NavMesh.SamplePosition(v.GetAgent().transform.position, out hit, 3, UnityEngine.AI.NavMesh.AllAreas);
            v.GetAgent().transform.position = hit.position;
        }
    }
    public virtual void OnEnterCommandState()
    {
    }

    public virtual void OnExitCommandState()
    {
    }

    /// <summary>
    /// Entrance point for end-of-turn actions.
    /// Buildings should look for workers on their cell 
    /// and if there are any perform a type-specific action.
    /// </summary>
    public abstract void OnEvaluateBuildingAction();

    public void OnEvaluateEnemyAction()
    {
    }

    public virtual void OnEnterRotationState()
    {
    }

    public virtual void OnExitRotationState()
    { 
    }

    public virtual void OnSelect()
    {
        foreach (Renderer r in RenderersToHighlight)
        {
            if (r != null)
            {
                r.material.SetColor("_RimColor", HighlightedColor);
                r.material.SetFloat("_RimStrength", 1.0f);
            }
        }

        isSelected = true;

        SFX.PlayAtPosition(SFXCategory.Building_Select, transform.position);

        if (OnSelectBuilding != null)
        {
            OnSelectBuilding.Invoke();
        }
    }

    public virtual void OnDeselect()
    {
        foreach (Renderer r in RenderersToHighlight)
        {
            if (r != null)
            {
                r.material.SetColor("_RimColor", Color.black);
                r.material.SetFloat("_RimStrength", 0.0f);
            }
        }

        isSelected = false;
    }

    public bool IsSelected()
    {
        return isSelected;
    }

    public virtual bool IsBusy()
    {
        return false;
    }

    public abstract WorldSpaceUI GetUI();

    public virtual void OnDestroy()
    {
        if (isSelected && PlayerInput.Instance != null)
            PlayerInput.Instance.Deselect();

        if(GetUI() != null)
            DestroyImmediate(GetUI().gameObject);

        if (TownManager.Instance != null) { TownManager.Instance.gameManager.RemoveListener(this); }
    }

    public void OnDemolished()
    {
        GameObject demolishEffect = Resources.Load<GameObject>("Effects/PuffDemolish");
        Instantiate(demolishEffect, transform.position, demolishEffect.transform.rotation);
    }


    #region Attackable stuff

    public int GetHealth()
    {
        return currentHealth;
    }

    public bool Attack(int damage)
    {
        if (damage <= 0)
            return false;

        int healthBeforeDamage = currentHealth;
        currentHealth -= damage;
        if (currentHealth < 0)
            currentHealth = 0;
        int delta = healthBeforeDamage - currentHealth;

        OnHealthChanged(-delta);
        return currentHealth <= 0 && delta > 0;
    }

    public bool isAlive()
    {
        return currentHealth > 0;
    }

    public void OnHealthChanged(int delta)
    {
        Vector2 randomModifier = Random.insideUnitCircle;
        ResourcePopup.CreateResourcePopup(Resource.HEALTH, delta, GetUI().GetPreferredPositionWorldSpace() + new Vector3(randomModifier.x, 0, randomModifier.y));

        if (!isAlive())
        {
            OwnCell.DemolishBuilding();
        }
        else
        {
            healthUI.SetDisplayedHealth(currentHealth);
        }
    }
    #endregion

    //Serialisation

    public JObject SaveData { get; set; }

    public virtual void OnSave()
    {
        SaveData = new JObject();
        SaveData.Add("TypeID", (int) Type());
        SaveData.Add("RingID", OwnCell.Ring.RingIndex);
        SaveData.Add("CellID", OwnCell.CellIndex);
        SaveData.Add("CurrentHealth", currentHealth);
        SaveData.Add("MaximumHealth", MaxHealth);

        if (Inventory)
        {
            Inventory.OnSave();
            SaveData.Add("Inventory", Inventory.SaveData);
        }
    }

    public virtual void LoadData()
    {
        if (SaveData == null)
            return;

        currentHealth = (int) SaveData["CurrentHealth"];
        MaxHealth = (int) SaveData["MaximumHealth"];

        if(SaveData.ContainsKey("Inventory"))
        {
            Inventory.SaveData = (JObject) SaveData["Inventory"];
            Inventory.LoadData();
        }
    }
}

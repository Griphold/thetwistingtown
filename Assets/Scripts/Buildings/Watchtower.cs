﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Watchtower : Building
{
    [Header("Watchtower Properties")]
    public Transform RotatingTransform;
    public MeshRenderer AttackRangeRenderer;
    public MeshFilter AttackRangeMesh;
    public float RotationMultiplier = 1;
    [Range(1, 180)]
    public float FieldOfViewAngle = 110f;
    public float Range = 20f;
    public int AttackDamage = 1;
    public WatchTowerUI ui;

    [Header("Arrow Effects")]
    public Arrow ArrowPrefab;
    public Transform ArrowSpawnpoint;
    public int Kills = 0;
    private int arrowsShot = 0;

    [Header("Interpolation")]
    public float LerpSpeed = 10f;
    private float targetAngle;
    private float rotatedByInner = 0;
    private float rotatedByOuter = 0;

    protected override void OnStart()
    {
        //add mesh to renderer
        Mesh pizzaslice = createPizzaSlice(FieldOfViewAngle, Range, Mathf.RoundToInt(FieldOfViewAngle / 2));
        AttackRangeMesh.mesh = pizzaslice;

        AttackRangeRenderer.enabled = false;
    }

    void Update()
    {
        //Turn towards target angle
        Vector3 currentAngles = RotatingTransform.localEulerAngles;
        float newAngle = Mathf.LerpAngle(currentAngles.y, targetAngle, Time.deltaTime * LerpSpeed);

        RotatingTransform.localEulerAngles = new Vector3(currentAngles.x, newAngle, currentAngles.z);
    }

    public override WorldSpaceUI GetUI()
    {
        return ui;
    }

    public override void OnCompletedConstruction()
    {
        base.OnCompletedConstruction();

        Initialize();
    }

    public override void OnEnterRotationState()
    {
        targetAngle = RotatingTransform.localEulerAngles.y;
    }

    public override void OnExitRotationState()
    {
        rotatedByInner = 0;
        rotatedByOuter = 0;
    }

    public override void OnEvaluateBuildingAction()
    {
        List<Enemy> shootableEnemies = GetShootableEnemies();

        //shoot enemies in field of view here
        foreach(Enemy e in shootableEnemies)
        {
            Shoot(e);
        }
    }

    public override void OnSelect()
    {
        base.OnSelect();

        SetShowLimits(true);
    }

    public override void OnDeselect()
    {
        base.OnDeselect();

        SetShowLimits(false);
    }

    public void OnRingsTurned(float degreesClockwise, bool inner)
    {
        degreesClockwise *= RotationMultiplier;

        //if the inner ring was turned we reverse the degrees
        if (inner)
        {
            degreesClockwise *= -1;
            rotatedByInner += degreesClockwise;
        }
        else
        {
            rotatedByOuter += degreesClockwise;
        }

        RotateRelative(degreesClockwise);
    }

    public void OnRingsReset(bool inner)
    {
        if (inner)
        {
            RotateRelative(-rotatedByInner);
            rotatedByInner = 0;
        }
        else
        {
            RotateRelative(-rotatedByOuter);
            rotatedByOuter = 0;
        }
    }

    public override TypeID Type()
    {
        return TypeID.Tower;
    }

    private void SetShowLimits(bool value)
    {
        //show tower limits
        AttackRangeRenderer.enabled = value;
    }

    private void RotateRelative(float degreesClockwise)
    {
        targetAngle += degreesClockwise * RotationMultiplier;
    }

    private InnerOuterRingListener innerListener, outerListener;
    private void Initialize()
    {
        if (OwnCell.Ring.InnerNeighbor)
        {
            innerListener = new InnerOuterRingListener(this, true);
            OwnCell.Ring.InnerNeighbor.AddListener(innerListener);
        }

        if (OwnCell.Ring.OuterNeighbor)
        {
            outerListener = new InnerOuterRingListener(this, false);
            OwnCell.Ring.OuterNeighbor.AddListener(outerListener);
        }
    }

    public override void OnDestroy()
    {
        base.OnDestroy();

        if (OwnCell.Ring.InnerNeighbor)
            OwnCell.Ring.InnerNeighbor.RemoveListener(innerListener);

        if (OwnCell.Ring.OuterNeighbor)
            OwnCell.Ring.OuterNeighbor.RemoveListener(outerListener);
    }

    private class InnerOuterRingListener : RingListener
    {
        private Watchtower w;
        private bool inner;

        public InnerOuterRingListener(Watchtower watchTower, bool inner)
        {
            w = watchTower;
            this.inner = inner;
        }

        public void OnRingReset()
        {
            w.OnRingsReset(inner);
        }

        public void OnRingTurned(float degreesClockwise)
        {
            w.OnRingsTurned(degreesClockwise, inner);
        }
    }

    private List<Enemy> GetShootableEnemies()
    {
        List<Enemy> enemies = GetEnemiesInBox();

        enemies.RemoveAll(x => { return !CanShoot(x); } );

        return enemies;
    }

    private bool CanShoot(Enemy enemy)
    {
        if (enemy == null)
            return false;

        //check angle
        Vector3 connectionVector = enemy.transform.position - transform.position;
        float angle = Vector3.Angle(connectionVector, RotatingTransform.forward);
        if (angle > FieldOfViewAngle * 0.5f)
            return false;

        //check range
        if (connectionVector.sqrMagnitude > Range * Range)
        {
            return false;
        }

        return true;
    }

    private void Shoot(Enemy enemy)
    {
        Arrow a = Instantiate(ArrowPrefab, ArrowSpawnpoint.position, Quaternion.identity);
        a.Init(ArrowSpawnpoint.position, enemy.transform.position, enemy, OnArrowArrived);
        arrowsShot++;
    }

    private void OnArrowArrived(Attackable enemy)
    {
        if(enemy == null || !enemy.isAlive())
        {
            arrowsShot--;
        }
        else
        {
            bool killedEnemy = enemy.Attack(AttackDamage);
            Debug.Log("Watchtower attacked " + enemy.gameObject().name + " for " + AttackDamage + " damage.");

            if (killedEnemy)
            {
                Kills++;
            }
        }

        arrowsShot--;
    }

    public override bool IsBusy()
    {
        return arrowsShot > 0;
    }

    private List<Enemy> GetEnemiesInBox()
    {
        Vector3 center = transform.position + RotatingTransform.transform.forward * Range / 2;
        Vector3 halfExtents = new Vector3(Range, 5, Range / 2);
        Quaternion orientation = RotatingTransform.rotation;
        LayerMask layerMask = LayerMask.GetMask("Enemies");
        QueryTriggerInteraction q = QueryTriggerInteraction.Ignore;

        Collider[] enemyColliders = Physics.OverlapBox(center, halfExtents, orientation, layerMask, q);

        List<Enemy> enemies = new List<Enemy>();
        foreach (Collider c in enemyColliders)
        {
            Enemy e = c.GetComponent<Enemy>();

            if (e && e.isAlive())
            {
                enemies.Add(e);
            }
        }

        return enemies;
    }

    private Mesh createPizzaSlice(float angle, float radius, int resolution)
    {
        //calculate angle of each triangle fan
        float vertexAngle = angle / (resolution - 1);

        // create vertices so center line of fan is on +z
        Vector3 direction = Quaternion.AngleAxis(-angle / 2f, Vector3.up) * Vector3.forward;

        //first vertex is middle
        //rest of array = outer vertices
        Vector3[] newVertices = new Vector3[resolution + 1];
        newVertices[0] = Vector3.zero;
        //create pairs of vertices according to specifications
        for (int i = 1; i < resolution + 1; i++)
        {
            newVertices[i] = direction * radius;

            //turn direction for next pair
            direction = Quaternion.AngleAxis(vertexAngle, Vector3.up) * direction;
        }

        //create triangle list
        int[] triangles = new int[(resolution - 1) * 3]; // vertices - 1 * 3 triangle indices


        for (int i = 1; i < resolution; i++)
        {
            int inner = 0;
            int outerLeft = i;
            int outerRight = i + 1;

            int triangleIndex = i - 1;
            //first slice
            triangles[3 * triangleIndex] = outerRight;
            triangles[3 * triangleIndex + 1] = inner;
            triangles[3 * triangleIndex + 2] = outerLeft;
        }

        //create UV list
        Vector2[] uvs = new Vector2[resolution + 1];
        uvs[0] = new Vector2(0, 0);
        for (int i = 1; i < resolution + 1; i++)
        {
            uvs[i] = new Vector2(Mathf.Lerp(0, 1, (i-1) * vertexAngle / angle), 1);
        }

        // create mesh
        Mesh newMesh = new Mesh();
        newMesh.vertices = newVertices;
        newMesh.triangles = triangles;
        newMesh.uv = uvs;
        newMesh.RecalculateNormals();

        return newMesh;
    }

    //Serialization
    public override void OnSave()
    {
        base.OnSave();

        SaveData.Add("RotationMultiplier", RotationMultiplier);
        SaveData.Add("FieldOfViewAngle", FieldOfViewAngle);
        SaveData.Add("Range", Range);
        SaveData.Add("AttackDamage", AttackDamage);
        SaveData.Add("Kills", Kills);
        SaveData.Add("targetAngle", targetAngle);
}

    public override void LoadData()
    {
        base.LoadData();

        RotationMultiplier = (float)SaveData["RotationMultiplier"];
        FieldOfViewAngle = (float)SaveData["FieldOfViewAngle"];
        Range = (float)SaveData["Range"];
        AttackDamage = (int)SaveData["AttackDamage"];
        Kills = (int)SaveData["Kills"];

        targetAngle = (float)SaveData["targetAngle"];
        RotatingTransform.localEulerAngles = new Vector3(RotatingTransform.localEulerAngles.x, targetAngle, RotatingTransform.localEulerAngles.z);

        Initialize();
    }
}

﻿using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class NaturalResourceSupply : MonoBehaviour, Selectable, TwistingSerializable {

    public enum TypeID
    {
        Forest
    }

    public Cell OwnCell;

    [SerializeField]
    private WorldSpaceUI UI;

    [SerializeField]
    private int RoundsToRefill; //refill resources every [RoundsToRefill] rounds
    private int NextRefill;

    [SerializeField]
    private Inventory Inventory;

    protected virtual void Start()
    {
        NextRefill = RoundsToRefill;
        Refill();
    }

    public WorldSpaceUI GetUI()
    {
        return UI;
    }

    public bool IsSelected()
    {
        return false;
    }

    public void OnDeselect()
    {
        
    }

    public void OnSelect()
    {
        
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void SetPosition(Transform parent)
    {
        this.transform.SetParent(parent, false);
    }

    protected Inventory GetInventory()
    {
        return Inventory;
    }

    public abstract TypeID GetTypeID();

    protected abstract void Refill();

    public abstract bool IsDepleted();

    public void OnEnterCommandState()
    {
        if (IsDepleted())
        {
            if (NextRefill <= 0)
            {
                NextRefill = RoundsToRefill;
                Refill();
            }

            NextRefill--;
        }
    }

    //Serialisation

    public JObject SaveData
    {
        get;
        set;
    }

    public virtual void OnSave()
    {
        SaveData = new JObject();
        SaveData.Add("RoundsToRefill", RoundsToRefill);
        SaveData.Add("NextRefill", NextRefill);
        SaveData.Add("TypeID", (int) GetTypeID());
        SaveData.Add("RingID", OwnCell.Ring.RingIndex);
        SaveData.Add("CellID", OwnCell.CellIndex);
    }

    public virtual void LoadData()
    {
        RoundsToRefill = (int) SaveData["RoundsToRefill"];
        NextRefill = (int)SaveData["NextRefill"];
    }
}

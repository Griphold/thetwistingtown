﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class BuildManager : MonoBehaviour, EventForwarder.ManagedPointerDownReceiver, TwistingSerializable {

    //-------------------------------------
    // static stuff
    //-------------------------------------
    private static BuildManager instance;
    public static BuildManager Instance
    {
        get { return instance; }
    }

    //-------------------------------------
    // Events stuff
    //-------------------------------------
    public UnityEvent OnBuildMenuOpened;
    public UnityEvent OnSelectingCell;
    public PlacedBuildOrderEvent OnPlacedBuildOrder = new PlacedBuildOrderEvent();
    public class PlacedBuildOrderEvent : UnityEvent<BuildOrder>
    {
    }
    //-------------------------------------
    //Settings stuff
    //-------------------------------------
    public bool DemolishEnabled = true;

    //-------------------------------------
    // instance stuff
    //-------------------------------------
    [Tooltip("Prefab for Build Orders. All spawned (incomplete) buildings will be instances of this. The BuildManager assigns their Formulas.")]
    [SerializeField]
    private BuildOrder buildOrderPrefab;
    [Header("GameObject References")]
    [SerializeField]
    private Board boardRef;
    [SerializeField]
    SelectBuildFormulaUI selectFormulaUI;

    [Header("Build Formulas")]
    [Tooltip("contains all BuildFormulas in the game. Must not be changed at runtime.")]
    [SerializeField]
    private BuildFormula[] existingFormulas;
    /// <summary>
    /// contains all BuildFormulas in the game.
    /// </summary>
    public BuildFormula[] ExistingFormulas { get { return existingFormulas; } }
    
    /// <summary>
    /// Contains all BuildFormulas currently available to the player.
    /// Editing entries in this is big doodoo.
    /// </summary>
    [Tooltip("contains all BuildFormulas the player can use immediately. To unlock new formulas edit the property of the same name.")]
    [SerializeField]
    private BuildFormula[] availableFormulas;
    
    
    /// <summary>
    /// Setter for availableFormulas.
    /// Also updates AvailableFormulaSet and the related UI.
    /// </summary>
    public void SetAvailableFormulas(BuildFormula[] value)
    {
        availableFormulas = value;
        availableFormulaSet = new HashSet<BuildFormula>(availableFormulas);
        selectFormulaUI.UpdateFormulaAvailability(availableFormulaSet);
    }

    /// <summary>
    /// Makes the given formula available to the player (= buildable.)
    /// Updates the AvailableFormulaSet and the related UI.
    /// Does nothing if the given formula already is available.
    /// </summary>
    /// <param name="formula">the formula to make available</param>
    public void MakeFormulaAvailable(BuildFormula formula)
    {
        // update stuff unless the given formula already is available
        if(availableFormulaSet.Add(formula))
        {
            BuildFormula[] arr = new BuildFormula[availableFormulas.Length + 1];
            availableFormulaSet.CopyTo(arr);
            SetAvailableFormulas(arr);
        }
    }

    private HashSet<BuildFormula> availableFormulaSet;
    /// <summary>
    /// contains a HashSet representation of the currently available BuildFormulas.
    /// Editing entries in this is big doodoo
    /// </summary>
    public HashSet<BuildFormula> AvailableFormulaSet { get { return availableFormulaSet; } }


    [Header("Behavior of the Preview Building")]
    [SerializeField]
    private float BuildingFollowMouseLerpSpeed = 10;
    [SerializeField]
    private float BuildingRotationLerpSpeed = 40;

    /// <summary>
    /// maps building type to build formula.
    /// This dictionary is constructed during BuildManager.Start() and so will not be available before that.
    /// </summary>
    public Dictionary<Building.TypeID, BuildFormula> formulaDict = null;


    /// <summary>
    /// container variable for remembering a selected build formula during selection of its cell.
    /// this Building object is just for visualization purposes and will be discarded when the actual Building gets placed
    /// </summary>
    Building buildingToPlace = null;

    BuildFormula selectedBuildFormula = null;

    // Use this for initialization
    void Start () {
        // register this instance as the current build manager
        // this acts like some kind of non-strict singleton
        if (instance != null)
        {
            Debug.LogWarning("BuildManager.instance already set during initialization. " +
                "There should never be two BMs. " +
                "Existing BM \"" + instance.gameObject.name + "\" will be overwritten.");
        }
        instance = this;

        // register self at the game manager in case that wasn't done in the inspector
        GameObject.FindObjectOfType<GameManager>().buildManager = this;

        
        // create prefab + formula dictionaries
        formulaDict = new Dictionary<Building.TypeID, BuildFormula>();

        foreach(BuildFormula buildFormula in existingFormulas)
        {
            formulaDict.Add(buildFormula.type, buildFormula);
        }

        // create available formula set
        availableFormulaSet = new HashSet<BuildFormula>(availableFormulas);

        // find build ui
        selectFormulaUI = GameObject.FindObjectOfType<SelectBuildFormulaUI>();

        // link build + demolish button
        selectFormulaUI.buildButton.onClick.AddListener(BuildButtonPressed);
        selectFormulaUI.demolishButton.onClick.AddListener(DemolishButtonPressed);
        selectFormulaUI.optionsButton.onClick.AddListener(OtherInputPressed);
	}
	

    Vector3 getMousePointOnMap()
    {
        return boardRef.GetMousePointOnMap();
    }

    Cell getClosestCell(Vector3 position)
    {
        Ring ring = boardRef.GetRingClickedOn(false);

        if (ring == null || !ring.Unlocked)
            return null;

        Cell closestCell = null;
        float closestDistance = float.MaxValue;
        foreach(Cell c in ring.cells)
        {
            float currentDistance = Vector3.SqrMagnitude(c.BuildingPosition.position - position);
            if (currentDistance < closestDistance)
            {
                closestCell = c;
                closestDistance = currentDistance;
            }
        }
        return closestCell;
    }
    
    /// <summary>
    /// creates a building of the specified type on the specified cell.
    /// any existing building will be discarded.
    /// </summary>
    /// <param name="type">type of the building to create</param>
    /// <param name="cell">cell on which to create the building</param>
    public Building SetBuilding(Building.TypeID type, Cell cell)
    {
        //
        cell.RemoveBuilding();

        //find out which building to place
        Building buildingToSet = null;
        if(type == Building.TypeID.BuildOrder)
        {
            buildingToSet = buildOrderPrefab;
        }
        else
        {
            //check dictionary
            BuildFormula formula;
            if(formulaDict.TryGetValue(type, out formula))
            {
                buildingToSet = formula.completedBuilding;
            }
            else
            {
                Debug.LogError("Buildmanager: No Build Formula set for type " + type.ToString());
                return null;
            }
        }

        // place build order on map
        Building newBuild = Instantiate<Building>(buildingToSet, cell.BuildingPosition);
        
        cell.SetBuilding(newBuild);//let the cell know of the building
        newBuild.OnCompletedConstruction();//let the build know it is completed

        return newBuild;
    }

    public void PlaceBuildingOrder(Cell cell, BuildFormula formula)
    {
        BuildOrder b = (BuildOrder) SetBuilding(Building.TypeID.BuildOrder, cell);

        b.Formula = formula;

        if(OnPlacedBuildOrder != null)
        {
            OnPlacedBuildOrder.Invoke(b);
        }
    }

    public bool ShouldAbort()
    {
        return PlayerInput.Instance.Keys.Cancel();
    }

    public void SetSelectedBuildFormula(BuildFormula formula)
    {
        if(!(currentState is SelectBuildFormula))
        {
            Debug.LogError("BuildManager's SetSelectedBuildFormula must only be called during the SelectBuildFormula state.");
            return;
        }

        selectedBuildFormula = formula;
        selectFormulaUI.HoveredFormula = formula;
        if (availableFormulaSet.Contains(formula))
            SetNextState(new SelectCellToBuildOn());
        else Debug.Log("Can't build a " + formula.name + ". That formula is not available at the moment.");
    }

    /// <summary>
    /// notifies the build manager of the build button being pressed.
    /// toggles the BM state between "waiting for input" and "select formula"(/"select cell")
    /// </summary>
    public void BuildButtonPressed()
    {
        System.Type current = currentState.GetType();
        if (current == typeof(WaitingForInput) || current == typeof(Demolish))
        {
            SetNextState(new SelectBuildFormula());
        }
        else if (current == typeof(BuildManager.SelectBuildFormula) || current == typeof(BuildManager.SelectCellToBuildOn))
        {
            SetNextState(new WaitingForInput());
        }
        else {
            Debug.LogError("Build Button was pressed during BM State: " + currentState.GetType().ToString()
              + ". In that state the Build Button should be inactive.");
        }
    }

    public void DemolishButtonPressed()
    {
        if (!DemolishEnabled)
            return;

        System.Type current = currentState.GetType();
        if (current == typeof(Demolish))
            SetNextState(new WaitingForInput());
        else if(current != typeof(Blocked)) {
            SetNextState(new Demolish());
        }
    }

    public void OtherInputPressed()
    {
        System.Type current = currentState.GetType();

        if (current != typeof(Blocked))
        {
            SetNextState(new WaitingForInput());
        }
    }

    //---------------------------------------------------------------------------------------------------
    // state management (copied from GameManager)
    //---------------------------------------------------------------------------------------------------
    State currentState = new Blocked();
    State nextState = new Blocked();
    

    // Update is called once per frame
    void Update()
    {
        if (nextState != null)
        {
            if (currentState != null)
                currentState.Exit(this);

            nextState.Entry(this);

            currentState = nextState;
            nextState = null;
        }

        currentState.Update(this);
    }

    public void SetNextState(State state)
    {
        if (state != null)
            nextState = state;
    }


    public interface State
    {
        void Entry(BuildManager buildManager);//Enters state on first update

        void Exit(BuildManager buildManager);//Exits immediately this frame

        void Update(BuildManager buildManager); //Update will be called on the next frame when changed to a certain state
    }

    public class Blocked : BuildManager.State
    {
        public void Entry(BuildManager buildManager)
        {
            // block build + demolish button
            buildManager.selectFormulaUI.buildButton.interactable = false;
            buildManager.selectFormulaUI.demolishButton.interactable = false;
        }

        public void Exit(BuildManager buildManager)
        {
            // reactivate build + demolish button
            buildManager.selectFormulaUI.buildButton.interactable = true;
            buildManager.selectFormulaUI.demolishButton.interactable = true;
        }

        public void Update(BuildManager buildManager)
        {
            // do nothing
        }
    }

    public class WaitingForInput : BuildManager.State
    {
        public void Entry(BuildManager buildManager)
        {
            // test: dont change ui state
            // set ui state
            // buildManager.selectFormulaUI.Displayed = false;
        }

        public void Exit(BuildManager buildManager)
        {
        }

        public void Update(BuildManager buildManager)
        {
            if(PlayerInput.Instance.Keys.Build())
            {
                buildManager.SetNextState(new BuildManager.SelectBuildFormula());
            }
            else if(PlayerInput.Instance.Keys.Demolish() && buildManager.DemolishEnabled)
            {
                buildManager.SetNextState(new Demolish());
            }
        }
    }


    public class SelectBuildFormula : BuildManager.State, EventForwarder.ManagedPointerDownReceiver
    {
        public void Entry(BuildManager buildManager)
        {

            // activate formula selection ui
            buildManager.selectFormulaUI.Displayed = true;

            //disable selection of buildings until placed
            PlayerInput.Instance.SetContext(PlayerInput.Context.BUILDMANAGER_FOCUS);

            if (buildManager.OnBuildMenuOpened != null)
            {
                buildManager.OnBuildMenuOpened.Invoke();
            }
        }

        public void Exit(BuildManager buildManager)
        {

            // close formula selection ui
            buildManager.selectFormulaUI.Displayed = false;

            // reset selected formula unless it's needed for the next state
            if(buildManager.nextState.GetType() != typeof(BuildManager.SelectCellToBuildOn))
            {
                buildManager.selectedBuildFormula = null;
            }

            //enable selection of buildings again
            releaseContext();
        }

        private void releaseContext()
        {
            PlayerInput.Instance.SetContext(PlayerInput.Context.FREE);
        }

        public void Update(BuildManager buildManager)
        {
            //-------------------------------------
            // check for abort conditions -> GameManager.Command state
            if (buildManager.ShouldAbort() || PlayerInput.Instance.Keys.Build())
            {
                buildManager.SetNextState(new BuildManager.WaitingForInput());
                return;
            }
            else if(PlayerInput.Instance.Keys.Demolish() && buildManager.DemolishEnabled)
            {
                buildManager.SetNextState(new Demolish());
                return;
            }

            //-------------------------------------
            // check for confirmed selection -> select cell state
            if((PlayerInput.Instance.Keys.Confirm()) && buildManager.selectFormulaUI.HoveredFormula != null)
            {
                buildManager.SetSelectedBuildFormula(buildManager.selectFormulaUI.HoveredFormula);
            }
            else if (PlayerInput.Instance.Keys.BuildingHotkey(Building.TypeID.VillagerHome))
            {
                buildManager.SetSelectedBuildFormula(Instance.formulaDict[Building.TypeID.VillagerHome]);
            }
            else if (PlayerInput.Instance.Keys.BuildingHotkey(Building.TypeID.Windmill))
            {
                buildManager.SetSelectedBuildFormula(Instance.formulaDict[Building.TypeID.Windmill]);
            }
            else if (PlayerInput.Instance.Keys.BuildingHotkey(Building.TypeID.Lumberjack))
            {
                buildManager.SetSelectedBuildFormula(Instance.formulaDict[Building.TypeID.Lumberjack]);
            }
            else if (PlayerInput.Instance.Keys.BuildingHotkey(Building.TypeID.Storage))
            {
                buildManager.SetSelectedBuildFormula(Instance.formulaDict[Building.TypeID.Storage]);
            }
            else if (PlayerInput.Instance.Keys.BuildingHotkey(Building.TypeID.Tower))
            {
                buildManager.SetSelectedBuildFormula(Instance.formulaDict[Building.TypeID.Tower]);
            }
        }

        public bool ReceivePointerDownEvent(PointerEventData.InputButton button)
        {
            if (button == PointerEventData.InputButton.Middle)
                return false;

            // release input context prematurely 
            // --would otherwise happen in State.Exit() during the next BuildManager.Update()
            // this makes it possible to exit the BM and to select something with the same click
            releaseContext();

            BuildManager.Instance.SetNextState(new WaitingForInput());
            return true;
        }
    }

    public class SelectCellToBuildOn : BuildManager.State, EventForwarder.ManagedPointerDownReceiver
    {
        /// <summary>
        /// prevents the selection of a cell using the same mouse click that was used to select a formula on the ui in the previous frame.
        /// </summary>
        private bool mouseBlocked;
        private bool hasLeftClicked;
        private BuildManager buildManager;

        private Vector3 targetPosition;
        private float targetYAngle;

        public void Entry(BuildManager buildManager)
        {
            this.buildManager = buildManager;

            if (buildManager.selectedBuildFormula == null)
            {
                Debug.LogError("Error Entering Cell Selection for Building State: no building to place is selected.");
                buildManager.SetNextState(new BuildManager.WaitingForInput());
                return;
            }

            // load model of building to place for visualization purposes
            buildManager.buildingToPlace = Instantiate<Building>(buildManager.selectedBuildFormula.completedBuilding);

            //disable navmeshobstacle
            NavMeshObstacle[] obstacles = buildManager.buildingToPlace.GetComponentsInChildren<NavMeshObstacle>();
            foreach(NavMeshObstacle obstacle in obstacles)
            {
                obstacle.enabled = false;
            }

            //disable selection of buildings until placed
            PlayerInput.Instance.SetContext(PlayerInput.Context.BUILDMANAGER_FOCUS);

            mouseBlocked = true;

            PlayerInput.Instance.SetShouldHighlightCurrentCell(true);
            PlayerInput.Instance.SetCursor(PlayerInput.CURSOR_BUILD);

            if (buildManager.OnSelectingCell != null)
            {
                buildManager.OnSelectingCell.Invoke();
            }
        }

        public void Exit(BuildManager buildManager)
        {

            // reset saved information from the previous formula selection
            buildManager.selectedBuildFormula = null;

            // reset saved information from the cell selection
            Destroy(buildManager.buildingToPlace.gameObject);
            buildManager.buildingToPlace = null;

            //enable selection of buildings again
            releaseContext();
        }

        private void releaseContext()
        {
            PlayerInput.Instance.SetContext(PlayerInput.Context.FREE);
            PlayerInput.Instance.SetShouldHighlightCurrentCell(false);
            PlayerInput.Instance.SetCursor(PlayerInput.CURSOR_DEFAULT);
        }


        // check for valid, confirmed cell selection -> create build order -> GameManager.Command state
        public bool ReceivePointerDownEvent(PointerEventData.InputButton button)
        {
            if (mouseBlocked)
                return false;

            // on right click: go back to select formula stage
            if (button == PointerEventData.InputButton.Right)
            {
                BuildManager.Instance.SetNextState(new BuildManager.SelectBuildFormula());
                return true;
            }
            else
                if (button == PointerEventData.InputButton.Left)
            {

                Vector3 pos = buildManager.getMousePointOnMap();
                Cell closestCell = buildManager.getClosestCell(pos);

                if (closestCell == null)
                {
                    Debug.Log("no cell selected. Select again.");
                    return true;
                }

                if (closestCell.IsOccupied())
                {
                    Debug.Log("Cell Occupied. Select a different one");
                    return true;
                }

                if (buildManager.selectedBuildFormula == null)
                {
                    Debug.LogError("Error while trying to create build order: no formula selected.");
                    return true;
                }

                buildManager.PlaceBuildingOrder(closestCell, buildManager.selectedBuildFormula);

                // after placing a building
                buildManager.SetNextState(new BuildManager.SelectBuildFormula());
                return true;
            }
            return false;
        }

        public void Update(BuildManager buildManager)
        {
            //-------------------------------------
            // check for abort conditions -> GameManager.Command state
            if (buildManager.ShouldAbort() || PlayerInput.Instance.Keys.Build())
            {
                buildManager.SetNextState(new BuildManager.WaitingForInput());
                return;
            }
            else if(PlayerInput.Instance.Keys.Demolish() && buildManager.DemolishEnabled)
            {
                buildManager.SetNextState(new Demolish());
                return;
            }

            //-------------------------------------
            // visualize currently hovered cell
            Vector3 pos = buildManager.getMousePointOnMap();
            Cell closestCell = buildManager.getClosestCell(pos);

            // if mouse is outside of the map
            // or over an area where there's no ring
            // hide model and try again next frame
            if (closestCell == null || closestCell.IsOccupied())
            {
                //follow the mouse
                //buildManager.buildingToPlace.gameObject.SetActive(false);
                targetPosition = pos;
                targetYAngle = 0;

                PlayerInput.Instance.SetShouldHighlightCurrentCell(false);
            }
            else
            {
                //follow the cell building position
                //buildManager.buildingToPlace.gameObject.SetActive(true);

                // make model of the building to place hover over the nearest cell
                //buildManager.buildingToPlace.transform.SetParent(closestCell.BuildingPosition, false);
                
                targetPosition = closestCell.BuildingPosition.position;
                targetYAngle = closestCell.BuildingPosition.eulerAngles.y;

                PlayerInput.Instance.SetShouldHighlightCurrentCell(true);
            }

            //lerp position
            Transform buildingTransform = buildManager.buildingToPlace.transform;
            buildingTransform.position = Vector3.Lerp(buildingTransform.position, targetPosition, Time.deltaTime * buildManager.BuildingFollowMouseLerpSpeed);

            //lerp rotation
            Vector3 currentBuildingEulers = buildingTransform.eulerAngles;
            Vector3 endEulers = new Vector3(currentBuildingEulers.x, Mathf.LerpAngle(currentBuildingEulers.y, targetYAngle, buildManager.BuildingRotationLerpSpeed * Time.deltaTime), currentBuildingEulers.z);
            buildingTransform.eulerAngles = endEulers;

            //-------------------------------------

            if (mouseBlocked && !Input.GetMouseButton(0))
                mouseBlocked = false;
        }
    }

    public class Demolish : State, EventForwarder.ManagedPointerDownReceiver
    {
        private BuildManager buildManager;

        public void Entry(BuildManager buildManager)
        {
            this.buildManager = buildManager;

            PlayerInput.Instance.SetShouldHighlightCurrentCell(true);
            PlayerInput.Instance.SetContext(PlayerInput.Context.BUILDMANAGER_FOCUS);
            PlayerInput.Instance.SetCursor(PlayerInput.CURSOR_DEMOLISH);
        }

        public void Exit(BuildManager buildManager)
        {
            PlayerInput.Instance.SetShouldHighlightCurrentCell(false);
            PlayerInput.Instance.SetContext(PlayerInput.Context.FREE);
            PlayerInput.Instance.SetCursor(PlayerInput.CURSOR_DEFAULT);
        }

        public bool ReceivePointerDownEvent(PointerEventData.InputButton button)
        {
            // on right click: go back to select formula stage
            if (button == PointerEventData.InputButton.Right)
            {
                BuildManager.Instance.SetNextState(new BuildManager.WaitingForInput());
                return true;
            }
            else if (button == PointerEventData.InputButton.Middle)
                return false;

            //raycast a building and demolish it

            // visualize currently hovered cell
            Vector3 pos = buildManager.getMousePointOnMap();
            Cell closestCell = buildManager.getClosestCell(pos);

            if(closestCell != null && closestCell.HasBuilding())
            {
                closestCell.DemolishBuilding();
                return true;
            }

            return false;
        }

        public void Update(BuildManager buildManager)
        {
            if(buildManager.ShouldAbort() || PlayerInput.Instance.Keys.Demolish())
            {
                buildManager.SetNextState(new WaitingForInput());
                return;
            }
            else if(PlayerInput.Instance.Keys.Build())
            {
                buildManager.SetNextState(new SelectBuildFormula());
                return;
            }
        }
    }

    private void DeregisterBuildingFromTown(Building building)
    {
        TownManager.Instance.RemoveBuilding(building);
    }

    /// <summary>
    /// called from EventForwarder when a PointerDownEvent made it past the UI
    /// </summary>
    public bool ReceivePointerDownEvent(PointerEventData.InputButton button)
    {
        if (currentState is EventForwarder.ManagedPointerDownReceiver)
            return ((EventForwarder.ManagedPointerDownReceiver)currentState).ReceivePointerDownEvent(button);
        else return false;
    }

    //Serialisation
    public JObject SaveData { get; set; }

    public void OnSave()
    {
    }

    public void LoadData()
    {
        //todo reset buildmanager here to first age
    }
}

﻿[System.Serializable]
public struct ResourceRequirement
{
    public Resource type;
    public int amount;
}

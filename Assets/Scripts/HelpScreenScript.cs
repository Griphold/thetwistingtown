﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HelpScreenScript : MonoBehaviour, IPointerClickHandler {
    
    public GameObject HelpScreen;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame - if the gameobject is enabled
	void Update () {
        if (Input.anyKeyDown)
        {
            CloseHelpScreen();
        }
		
	}

    public void OpenHelpScreen()
    {
        HelpScreen.SetActive(true);
    }

    public void CloseHelpScreen()
    {
        HelpScreen.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button != PointerEventData.InputButton.Middle)
            CloseHelpScreen();
    }
}

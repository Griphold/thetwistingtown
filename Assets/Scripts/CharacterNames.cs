﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterNames : MonoBehaviour{

    private const string NAMES_VILLAGERS = 
        "Benjapin, Buttonio, Clothar, Crotchester, Fabricardo, Fibert, Flannelson, Flaxel, Hemdrik, " +
        "Lacelo, Sewmour, Stitch, Threadginald, Voodolph, Weavan, Woolfgang, Yarnold, " +
        "Cordelia, Cottina, Dollores, Feleece, Garbriela, Knitalia, Loomise, " +
        "Needelilah, Puppetunia, Seamone, Tailor";


    private static List<string> FreeVillagerNames = new List<string>(NAMES_VILLAGERS.Split(new string[] { ", " }, System.StringSplitOptions.None));
    private static List<string> VillagerNamesInUse = new List<string>(FreeVillagerNames.Count);

    /// <summary>
    /// Returns a random villager name from the name list.
    /// Prioritizes unused names.
    /// </summary>
    public static string GetNextVillagerName()
    {
        if (FreeVillagerNames.Count == 0)
        {
            // no more free names. Mark all names free again.
            List<string> swap = FreeVillagerNames;
            FreeVillagerNames = VillagerNamesInUse;
            VillagerNamesInUse = swap;
        }

        // mark a random name as in use and return it
        string nextName = FreeVillagerNames[Random.Range(0, FreeVillagerNames.Count)];
        FreeVillagerNames.Remove(nextName);
        VillagerNamesInUse.Add(nextName);
        return nextName;
    }


    private void Awake()
    {
        //Random.InitState(System.DateTime.Now.GetHashCode());
    }
}

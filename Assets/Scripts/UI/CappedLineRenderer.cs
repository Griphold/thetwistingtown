﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class CappedLineRenderer: MonoBehaviour {
    [SerializeField]
    private GameObject Cap;
    public float CapLength;
    public bool AlignCapToPath;
    [SerializeField]
    private LineRenderer lineRenderer;

    public void SetPositions(Vector3[] positions)
    {
        if (positions == null || positions.Length < 1)
        {
            //set rest of positions
            lineRenderer.enabled = false;
            Cap.gameObject.SetActive(false);
            Cap.transform.localPosition = Vector3.zero;
            return;
        }

        lineRenderer.enabled = true;
        Cap.gameObject.SetActive(true);

        //set cap position so the tip is at the end of the positions and rotation along path to second point
        

        //Cap.transform.position = positions[positions.Length - 1] - 0.001f * transform.forward;//move it a bit upwards so it gets rendered above the line
            
        if(AlignCapToPath && positions.Length > 1)
        {
            //sum up point distances until we have enough to fill the cap
            float distanceAlready = 0;
            int currentPoint = positions.Length - 2;
            while (distanceAlready < CapLength && currentPoint > 0)
            {
                distanceAlready = Vector3.Distance(positions[positions.Length - 1], positions[currentPoint]);
                currentPoint--;
            }

            //float surplusDistance = distanceAlready - CapLength;
            Vector3 pointTakenAsCapEnd = positions[positions.Length - 1] + (positions[currentPoint] - positions[positions.Length - 1]).normalized * CapLength;
            //calculate position of cap as average of last and currentPoint which covers enough distance
            Vector3 capPosition = (positions[positions.Length - 1] + pointTakenAsCapEnd) / 2;
            //calculate direction
            Vector3 capDirection = (positions[positions.Length - 1] - pointTakenAsCapEnd).normalized;

            Cap.transform.position = capPosition;
            Cap.transform.rotation = Quaternion.LookRotation(-transform.forward, capDirection);


            //remove points occluded by cap
            int numOfPointsToCopy = currentPoint;
            Vector3[] newPositions = new Vector3[numOfPointsToCopy + 2];
            for (int i = 0; i < numOfPointsToCopy; i++)
            {
                newPositions[i] = positions[i];
            }

            if(newPositions.Length > 1)
            {
                //split last point into 2 so we can align the end of the renderer correctly
                Debug.DrawLine(pointTakenAsCapEnd, positions[positions.Length - 1]);
                newPositions[currentPoint + 1] = pointTakenAsCapEnd - (capDirection * 0.1f);
                newPositions[currentPoint] = newPositions[currentPoint + 1] - (capDirection * 1f);
            }

            positions = newPositions;
        }

        if(positions.Length > 2)
        {
            Debug.DrawLine(positions[positions.Length - 1], positions[positions.Length - 2]);
            Debug.DrawLine(positions[positions.Length - 2], positions[positions.Length - 3]);
        }
        //set rest of positions
        lineRenderer.positionCount = positions.Length;
        lineRenderer.SetPositions(positions);
    }

    public void SetColor()
    {

    }
}

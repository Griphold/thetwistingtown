﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Windowmanager : MonoBehaviour {
    public static Windowmanager Instance;

    public int MaximumOpenedWindows = 2;

    //Global UI Management
    public List<WorldSpaceUI> OpenWorldSpaceUIs = new List<WorldSpaceUI>();

    private Camera UICamera;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    private void OnDestroy()
    {
        if (Instance == this)
            Instance = null;
    }

    void Start()
    {
        Camera[] allCams = Camera.allCameras;

        foreach(Camera camera in allCams)
        {
            if (camera.CompareTag("UICamera"))
            {
                UICamera = camera;
                break;
            }
        }
    }
    

    public void Update()
    {

        MoveOpenUIs();

        //TODO keep on screen
    }

    private void DestroyRemovedUIs()
    {
        for (int i = OpenWorldSpaceUIs.Count - 1; i >= 0; i--)
        {
            WorldSpaceUI potentialDestroyedUI = OpenWorldSpaceUIs[i];
            if (potentialDestroyedUI.OwnerWasDestroyed())
            {
                OpenWorldSpaceUIs.RemoveAt(i);
                Destroy(potentialDestroyedUI.gameObject);
            }
        }
    }

    private void MoveOpenUIs()
    {
        if(OpenWorldSpaceUIs.Count == 1)
        {
            WorldSpaceUI ui = OpenWorldSpaceUIs[0];
            ui.SetTargetPosition(ui.GetPrefferedPositionScreenSpace());
        }
        else if (OpenWorldSpaceUIs.Count == 2)
        {
            WorldSpaceUI ui1 = OpenWorldSpaceUIs[0];
            WorldSpaceUI ui2 = OpenWorldSpaceUIs[1];

            //transform canvas world position to screen, add overlap/2 and then translate back to world
            Vector2 ui1CurrentScreenPos = ui1.GetPrefferedPositionScreenSpace();
            Vector2 ui2CurrentScreenPos = ui2.GetPrefferedPositionScreenSpace();

            //get screenspace corners
            Vector2[] ui1SSCorners = ui1.GetCornersAtPreferredPosition();
            Vector2[] ui2SSCorners = ui2.GetCornersAtPreferredPosition();
            
            Vector2 overlap = OverlapVectorScreenSpace(ui1SSCorners, ui2SSCorners);
            
            ui1CurrentScreenPos -= overlap / 2;
            ui2CurrentScreenPos += overlap / 2;

            ui1.SetTargetPosition(ui1CurrentScreenPos);
            ui2.SetTargetPosition(ui2CurrentScreenPos);
            
        }
    }

    public void Show(WorldSpaceUI ui)
    {
        if (ui == null || OpenWorldSpaceUIs.Contains(ui))
            return;

        OpenWorldSpaceUIs.Add(ui);

        InitUI(ui);

        ui.Show();

        //remove oldest UI
        if (OpenWorldSpaceUIs.Count > MaximumOpenedWindows)
        {
            HideOldestOpenedUI();
        }
    }

    public void InitUI(WorldSpaceUI ui)
    {
        ui.ChildToScreen(UICamera, transform);
        ui.transform.SetSiblingIndex(1);
    }

    private void HideOldestOpenedUI()
    {
        if (OpenWorldSpaceUIs.Count < 1)
            return;

        WorldSpaceUI ui = OpenWorldSpaceUIs[0];
        OpenWorldSpaceUIs.RemoveAt(0);

        if (ui != null && ui.IsVisible())
        {
            ui.Hide();
        }
    }

    public void Hide(WorldSpaceUI ui)
    {
        if(OpenWorldSpaceUIs.Remove(ui))
        {
            ui.Hide();
        }
    }

    public void HideAll()
    {
        while (OpenWorldSpaceUIs.Count > 0)
        {
            HideOldestOpenedUI();
        }
    }

    private Vector3 OverlapVectorScreenSpace(Vector2[] ui1SSCorners, Vector2[] ui2SSCorners)
    {
        //Get screen space corners
        Vector2[] ui1Corners = new Vector2[4];
        float minX_ui1 = Mathf.Infinity;
        float maxX_ui1 = -Mathf.Infinity;
        float minY_ui1 = Mathf.Infinity;
        float maxY_ui1 = -Mathf.Infinity;

        Vector2[] ui2Corners = new Vector2[4];
        float minX_ui2 = Mathf.Infinity;
        float maxX_ui2 = -Mathf.Infinity;
        float minY_ui2 = Mathf.Infinity;
        float maxY_ui2 = -Mathf.Infinity;

        //Determine Minimum X, Y values in Screen space
        for (int i = 0; i < 4; i++)
        {
            ui1Corners[i] = ui1SSCorners[i];
            if (ui1Corners[i].x < minX_ui1) { minX_ui1 = ui1Corners[i].x; }
            if (ui1Corners[i].x > maxX_ui1) { maxX_ui1 = ui1Corners[i].x; }
            if (ui1Corners[i].y < minY_ui1) { minY_ui1 = ui1Corners[i].y; }
            if (ui1Corners[i].y > maxY_ui1) { maxY_ui1 = ui1Corners[i].y; }

            ui2Corners[i] = ui2SSCorners[i];
            if (ui2Corners[i].x < minX_ui2) { minX_ui2 = ui2Corners[i].x; }
            if (ui2Corners[i].x > maxX_ui2) { maxX_ui2 = ui2Corners[i].x; }
            if (ui2Corners[i].y < minY_ui2) { minY_ui2 = ui2Corners[i].y; }
            if (ui2Corners[i].y > maxY_ui2) { maxY_ui2 = ui2Corners[i].y; }
        }

        float moveRight = maxX_ui1 - minX_ui2;//how many pixels to move right until not occluded
        float moveLeft = maxX_ui2 - minX_ui1;//how many pixels to move left until not occluded

        float moveUp = maxY_ui1 - minY_ui2; //how many pixels to move up until not occluded
        float moveDown = maxY_ui2 - minY_ui1; //how many pixels to move down until not occluded

        float moveXTotal = 0;
        //they are not occluding each other on x or y
        if(moveRight < 0 || moveLeft < 0 || moveUp < 0 || moveDown < 0)
        {
            return Vector2.zero;
        }
        
        //occluding each other and need to move less pixels when moving to right
        if(moveRight < moveLeft)
        {
            moveXTotal = moveRight;
        }
        //occluding each other and need to move less pixels when moving to left
        else
        {
            moveXTotal = -moveLeft;
        }


        float moveYTotal = 0;
        //occluding each other and need to move less pixels when moving up
        if (moveUp < moveDown)
        {
            moveYTotal = moveUp;
        }
        //occluding each other and need to move less pixels when moving down
        else
        {
            moveYTotal = -moveDown;
        }

        //move as few pixels as possible
        Vector2 totalOverlap = new Vector2();
        if(Mathf.Abs(moveXTotal) < Mathf.Abs(moveYTotal))
        {
            totalOverlap = new Vector2(moveXTotal, 0);
        }
        else
        {
            totalOverlap = new Vector2(0, moveYTotal);
        }


        return totalOverlap;
    }
}

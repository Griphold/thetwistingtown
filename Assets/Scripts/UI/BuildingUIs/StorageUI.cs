﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageUI : WorldSpaceUI
{
    public override void UpdateUIElements()
    {
    }

    public override void Show()
    {
        base.Show();
        SFX.PlayAtPosition(SFXCategory.Open_BuildingUI, PreferredPosition.position);
    }
}

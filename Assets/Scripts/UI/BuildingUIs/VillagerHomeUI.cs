﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VillagerHomeUI : WorldSpaceUI
{
    [SerializeField]
    private VillagerHome home;
    [SerializeField]
    private Text hungerTurnsField;
    [SerializeField]
    private Text inhabitantNameField;
    [SerializeField]
    private GameObject focusButtonContainer;

    private void Start()
    {
        UpdateUIElements();
    }

    public override void UpdateUIElements()
    {
        if (!home.MyVillager)
        {
            hungerTurnsField.text = "";
            inhabitantNameField.text = "Vacant";
            focusButtonContainer.SetActive(false);
        }
        else
        {
            hungerTurnsField.text = home.MyVillager.turnsTillHunger.ToString();
            inhabitantNameField.text = home.MyVillager.Name;
            focusButtonContainer.SetActive(true);
        }
    }

    public override void Show()
    {
        base.Show();
        SFX.PlayAtPosition(SFXCategory.Open_BuildingUI, PreferredPosition.position);
    }

    public void selectInhabitingVillager()
    {
        // close UI
        HideViaManager();
        // focus and select villager
        GameObject.FindObjectOfType<BirdsEyeCamera>().LookAt(home.MyVillager.transform);
        PlayerInput.Instance.Select(home.MyVillager);
    }
}

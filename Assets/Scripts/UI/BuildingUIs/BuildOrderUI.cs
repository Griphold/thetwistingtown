﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class BuildOrderUI : WorldSpaceUI
{
    [SerializeField]
    private float minCanvasWidth;

    private bool isCanvasWidthInitialized = false;

    // required references:
    private BuildOrder buildOrder;
    private Text ActionsInProgressField;
    

    public override void UpdateUIElements()
    {
        ActionsInProgressField.text = (buildOrder.Formula.requiredActions - buildOrder.ActionsTaken).ToString();
    }

    public override void Show()
    {
        if (!isCanvasWidthInitialized)
        {
            FitCanvasWidthToChild(true);
            isCanvasWidthInitialized = true;
        }
        base.Show();
        SFX.PlayAtPosition(SFXCategory.Open_BuildingUI, PreferredPosition.position);
    }

    public void Initialize()
    {
        // set build order reference
        buildOrder = transform.parent.gameObject.GetComponent<BuildOrder>();

        // if there's more than 1 inventory slot: resize UI
        Inventory inv = GetComponentInChildren<Inventory>();
        if(inv.GetNumberOfSlots() > 1)
        {
            // 2 slots: increase size by 1 slot
            GridLayoutGroup grid = inv.GetComponent<GridLayoutGroup>();
            inv.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 2f * grid.cellSize.x + grid.spacing.x);
            RectTransform view = transform.Find("Inventory View").GetComponent<RectTransform>();
            view.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, view.rect.width + grid.cellSize.x + grid.spacing.x);
        }

        Text[] textComponents = GetComponentsInChildren<Text>();

        // initialize required actions field
        textComponents.First<Text>(t => t.name.Equals("Required Actions Text")).text = buildOrder.Formula.requiredActions.ToString();

        // save reference to and disable supplied actions display
        ActionsInProgressField = textComponents.First<Text>(t => t.name.Equals("Supplied Actions Text"));
        transform.Find("Actions View").gameObject.SetActive(false);

        // set building name
        textComponents.First<Text>(t => t.name.Equals("Name Text")).text = buildOrder.Formula.name;
    }

    public void OnStartConstruction()
    {
        UpdateUIElements();
        // disable inventory display
        transform.Find("Inventory View").gameObject.SetActive(false);
        // enable supplied actions display
        transform.Find("Actions View").gameObject.SetActive(true);
        FitCanvasWidthToChild(false);
    }

    private void FitCanvasWidthToChild(bool useInventoryView)
    {
        string viewName = useInventoryView ? "Inventory View" : "Actions View";
        float width = transform.Find(viewName).GetComponent<RectTransform>().rect.width;

        if (width < minCanvasWidth)
            width = minCanvasWidth;

        GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
    }
}

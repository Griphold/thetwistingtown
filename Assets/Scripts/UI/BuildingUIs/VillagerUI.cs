﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VillagerUI : WorldSpaceUI
{
    // required references
    private Villager myVillager;

    [SerializeField]
    private Text unitNameField;
    [SerializeField]
    private Text hungerTimerField;

    /// <summary>
    /// initializes references. call this from Villager.Start()
    /// </summary>
    public void Initialize(Villager villager)
    {
        myVillager = villager;

        if (!unitNameField)
            unitNameField = transform.Find("Unit Name").GetComponent<Text>();

        if(!hungerTimerField)
            hungerTimerField = transform.Find("Background/Hunger Timer").GetComponentInChildren<Text>();

        // set name
        unitNameField.text = myVillager.Name;

        UpdateUIElements();
    }

    public override void UpdateUIElements()
    {
        hungerTimerField.text = myVillager.turnsTillHunger.ToString();
    }

    public override void Show()
    {
        base.Show();
        SFX.PlayAtPosition(SFXCategory.Open_BuildingUI, PreferredPosition.position);
    }

    public void selectHome()
    {
        // if homeless: abort
        if (!myVillager.home)
            return;

        // close own UI
        HideViaManager();
        // focus camera on home and select it.
        Camera.main.GetComponent<BirdsEyeCamera>().LookAt(myVillager.home.transform);
        PlayerInput.Instance.Select(myVillager.home);
    }
}

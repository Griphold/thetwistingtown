﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TownCenterUI : WorldSpaceUI {
    

    [SerializeField]
    Text InfoViewWoodField;
    [SerializeField]
    Text InfoViewStoneField;
    [SerializeField]
    Text InfoViewWheatField;
    [SerializeField]
    Text InfoViewVillagerField;
    [SerializeField]
    Button AgeUpButton;
    [SerializeField]
    Text UnlocksText;
    // set in start
    Animator infoViewWoodSlot;
    Animator infoViewStoneSlot;
    Animator infoViewWheatSlot;
    Animator infoViewVillagerSlot;

    private bool initialized = false;

    void Start()
    {
        InitializeSelf();
        AgeUpButton.onClick.AddListener(OnPressedAgeUpButton);   
    }

    public override void UpdateUIElements()
    {
        Age nextAge = TownManager.Instance.GetNextAge();

        //update costs
        updateInfoView(nextAge);

        AgeUpButton.interactable = TownManager.Instance.CanEnterNextAge();

        UnlocksText.text = nextAge != null ? nextAge.GetNewUnlocksText() : "";
    }

    public void OnPressedAgeUpButton()
    {
        TownManager.Instance.AdvanceToNextAge();
        UpdateUIElements();
        HideViaManager();
    }

    public override void Show()
    {
        base.Show();
        SFX.PlayAtPosition(SFXCategory.Open_BuildingUI, PreferredPosition.position);

        InitializeSelf();
        UpdateUIElements();
    }

    /// <summary>
    /// Initializes the gameObject. Returns immediately if already initialized.
    /// </summary>
	void InitializeSelf()
    {
        if (initialized)
            return;

        // setting references
        infoViewWoodSlot = InfoViewWoodField.transform.parent.parent.gameObject.GetComponent<Animator>();
        infoViewStoneSlot = InfoViewStoneField.transform.parent.parent.gameObject.GetComponent<Animator>();
        infoViewWheatSlot = InfoViewWheatField.transform.parent.parent.gameObject.GetComponent<Animator>();
        infoViewVillagerSlot = InfoViewVillagerField.transform.parent.parent.gameObject.GetComponent<Animator>();
    }

    private void updateInfoView(Age age)
    {
        // set resources info
        bool usesWood = false;
        bool usesStone = false;
        bool usesWheat = false;

        if (age != null)
        {
            // set time info
            InfoViewVillagerField.text = age.GetVillagerRequirement().ToString();
            
            foreach (var req in age.GetResourceCosts())
            {
                switch (req.type)
                {
                    case Resource.WOOD:
                        usesWood = true;
                        InfoViewWoodField.text = req.amount.ToString();
                        break;
                    case Resource.STONE:
                        usesStone = true;
                        InfoViewStoneField.text = req.amount.ToString();
                        break;
                    case Resource.WHEAT:
                        usesWheat = true;
                        InfoViewWheatField.text = req.amount.ToString();
                        break;
                    default:
                        Debug.LogError("Town Center UI has no info field for resource requirement of type " + req.type.ToString());
                        break;
                }
            }
        }

        updateSlot(infoViewWoodSlot, usesWood);
        updateSlot(infoViewStoneSlot, usesStone);
        updateSlot(infoViewWheatSlot, usesWheat);
        updateSlot(infoViewVillagerSlot, age != null);
    }

    // helper for updateInfoView()
    private void updateSlot(Animator slot, bool setActive)
    {
        GameObject go = slot.gameObject;
        if (!setActive)
            go.SetActive(false);
        else if (go.activeSelf)
            slot.SetTrigger("refresh");
        else go.SetActive(true);
    }
}

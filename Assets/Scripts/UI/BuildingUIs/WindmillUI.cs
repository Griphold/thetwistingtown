﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindmillUI : WorldSpaceUI
{
    [SerializeField] private Windmill owner;
    
    [SerializeField] private Text minText;
    [SerializeField] private Text maxText;

    [SerializeField] private Text valueText;

    [SerializeField] private Slider valueSlider;
    [SerializeField] private Slider fancySlider;

    [SerializeField] private Animator wingsAnimator;

    private string precisionTemplate = "0";

    public void Start()
    {
        // set minimum
        float temp = owner.GetMinimumProductivity();
        valueSlider.minValue = temp;
        fancySlider.minValue = temp;
        minText.text = '+' + temp.ToString(precisionTemplate) + ' ';

        // set maximum
        temp = owner.GetMaximumProductivity();
        valueSlider.maxValue = temp;
        fancySlider.maxValue = temp;
        maxText.text = '+' + temp.ToString(precisionTemplate) + ' ';

        UpdateUIElements();
    }

    public override void Show()
    {
        base.Show();
        UpdateUIElements();
        SFX.PlayAtPosition(SFXCategory.Open_BuildingUI, PreferredPosition.position);
    }

    public override void UpdateUIElements()
    {
        float productivity = owner.GetCurrentProductivity();

        valueSlider.value = productivity;
        fancySlider.value = productivity;
        valueText.text = productivity.ToString(precisionTemplate);

        if(IsVisible())
            wingsAnimator.SetFloat("rotationMultiplier", owner.GetBonusProductivity());
    }
}

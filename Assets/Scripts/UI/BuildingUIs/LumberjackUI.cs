﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LumberjackUI : WorldSpaceUI
{
    public RectTransform treesPanel;
    public Lumberjack owner;
    public GameObject Tree_Icon;

    private List<GameObject> allIcons = new List<GameObject>();

    public override void Show()
    {
        base.Show();
        SFX.PlayAtPosition(SFXCategory.Open_BuildingUI, PreferredPosition.position);
        UpdateUIElements();
    }

    public override void UpdateUIElements()
    {
        ShowTrees(owner.GetTreesInRange().Count);
    }

    private void ShowTrees(int count)
    {
        if(count > 20)
        {
            count = 20; //only 20 trees fit in indicator
        }

        int treesAlreadySpawned = allIcons.Count;
        
        //spawn new trees if necessary
        if(count > treesAlreadySpawned)
        {
            SpawnNewTrees(count - treesAlreadySpawned);
        }

        //show all neccessary trees and hide the rest
        for (int i = 0; i < allIcons.Count; i++)
        {
            //active section
            if(i < count)
            {
                allIcons[i].SetActive(true);
            }
            else
            //inactive section
            {
                allIcons[i].SetActive(false);
            }
        }
    }

    private int GetNumOfTreeIconsSpawned()
    {
        return allIcons.Count;
    }

    private int GetCurrentTreeAmountShown()
    {
        return allIcons.Where(x => { return x.activeSelf; }).Count();
    }

    private void SpawnNewTrees(int count)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject g = Instantiate(Tree_Icon, treesPanel);
            allIcons.Add(g);
        }
    }
}

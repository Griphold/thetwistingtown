﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using System.IO;
using Newtonsoft.Json;

public class SaveGame {
    public const string PATH_PREFIX = "saves/";
    public const string PATH_SUFFIX = ".twist";
    public const string EmptySceneForLoadingGames = "ringtests";

    private string name;
    private DateTime dateTime;
    private JObject saveGameData;

    public string Name
    {
        get
        {
            return name;
        }

        set
        {
            name = value;
        }
    }

    public DateTime DateTime
    {
        get
        {
            return dateTime;
        }

        set
        {
            dateTime = value;
        }
    }

    public SaveGame(string name, DateTime dateTime)
    {
        this.name = name;
        this.dateTime = dateTime;
    }

    public SaveGame(string name, DateTime dateTime, JObject data)
    {
        this.name = name;
        this.dateTime = dateTime;
        this.saveGameData = data;
    }

    public void SaveToDisk()
    {
        using (StreamWriter file = File.CreateText(PATH_PREFIX + name + PATH_SUFFIX))
        {
            JsonSerializer serializer = new JsonSerializer();
            serializer.Serialize(file, saveGameData);
        }
    }

    public JObject LoadFromDisk()
    {
        if(saveGameData == null)
        {
            try
            {
                StreamReader file = File.OpenText(PATH_PREFIX + name + PATH_SUFFIX);
                JsonSerializer serializer = new JsonSerializer();
                saveGameData = (JObject)serializer.Deserialize(file, typeof(JObject));
            }
            catch(Exception e)
            {
                Debug.LogError("Save game at saves/" + name + ".twist could not be loaded. Exception: " + e);
                return null;
            }
        }
        
        return saveGameData;
    }
}

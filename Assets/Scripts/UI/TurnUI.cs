﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TurnUI : MonoBehaviour, GameStateListener, Board.IBoardListener {
    [Header("Texts")]
    public string CommandPhaseText = "Start Rotating";
    public string RotationPhaseText = "Confirm Rotation";

    [Header("References")]
    public GameObject interactableIndicator;
    public Text TurnNumberText;
    public Button endTurnButton;
    //private Text endTurnButtonText;
    private Animator EndTurnButtonAnimator;

    private GameManager gameManager;
    private bool isRotationState;

    // Use this for initialization
    void Start () {
        gameManager = FindObjectOfType<GameManager>();

        if (gameManager == null)
        {
            Debug.LogError("No Gamemanager found in scene!");
            return;
        }

        
        endTurnButton.onClick.AddListener(OnClick);

        //endTurnButtonText = endTurnButton.GetComponentInChildren<Text>();
        EndTurnButtonAnimator = endTurnButton.GetComponent<Animator>();

        gameManager.AddListener(this);
        gameManager.board.AddListener(this);
	}

    void Update()
    {
        if (PlayerInput.Instance.Keys.AdvancePhase())
        {
            OnClick();
        }

        interactableIndicator.SetActive(!endTurnButton.interactable);
    }

    void UpdateUI()
    {
        //read turn number
        TurnNumberText.text = "" + gameManager.GetTurnNumber();

        if(isRotationState)
        {
            bool rotationValid = gameManager.board.IsRotationValid();

            if (endTurnButton.interactable != rotationValid)
                endTurnButton.interactable = rotationValid;
        }
    }

    private void OnClick()
    {
        gameManager.EndTurn();
    }

    public void OnEnterCommandState()
    {
        //endTurnButtonText.text = CommandPhaseText;
        EndTurnButtonAnimator.SetTrigger("Command");
        endTurnButton.interactable = true;
        UpdateUI();
    }

    public void OnExitCommandState()
    {
        
    }

    public void OnEvaluateBuildingAction()
    {
        EndTurnButtonAnimator.SetTrigger("Action");
        endTurnButton.interactable = false;
    }

    public void OnEvaluateEnemyAction()
    {
        
    }

    public void OnEnterRotationState()
    {
        isRotationState = true;
        UpdateUI();
        EndTurnButtonAnimator.SetTrigger("Rotation");
        endTurnButton.interactable = false;
    }

    public void OnExitRotationState()
    {
        isRotationState = false;
    }

    public void OnAnyRingChanged()
    {
        UpdateUI();
    }
}

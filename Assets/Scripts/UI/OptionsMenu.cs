﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.SceneManagement;

public class OptionsMenu : MonoBehaviour {

    private BirdsEyeCamera birdCamera;
    private PostProcessingBehaviour postProcessingBehaviour;
    private Animator animator;

    private void Start()
    {
        birdCamera = Camera.main.GetComponent<BirdsEyeCamera>();
        if(!birdCamera)
        {
            Debug.LogError("No BirdsEyeCamera found in scene");
        }

        postProcessingBehaviour = Camera.main.GetComponent<PostProcessingBehaviour>();
        if(!postProcessingBehaviour)
        {
            Debug.LogError("No PostProcessingBehaviour found in scene");
        }

        animator = GetComponent<Animator>();
    }

    public void Toggle()
    {
        animator.SetBool("isDisplayed", !animator.GetBool("isDisplayed"));
    }

    public void Close()
    {
        animator.SetBool("isDisplayed", false);
    }

    private void playOpenSFX()
    {
        SFX.PlayGlobal(SFXCategory.Open_OptionsUI);
    }

    private void playCloseSFX()
    {
        SFX.PlayGlobal(SFXCategory.Close_OptionsUI);
    }

    private void disableSelf()
    {
    }

    public void OnMainMenuButtonClicked()
    {
        SceneManager.LoadScene("mainmenu");
    }

    public void OnEdgePanningChanged(bool value)
    {
        birdCamera.EnableEdgePanning = value;
    }

    public void OnHorizontalInvertChanged(bool value)
    {
        birdCamera.InvertHorizontal = value;
    }

    public void OnVerticalInvertChanged(bool value)
    {
        birdCamera.InvertVertical = value;
    }

    public void OnPostProcessingChanged(bool value)
    {
        postProcessingBehaviour.enabled = value;
    }

    public void OnTreeSwayingChanged(bool value)
    {
        if (!TownManager.Instance)
            return;

        Board b = TownManager.Instance.gameManager.board;
        List<Swaying> allSwayingComponents = FindComponentsInDescendants<Swaying>(b.gameObject);
        
        foreach(Swaying s in allSwayingComponents)
        {
            s.enabled = value;
        }
    }



    static private List<T> FindComponentsInDescendants<T>(GameObject go)
    {
        List<T> result = new List<T>();
        if (go == null) return result;
        GameObject comp = go;
        T t = go.GetComponent<T>();

        if (t != null)
            result.Add(t);

        int numOfChildren = go.transform.childCount;
        int index = 0;
        while (index < numOfChildren)
        {
            result.AddRange(FindComponentsInDescendants<T>(comp.transform.GetChild(index++).gameObject));
        }
        return result;
    }
}

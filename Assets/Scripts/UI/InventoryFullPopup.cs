﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryFullPopup : MonoBehaviour {

    private static InventoryFullPopup popupPrefab;
    private static InventoryFullPopup popupPrefabLoaded
    {
        get
        {
            if (popupPrefab == null)
            {
                // load prefab
                popupPrefab = Resources.Load<InventoryFullPopup>("UI/InventoryFullPopup");
                if (popupPrefab == null)
                {
                    Debug.LogError("Could not find InventoryFullPopup prefab at the expected location.");
                    return null;
                }
            }
            return popupPrefab;
        }
    }

    /// <summary>
    /// spawns a InventoryFullPopup at the specified location
    /// </summary>
    public static void Create(Vector3 initialPosition)
    {
        // create popup
        Instantiate(popupPrefabLoaded, initialPosition, Quaternion.identity);
    }

    /// <summary>
    /// waits an amount of time specified in the prefab, then spawns a popup at the given location.
    /// </summary>
    public static IEnumerator CreateDelayedCoroutine(Vector3 initialPosition)
    {
        return CreateDelayedCoroutine(initialPosition, popupPrefabLoaded.spawnDelayValue);
    }

    /// <summary>
    /// waits for the specified time, then spawns a popup at the given position.
    /// </summary>
    public static IEnumerator CreateDelayedCoroutine(Vector3 initialPosition, float delayInSec)
    {
        yield return new WaitForSeconds(delayInSec);
        Create(initialPosition);   
    }

    //--------------------------------------------------------------
    // instance behavior
    //--------------------------------------------------------------

    /// <summary>
    /// delay value for spawning after a resource popup
    /// </summary>
    [SerializeField]
    private float spawnDelayValue;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //always face camera
        transform.LookAt(transform.position - Camera.main.transform.rotation * Vector3.back, Camera.main.transform.rotation * Vector3.up);
    }

    public void OnClose()
    {
        Destroy(this.gameObject);
    }
}

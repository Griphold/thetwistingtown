﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// button for selecting a specific build formula in the select build formula UI.
/// this button notifies the UI when hovered, so the UI can display data on its formula.
/// </summary>
[RequireComponent(typeof(UnityEngine.UI.Button))]
public class SelectBuildFormulaButton : MonoBehaviour, IPointerEnterHandler {
    [SerializeField]
    private BuildFormula formula;
    public BuildFormula Formula { get { return formula; } }

    [Header("required references")]
    [SerializeField]
    private UnityEngine.UI.Button buttonComponent;
    [SerializeField]
    private GameObject lockIcon;

    [SerializeField]
    private string lockedDescription;
    public string Description
    {
        get
        {
            if (Interactable) return formula.description;
            else return lockedDescription;
        }
    }

     public bool Interactable
    {
        get { return buttonComponent.interactable; }
        set
        {
            buttonComponent.interactable = value;
            lockIcon.SetActive(!value);
        }
    }

    /// <summary>
    /// reference to the UI this button is a part of
    /// </summary>
    private SelectBuildFormulaUI parentUI;


    public void InitializeFormulaButton(BuildFormula formula, SelectBuildFormulaUI parentUI)
    {
        this.parentUI = parentUI;
        this.formula = formula;
        
        buttonComponent.interactable = false;
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        parentUI.HoveredFormula = formula;
    }

    public void OnButtonPressed()
    {
        parentUI.HoveredFormula = formula;
        parentUI.ConfirmSelection();
    }
}

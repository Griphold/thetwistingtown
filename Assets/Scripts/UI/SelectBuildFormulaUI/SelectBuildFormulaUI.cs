﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

[RequireComponent(typeof(Animator))]
public class SelectBuildFormulaUI : MonoBehaviour {

    private bool displayed = false;
    public bool Displayed
    {
        set
        {
            if (displayed != value)
            { 
                displayed = value;
                if (displayed)
                    activateSelf();
                else hideSelf();
            }
        }
    }

    [Header("Required Prefabs")]
    [SerializeField]
    SelectBuildFormulaButton selectFormulaButtonPrefab;

    [Header("Required References")]
    public Button buildButton;
    public Button demolishButton;
    public Button optionsButton;
    [SerializeField]
    Transform formulaButtonsContainer;
    [SerializeField]
    Text InfoViewNameField;
    [SerializeField]
    Text InfoViewWoodField;
    [SerializeField]
    Text InfoViewStoneField;
    [SerializeField]
    Text InfoViewWheatField;
    [SerializeField]
    Text InfoViewTimeField;
    [SerializeField]
    Text DescriptionField;
    // set in start
    GameObject infoViewInfoRow;
    Animator infoViewAnimator;
    Animator infoViewWoodSlot;
    Animator infoViewStoneSlot;
    Animator infoViewWheatSlot;
    Animator infoViewTimeSlot;
    Animator animatorComponent;
    Dictionary<BuildFormula, SelectBuildFormulaButton> buildFormulaButtons;

    // currently hovered build formula.
    // will be sent to build manager if selection is confirmed.
    // its data is displayed in the info view.
    private BuildFormula hoveredFormula = null;
    public BuildFormula HoveredFormula
    {
        get { return hoveredFormula; }
        set {
            if (hoveredFormula != value)
            {
                hoveredFormula = value;
                updateInfoView();
            }
        }
    }



	/// <summary>
    /// Initializes the gameObject. Returns immediately if already initialized.
    /// </summary>
	void InitializeSelf () {
        if (animatorComponent != null)
            return;

        // setting references
        animatorComponent = GetComponent<Animator>();
        infoViewWoodSlot = InfoViewWoodField.transform.parent.parent.gameObject.GetComponent<Animator>();
        infoViewStoneSlot = InfoViewStoneField.transform.parent.parent.gameObject.GetComponent<Animator>();
        infoViewWheatSlot = InfoViewWheatField.transform.parent.parent.gameObject.GetComponent<Animator>();
        infoViewTimeSlot = InfoViewTimeField.transform.parent.parent.gameObject.GetComponent<Animator>();
        infoViewInfoRow = infoViewWoodSlot.transform.parent.gameObject;
        infoViewAnimator = infoViewInfoRow.transform.parent.gameObject.GetComponent<Animator>();

        // link buttons for build formulas
        InitializeFormulaButtons(BuildManager.Instance.ExistingFormulas);
        // set availability status
        UpdateFormulaAvailability(BuildManager.Instance.AvailableFormulaSet);
    }

    /// <summary>
    /// confirms selection of the last hovered build formula 
    /// (= the one currently displayed in the info field) 
    /// and notifies the build manager.
    /// Does nothing if there is no hovered build formula.
    /// </summary>
    public void ConfirmSelection()
    {
        if(hoveredFormula != null)
        {
            BuildManager.Instance.SetSelectedBuildFormula(hoveredFormula);
        }
    }

    private void activateSelf()
    {
        InitializeSelf();
        gameObject.SetActive(true);
        animatorComponent.SetBool("isDisplayed", true);
        infoViewAnimator.SetBool("isAvailable", hoveredFormula == null || BuildManager.Instance.AvailableFormulaSet.Contains(hoveredFormula));
    }

    private void playOpenSFX()
    {
        SFX.PlayGlobal(SFXCategory.Open_FormulaUI);
    }

    private void playCloseSFX()
    {
        SFX.PlayGlobal(SFXCategory.Close_FormulaUI);
    }

    private void hideSelf()
    {
        animatorComponent.SetBool("isDisplayed", false);
    }

    /// <summary>
    /// disables the gameObject. Used in the DisableSelf animation.
    /// </summary>
    private void disableSelf()
    {
        if (displayed == false)
            gameObject.SetActive(false);
    }


    /// <summary>
    /// gives all attached select formula buttons a reference to this ui.
    /// 
    /// shows an error message if some formula has no select formula button
    /// cause that probably means we forgot to put it in.
    /// (Yeah they're not auto-generated any more..)
    /// </summary>
    /// <param name="allBuildableFormulas">all BuildFormulas that need a select formula button at some point in the game</param>
    private void InitializeFormulaButtons(BuildFormula[] allBuildableFormulas)
    {
        // find all buttons
        SelectBuildFormulaButton[] buttons = formulaButtonsContainer.GetComponentsInChildren<SelectBuildFormulaButton>();

        // create dict
        buildFormulaButtons = new Dictionary<BuildFormula, SelectBuildFormulaButton>(buttons.Length);

        foreach (var button in buttons)
        {
            buildFormulaButtons.Add(button.Formula, button);
            // set ui reference
            button.InitializeFormulaButton(button.Formula, this);
        }

        // check if there's a button for each formula
        foreach(BuildFormula formula in allBuildableFormulas)
        {
            if (buttons.First<SelectBuildFormulaButton>(x => x.Formula == formula) == null)
                Debug.LogError("Missing Select Formula Button for " + formula.name);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CheckFormulaButtons(BuildFormula[] allExistingFormulas)
    {
        
    }

    /// <summary>
    /// Makes all SelectBuildFormulaButtons that are attached to the UI interactable
    /// if and only if their formula is contained in the given set.
    /// </summary>
    /// <param name="availableFormulas">set of all BuildFormulas currently available to the player</param>
    public void UpdateFormulaAvailability(HashSet<BuildFormula> availableFormulas)
    {
        SelectBuildFormulaButton[] formulaButtons = GetComponentsInChildren<SelectBuildFormulaButton>();
        foreach (var button in formulaButtons)
            button.Interactable = availableFormulas.Contains(button.Formula);
    }


    private void updateInfoView()
    {
        if (hoveredFormula == null)
        {
            InfoViewNameField.text = "Select a Formula";
            infoViewInfoRow.SetActive(false);
            infoViewAnimator.SetBool("isAvailable", true);
            DescriptionField.text = "";
        }
        else
        {
            infoViewAnimator.SetBool("isAvailable", BuildManager.Instance.AvailableFormulaSet.Contains(hoveredFormula));

            InfoViewNameField.text = hoveredFormula.displayName;
            infoViewInfoRow.SetActive(true);

            DescriptionField.text = buildFormulaButtons[hoveredFormula].Description;

            // set time info
            InfoViewTimeField.text = hoveredFormula.requiredActions.ToString();

            // set resources info
            bool usesWood = false;
            bool usesStone = false;
            bool usesWheat = false;

            foreach (var req in hoveredFormula.neededResources)
            {
                switch(req.type)
                {
                    case Resource.WOOD:
                        usesWood = true;
                        InfoViewWoodField.text = req.amount.ToString();
                        break;
                    case Resource.STONE:
                        usesStone = true;
                        InfoViewStoneField.text = req.amount.ToString();
                        break;
                    case Resource.WHEAT:
                        usesWheat = true;
                        InfoViewWheatField.text = req.amount.ToString();
                        break;
                    default:
                        Debug.LogError("SelectBuildFormulaUI has no info field for resource requirement of type " + req.type.ToString());
                        break;
                }
            }

            updateSlot(infoViewWoodSlot, usesWood);
            updateSlot(infoViewStoneSlot, usesStone);
            updateSlot(infoViewWheatSlot, usesWheat);
            updateSlot(infoViewTimeSlot, true);
        }

    }

    // helper for updateInfoView()
    private void updateSlot(Animator slot, bool setActive)
    {
        GameObject go = slot.gameObject;
        if (!setActive)
            go.SetActive(false);
        else if (go.activeSelf)
            slot.SetTrigger("refresh");
        else go.SetActive(true);
    }

}

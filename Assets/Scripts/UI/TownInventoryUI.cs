﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownInventoryUI : MonoBehaviour {

    [System.Serializable]
    public struct TownInventorySlot
    {
        public Resource Resource;
        public bool IsTownAccessible; // TODO do something with this or remove it.
        public Animator SlotReference;
    }


    [SerializeField]
    private TownInventorySlot[] resourceSlots;

    [SerializeField]
    private Animator villagerSlot;

    /// <summary>
    /// [0] - [resourceSlots.Length -1] contain values for the resource slots,
    /// [resourceSlots.Length] contains the value for the villager slot
    /// </summary>
    private int[] displayedAmounts;
    /// <summary>
    /// [0] - [resourceSlots.Length -1] contain values for the resource slots,
    /// [resourceSlots.Length] contains the value for the villager slot
    /// </summary>
    private UnityEngine.UI.Text[] textReferences;

    private TownManager townManager;

	// Use this for initialization
	void Start () {
        // find townmanager reference
        townManager = GameObject.FindObjectOfType<GameManager>().townManager;

        displayedAmounts = new int[resourceSlots.Length + 1];
        textReferences = new UnityEngine.UI.Text[resourceSlots.Length + 1];

        // find textfield references
		for(int i = 0; i < resourceSlots.Length; i++)
        {
            textReferences[i] = resourceSlots[i].SlotReference.gameObject.GetComponentInChildren<UnityEngine.UI.Text>();
            displayedAmounts[i] = -1;
        }
        textReferences[resourceSlots.Length] = villagerSlot.gameObject.GetComponentInChildren<UnityEngine.UI.Text>();
	}
	
	// Update is called once per frame
	void Update () {
        // update resource slots
		for(int i = 0; i < resourceSlots.Length; i++)
        {
            var slot = resourceSlots[i];

            // check if slot amount changed
            int currentAmount = getAmount(slot);
            
            if(currentAmount != displayedAmounts[i])
            {
                // if it did play the refresh animation (unless it's the first frame)
                if (displayedAmounts[i] != -1)
                    slot.SlotReference.SetTrigger("refresh");
                // ..and update the displayed amount
                displayedAmounts[i] = currentAmount;
                textReferences[i].text = currentAmount.ToString();
            }
        }

        // check if villager count changed
        int villagerCount = townManager.AllVillagers.Count;
        if(displayedAmounts[resourceSlots.Length] != villagerCount)
        {
            // if it did play the refresh animation (unless it's the first frame)
            if (displayedAmounts[resourceSlots.Length] != -1)
                villagerSlot.SetTrigger("refresh");
            // ..and update the displayed amount
            displayedAmounts[resourceSlots.Length] = villagerCount;
            textReferences[resourceSlots.Length].text = villagerCount.ToString();
        }
	}

    private int getAmount(TownInventorySlot slot)
    {
        int amount = townManager.GetPlayerAvailableTotal(slot.Resource);
        return amount;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePhaseAlert : MonoBehaviour, GameStateListener
{
    [SerializeField]
    private GameObject commandPhaseAlert;

    [SerializeField]
    private GameObject rotationPhaseAlert;

    [SerializeField]
    private GameObject gameOverAlert;

    [SerializeField]
    private GameObject winAlert;

    [SerializeField]
    private GameObject enemyPhaseAlert;

    private GameManager gameManager;

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        if(gameManager != null) { gameManager.AddListener(this); }
    }

    public void OnEnterCommandState()
    {
        DisableAllAlerts();
        commandPhaseAlert.SetActive(true);
    }

    public void OnEnterRotationState()
    {
        DisableAllAlerts();
        if (gameManager.IsGameOver())
        {
            gameOverAlert.SetActive(true);
        }
        else
        {
            rotationPhaseAlert.SetActive(true);
        }
    }

    public void ShowEnemyPhaseAlert()
    {
        DisableAllAlerts();
        enemyPhaseAlert.SetActive(true);
    }

    public bool IsEnemyPhaseAlertActive()
    {
        return enemyPhaseAlert.activeInHierarchy;
    }

    public void OnEvaluateBuildingAction()
    {

    }

    public void OnEvaluateEnemyAction()
    {

    }

    public void OnExitCommandState()
    {

    }

    public void OnExitRotationState()
    {
    }

    public void DisableCommandPhaseAlert()
    {
        commandPhaseAlert.SetActive(false);
    }

    public void DisableRotationPhaseAlert()
    {
        rotationPhaseAlert.SetActive(false);
    }

    public void DisableEnemyPhaseAlert()
    {
        enemyPhaseAlert.SetActive(false);
    }

    public void DisableGameOverAlert()
    {
        gameOverAlert.SetActive(false);
    }

    private void DisableAllAlerts()
    {
        DisableCommandPhaseAlert();
        DisableRotationPhaseAlert();
        DisableEnemyPhaseAlert();
        DisableWinAlert();
        DisableGameOverAlert();
    }

    public void ShowWinAlert()
    {
        winAlert.SetActive(true);
    }

    public void DisableWinAlert()
    {
        winAlert.SetActive(false);
    }
}

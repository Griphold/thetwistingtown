﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class VolumeSlider : MonoBehaviour {

    [SerializeField]
    private AudioMixer audioMixer;
    private const string MASTERVOLUME = "MasterVolume";

    [SerializeField]
    private AnimationCurve SliderToDB;
    private float sliderValue;
    private float volumeValue;

    private bool mute = false;

    [SerializeField]
    private Slider slider;
    [SerializeField]
    private GameObject toggledOnIcon;
    [SerializeField]
    private GameObject toggledOffIcon;

    void Start()
    {
        audioMixer.GetFloat(MASTERVOLUME, out volumeValue);

        SetMute(mute);
    }

    public void OnVolumeSliderChanged(float value)
    {
        if(value == slider.minValue)
        {
            SetMute(true);
            return;
        }

        sliderValue = value;
        volumeValue = SliderToDB.Evaluate(value);

        if (mute)
            SetMute(false);
        else
            audioMixer.SetFloat(MASTERVOLUME, volumeValue);

        SFX.PlayGlobal(SFXCategory.UI_Test_Volume);
    }

    public void OnToggleChanged(bool isOn)
    {
        SetMute(!isOn);
    }
    
    private void SetMute(bool value)
    {
        mute = value;

        toggledOnIcon.SetActive(!mute);
        toggledOffIcon.SetActive(mute);

        if(mute)
        {
            // mute
            audioMixer.SetFloat(MASTERVOLUME, -80f);

            slider.value = slider.minValue;
        }
        else
        {
            // unmute
            audioMixer.SetFloat(MASTERVOLUME, volumeValue);

            slider.value = sliderValue;
        }
    }
}

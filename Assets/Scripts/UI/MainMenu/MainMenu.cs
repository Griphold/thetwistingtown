﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
    public void OnStartGameButtonClicked()
    {
        SceneManager.LoadScene("newgame");
    }

    public void OnQuitButtonClicked()
    {
        Application.Quit();
    }

    public void OnTutorialButtonClicked()
    {
        SceneManager.LoadScene("tutorial");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthUI : MonoBehaviour {
    /// <summary>
    /// references to the attached health slots. 
    /// spawned on initialize if left empty
    /// </summary>
    [Tooltip("Spawned on Initialize() if left empty")]
    [SerializeField]
    private HealthSlot[] slots = new HealthSlot[0];

    private int maxHealth = 0;

    public void Initialize(int maximumHealth)
    {
        maxHealth = maximumHealth;

        if (slots.Length == 0) // generate slots
        {
            slots = new HealthSlot[maxHealth];

            for (int i = 0; i < maxHealth; i++)
            {
                slots[i] = Instantiate<HealthSlot>(HealthSlot.Prefab, transform);
            } 
        }
        else if (slots.Length < maxHealth) // not enough slots provided
        {
            string hopefullyOwnerName = GetComponentInParent<WorldSpaceUI>().transform.parent.name;
            Debug.LogError("HealthUI set up incorrectly. Either leave the slots array empty, or give it enough slots for the owner's max health " +
                "("+ hopefullyOwnerName + " - " + maxHealth + ")");
        }
        else if (slots.Length > maxHealth) // too many slots provided
        {
            // disable the surplus slots
            for (int i = maxHealth; i < slots.Length; i++)
                slots[i].gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Makes the specified number of attached HealthSlots display a heart.
    /// </summary>
    public void SetDisplayedHealth(int currentHealth)
    {
        for(int i = 0; i < maxHealth; i++)
        {
            // slots 0 - (current -1) display a heart,
            // slots current - (max -1) dont.
            slots[i].DisplaysHeart = i < currentHealth;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourcePopup : MonoBehaviour
{

    private static ResourcePopup ResourcePopupPrefab;

    public static ResourcePopup CreateResourcePopup(Resource resource, int amount, Vector3 startPosition)
    {
        if (amount == 0)
            return null;

        if (ResourcePopupPrefab == null)
        {
            ResourcePopupPrefab = Resources.Load<ResourcePopup>("UI/ResourcePopup");
            if (ResourcePopupPrefab == null)
            {
                Debug.LogError("Could not find ResourcePopup Prefab in Resources/UI folder.");
                return null;
            }
        }

        ResourcePopup p = Instantiate(ResourcePopupPrefab, startPosition, Quaternion.identity);
        p.resource = resource;
        p.amount = amount;

        return p;
    }

    [SerializeField]
    private Image image;
    [SerializeField]
    private Text text;
    [SerializeField]
    private Color positiveColor;
    [SerializeField]
    private Color negativeColor;
    private Resource resource;
    private int amount;
    private Transform mainCamera;
    //private Animator animator;

    // Use this for initialization
    void Start()
    {
        mainCamera = Camera.main.transform;

        image.sprite = ResourceMethods.GetSprite(resource);
        
        text.color = amount > 0 ? positiveColor : negativeColor;
        text.text = amount.ToString();
    }

    // Update is called once per frame
    void Update()
    {

        //always face camera
        transform.LookAt(transform.position - mainCamera.rotation * Vector3.back, mainCamera.rotation * Vector3.up);
    }

    public void OnClose()
    {
        Destroy(this.gameObject);
    }

}

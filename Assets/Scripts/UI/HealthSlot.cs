﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSlot : MonoBehaviour {

    private static HealthSlot prefab;
    
    /// <summary>
    /// loaded from Resources on first access
    /// </summary>
    public static HealthSlot Prefab
    {
        get
        {
            if (prefab == null)
            {
                prefab = Resources.Load<HealthSlot>("UI/Health Slot");
                if (prefab == null)
                    Debug.LogError("Could not load Health Slot prefab from the expected location.");
            }
            return prefab;
        }
    }

    //-------------------------------------------------------
    // Instance behavior
    //-------------------------------------------------------

    [SerializeField] private GameObject pinSolo;
    [SerializeField] private GameObject heart;

    private bool displaysHeart = true;
    public bool DisplaysHeart
    {
        get { return displaysHeart; }
        set
        {
            if (displaysHeart != value)
            {
                displaysHeart = value;
                UpdateDisplay();
            }
        }
    }

    public void UpdateDisplay()
    {
        pinSolo.SetActive(!displaysHeart);
        heart.SetActive(displaysHeart);
    }
}

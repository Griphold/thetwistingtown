﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Canvas))]
public abstract class WorldSpaceUI : MonoBehaviour {
    public static string PREFERRED_POSITION_NAME = "Preferred UI Position";

    public float LerpSpeed = 10;
    public bool DEBUG_SHOW_PREFERRED_POSITION_GIZMO = false;

    protected Transform PreferredPosition;
    private Vector2 targetPosition;
    private Camera UICamera;
    private bool init = false;

    public void ChildToScreen(Camera UICamera, Transform screenSpaceCanvas)
    {
        if (init)
            return;

        this.UICamera = UICamera;

        fixedSize = transform.localScale;

        GameObject g = new GameObject(PREFERRED_POSITION_NAME);
        g.transform.position = this.transform.position;
        g.transform.SetParent(transform.parent, true);
        
        PreferredPosition = g.transform;
        
        transform.gameObject.name = transform.gameObject.name + "(" + transform.parent.name + ")";
        transform.SetParent(screenSpaceCanvas.transform);

        //set up ui screen space stuff
        ((RectTransform)transform).localScale = Vector3.one;
        ((RectTransform)transform).position = GetPrefferedPositionScreenSpace();
        ((RectTransform)transform).rotation = Quaternion.identity;

        targetPosition = GetPrefferedPositionScreenSpace();

        init = true;
    }

    public Vector2 GetTargetPosition()
    {
        return targetPosition;
    }

    public void SetTargetPosition(Vector2 newTargetPosition)
    {
        targetPosition = newTargetPosition;
    }

    public Vector3 GetPreferredPositionWorldSpace()
    {
        if (PreferredPosition == null)
            Windowmanager.Instance.InitUI(this);

        return PreferredPosition.position;
    }

    public Vector2 GetPrefferedPositionScreenSpace()
    {
        if (PreferredPosition)
            return UICamera.WorldToScreenPoint(PreferredPosition.position);
        else
            return Vector2.zero;
    }

    public Vector2[] GetCornersAtPreferredPosition()
    {
        //move canvas to target pos and read corners then move back
        Vector3[] ssCorners = new Vector3[4];
        Vector3 currentPos = transform.position;
        transform.position = GetPrefferedPositionScreenSpace();
        ((RectTransform)transform).GetWorldCorners(ssCorners);
        
        transform.position = currentPos;

        Vector2[] corners = new Vector2[4];

        for (int i = 0; i < 4; i++)
        {
            corners[i] = new Vector2(ssCorners[i].x, ssCorners[i].y);
        }

        return corners;
    }

    public bool OwnerWasDestroyed()
    {
        return PreferredPosition == null;
    }

    private void OnDrawGizmos()
    {
        if(DEBUG_SHOW_PREFERRED_POSITION_GIZMO)
        {
            Vector2[] corners = GetCornersAtPreferredPosition();
            Vector2 center = Vector2.zero;
            foreach(Vector2 corner in corners)
            {
                center += corner;
            }
            center /= 4f;
            Vector2 size = new Vector2(Mathf.Abs(corners[0].x - corners[2].x), Mathf.Abs(corners[0].y - corners[2].y));
            Gizmos.DrawCube(center, size);
        }
    }

    protected virtual void OnDestroy()
    {
        HideViaManager();
    }

    private Vector3 fixedSize;
    public Vector3 FixedSize
    {
        get { return fixedSize; }
    }

    private void Start()
    {
        targetPosition = transform.position;
    }

    private void Update()
    {
        //targetPosition = GetPrefferedPositionScreenSpace();
        transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * LerpSpeed);
    }

    public bool IsVisible()
    {
        return gameObject.activeInHierarchy;
    }

    /// <summary>
    /// causes the windowmanager to hide this UI.
    /// </summary>
    public virtual void HideViaManager()
    {
        if(Windowmanager.Instance)
            Windowmanager.Instance.Hide(this);
    }

    /// <summary>
    /// Do not call this unless from the context of the window manager
    /// </summary>
    public virtual void Show()
    {
        gameObject.SetActive(true);

        transform.position = GetPrefferedPositionScreenSpace();
        SetTargetPosition(GetPrefferedPositionScreenSpace());
    }

    /// <summary>
    /// Do not call this unless from the context of the window manager.
    /// Use HideViaManager instead.
    /// </summary>
    public virtual void Hide()
    {
        gameObject.SetActive(false);
    }

    public abstract void UpdateUIElements();
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;

public class SaveGamePanel : MonoBehaviour {
    [Header("Options")]
    public bool canLoad = true;
    public bool canSave = false;
    public SerialisationManager serialisationManager;
    
    [Header("Prefabs")]
    public SaveGameUI saveGameUIPrefab;

    [Header("UI References")]
    public ToggleGroup saves;
    public InputField saveNameInput;
    public Button saveGameButton;
    public Button loadGameButton;

    private List<SaveGameUI> allSaveUIs = new List<SaveGameUI>();

    private void Awake()
    {
        //create saves directory if it doesnt exist
        if (!Directory.Exists(SaveGame.PATH_PREFIX))
        {
            Directory.CreateDirectory(SaveGame.PATH_PREFIX);
        }
    }

    private void Start()
    {
        if(!serialisationManager)
        {
            canSave = false;
        }

        if(saveGameButton)
        {
            saveGameButton.interactable = canSave;
            saveGameButton.onClick.AddListener(OnClickedSave);
        }

        if(loadGameButton)
        {
            loadGameButton.interactable = canLoad;
            loadGameButton.onClick.AddListener(OnClickedLoad);
        }
    }

    private void OnEnable()
    {
        if(PlayerInput.Instance) PlayerInput.Instance.Keys.DisableKeyboard = true;
        PopulateSaves();
    }

    public void PopulateSaves()
    {
        if (!saves)
            return;

        ClearSavesList();

        //find all files in saves folder
        DirectoryInfo d = new DirectoryInfo(SaveGame.PATH_PREFIX);
        FileInfo[] Files = d.GetFiles("*" + SaveGame.PATH_SUFFIX); //Getting twist files
        List<SaveGame> allSaves = new List<SaveGame>();
        foreach (FileInfo file in Files)
        {
            string name =  Path.GetFileNameWithoutExtension(file.Name);
            DateTime lastModification = file.LastWriteTime;
            allSaves.Add(new SaveGame(name, lastModification));
        }

        //sort by newest to oldest
        allSaves.Sort((a, b) =>
        {
            return DateTime.Compare(b.DateTime, a.DateTime);
        });

        //populate the list
        foreach(SaveGame s in allSaves)
        {
            SaveGameUI sUI = Instantiate(saveGameUIPrefab, saves.transform);
            sUI.SetSaveGame(s);
            allSaveUIs.Add(sUI);
            sUI.toggle.group = saves;
            sUI.toggle.isOn = false;
            sUI.OnSelect.AddListener(OnSelectSave);
        }
    }

    private void ClearSavesList()
    {
        for (int i = allSaveUIs.Count - 1; i >= 0; i--)
        {
            Destroy(allSaveUIs[i].gameObject);
        }

        allSaveUIs.Clear();
    }

    private void OnClickedSave()
    {
        //validate save game name input
        string saveName = saveNameInput.text;
        if(saveName == null || saveName.Length == 0 || saveName.Equals(""))
        {
            Debug.Log("Invalid Save name input: " + saveName);
            return;
        }

        //check if overwriting
        foreach(SaveGameUI sUI in allSaveUIs)
        {
            if(sUI.GetSaveGame().Name.Equals(saveName))
            {
                Debug.Log("Overwriting Save " + saveName);
            }
        }

        //save
        serialisationManager.SaveGame(saveName);

        OnClose();
    }

    private void OnClickedLoad()
    {
        if(saves)
        {
            Toggle activeToggle = saves.ActiveToggles().First();

            if (activeToggle != null)
            {
                //make sure we find a savegame
                SaveGameUI s = activeToggle.GetComponent<SaveGameUI>();

                if(!s)
                {
                    Debug.LogError("No save game component on toggle found.");
                    return;
                }

                //we are in the correct scene already and there is a savegame selected
                if(serialisationManager)
                {
                    serialisationManager.InitializeLoad(s.GetSaveGame());
                    OnClose();
                }
                //still need to load into correct scene
                else
                {
                    SavegameToLoad.SetSaveGameToLoad(s.GetSaveGame());
                    SceneManager.LoadScene(SaveGame.EmptySceneForLoadingGames);
                }
            }
            else
            {
                Debug.Log("No game selected to load.");
            }
        }
    }

    public void OnClose()
    {
        if (PlayerInput.Instance) PlayerInput.Instance.Keys.DisableKeyboard = false;
        gameObject.SetActive(false);
    }

    private void OnSelectSave(SaveGameUI saveUI)
    {
        //If a savegame was selected from the list, write its name into the input field
        if (saveNameInput)
        {
            saveNameInput.text = saveUI.GetSaveGame().Name;
        }
    }
}

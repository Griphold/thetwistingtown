﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class SaveGameUI : MonoBehaviour {
    public Toggle toggle;
    public Text nameText;
    public Text dateTimeText;
    public Image screenshot;

    private SaveGame saveGame;

    public class SaveGameUIEvent : UnityEvent<SaveGameUI> { };
    public SaveGameUIEvent OnSelect = new SaveGameUIEvent();

    public void SetSaveGame(SaveGame saveGame)
    {
        this.saveGame = saveGame;
    }

    public SaveGame GetSaveGame()
    {
        return saveGame;
    }

	// Use this for initialization
	void Start () {
        //setup ui here
        nameText.text = saveGame.Name;
        dateTimeText.text = saveGame.DateTime.ToString("MM/dd/yyyy") + "\t\t" + saveGame.DateTime.ToString("H:mm");

        if (toggle) { toggle.onValueChanged.AddListener(OnToggled); }
    }

    private void OnToggled(bool toggleValue)
    {
        if (toggleValue) { OnSelect.Invoke(this); }
    }
}

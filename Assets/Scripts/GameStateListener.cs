﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface GameStateListener {
    void OnEnterCommandState();
    void OnExitCommandState();

    void OnEvaluateBuildingAction();
    void OnEvaluateEnemyAction();

    void OnEnterRotationState();
    void OnExitRotationState();
}

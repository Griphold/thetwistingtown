﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitedInventorySlot : InventorySlot {


    [SerializeField]
    protected Color emptyColor;

    public override void Start()
    {
        base.Start();
    }

    protected override bool CanAddResource(Resource r)
    {
        return r == ResourceType;
    }

    protected override void UpdateSprite()
    {
        icon.sprite = ResourceMethods.GetSprite(ResourceType);
        icon.color = IsEmpty() ? emptyColor : Color.white;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using Newtonsoft.Json.Linq;

public class Inventory : MonoBehaviour, TwistingSerializable {

    public static float TRANSFER_RADIUS = 15f;

    [SerializeField]
    private List<InventorySlot> contents = new List<InventorySlot>();
    public GameObject Owner { get; set; }

    public void Start()
    {
        foreach (InventorySlot slot in contents)
        {
            slot.Inventory = this;
        }
    }

    public int GetNumberOfSlots()
    {
        return contents.Count;
    }

    public void SetDraggable(int index, bool value)
    {
        if(index < 0 || index > contents.Count - 1)
            return;

        contents[index].IsDraggable = value;
    }

    /// <summary>
    /// Add resources to inventory
    /// </summary>
    /// <param name="r">Resource</param>
    /// <param name="amountToAdd"></param>
    /// <returns>Leftover amount due to limits</returns>
    public int Add(Resource r, int amountToAdd, bool showPopup = true)
    {
        if (amountToAdd <= 0) return 0;
        int amountTriedToAdd = amountToAdd;
        //prioritise slots with more stuff already in them
        IEnumerable<InventorySlot> sortedSlots = contents.OrderByDescending(x => x.Amount);
        
        //try and add the resource to the first non-full slot
        foreach (InventorySlot slot in sortedSlots)
        {
            amountToAdd = slot.Add(r, amountToAdd);
        }

        if(!gameObject.activeInHierarchy && showPopup)
        {
            int popupAmount = amountTriedToAdd - amountToAdd; //how much we originally wanted to add - how much is leftover = how much we actually added to the inventory
            ResourcePopup.CreateResourcePopup(r, popupAmount, GetPopupSpawnPosition());
        }

        return amountToAdd;
    }

    public void Update()
    {
        if(TownManager.Instance.cheats)
        {
            if (PlayerInput.Instance.Keys.Cheat(1))
            {
                Add(Resource.WOOD, 1);
            }

            if (PlayerInput.Instance.Keys.Cheat(2))
            {
                Add(Resource.WHEAT, 1);
            }
        }
    }

    public void CreateNewSlot(int limit)
    {
        InventorySlot slot = Instantiate<InventorySlot>(InventorySlot.InventorySlotPrefab, this.transform);
        slot.Limit = limit;
        slot.Inventory = this;

        contents.Add(slot);
    }

    public void CreateNewSlot(int limit, Resource resource)
    {
        InventorySlot slot = Instantiate<InventorySlot>(InventorySlot.LimitedInventorySlotPrefab, this.transform);
        slot.Limit = limit;
        slot.ResourceType = resource;
        slot.Inventory = this;

        contents.Add(slot);
    }

    public bool CanTransferTo(Inventory other)
    {
        float distance = Vector3.Distance(Owner.transform.position, other.Owner.transform.position);

        return distance <= TRANSFER_RADIUS;
    }

    public bool IsFull()
    {
        foreach(InventorySlot slot in contents)
        {
            if(!slot.IsFull())
            {
                return false;
            }
        }

        return true;
    }

    public bool IsEmpty()
    {
        foreach (InventorySlot slot in contents)
        {
            if (!slot.IsEmpty())
            {
                return false;
            }
        }

        return true;
    }

    public void Clear()
    {
        foreach (InventorySlot slot in contents)
        {
            slot.Amount = 0;
        }
    }

    public int GetTotal(Resource r)
    {
        int result = 0;

        foreach(InventorySlot slot in contents)
        {
            if(slot.ResourceType == r)
            {
                result += slot.Amount;
            }
        }

        return result;
    }

    public int GetPlayerAvailableTotal(Resource r)
    {
        int amount = 0;
        foreach(InventorySlot slot in contents)
        {
            if (slot.ResourceType == r && slot.IsDraggable)
                amount += slot.Amount;
        }
        return amount;
    }

    public int GetTownAccessibleTotal(Resource r)
    {
        int result = 0;

        foreach (InventorySlot slot in contents)
        {
            if (slot.ResourceType == r && slot.TownAccessible)
            {
                result += slot.Amount;
            }
        }

        return result;
    }

    /// <summary>
    /// attempts to remove the given amount of the specified resource from the inventory.
    /// returns how many resources could be removed.
    /// </summary>
    /// <param name="r">type of resources to remove</param>
    /// <param name="amount">amount of resources to remove</param>
    /// <param name="townAccessible">if true, only resources from townAccessible slots will be taken</param>
    /// <returns>number of resources that were removed</returns>
    public int Remove(Resource r, int amount, bool townAccessible = false)
    {
        if (amount == 0)
            return 0;

        int counter = 0;

        foreach (InventorySlot slot in contents)
        {
            if(!townAccessible || slot.TownAccessible)
                counter += slot.Remove(r, amount - counter);
        }

        return counter;
    }

    public int RemainingSpace(Resource r)
    {
        int counter = 0;

        foreach(InventorySlot slot in contents)
        {
            counter += slot.RemainingSpace(r);
        }

        return counter;
    }

    /// <summary>
    /// calculates and returns the position at which to spawn resource popups
    /// </summary>
    public Vector3 GetPopupSpawnPosition()
    {
        Vector3 positionToSpawn = Owner.transform.position;

        //look for preferred ui position
        Selectable selectable = Owner.GetComponent<Selectable>();
        if (selectable != null && selectable.GetUI() != null)
        {
            positionToSpawn = selectable.GetUI().GetPreferredPositionWorldSpace();
        }

        return positionToSpawn;
    }


    public JObject SaveData { get; set; }

    public void OnSave()
    {
        foreach(InventorySlot slot in contents)
        {
            slot.OnSave();
        }

        SaveData = new JObject();
        JArray array = new JArray();
        foreach(InventorySlot slot in contents)
        {
            array.Add(slot.SaveData);
        }
        SaveData.Add("Slots", array);
    }

    public void LoadData()
    {
        if (SaveData == null)
            return;

        JArray slots = (JArray) SaveData["Slots"];
        DeleteSlots();

        foreach(JObject slotData in slots)
        {
            LoadSlot(slotData);
        }
    }
    
    private void LoadSlot(JObject slotData)
    {
        bool amILimited = (bool) slotData["Limited"];

        InventorySlot slotPrefab = amILimited ? InventorySlot.LimitedInventorySlotPrefab : InventorySlot.InventorySlotPrefab;

        InventorySlot slot = Instantiate<InventorySlot>(slotPrefab, this.transform);
        slot.SaveData = slotData;
        slot.LoadData();
        slot.Inventory = this;

        contents.Add(slot);
    }

    private void DeleteSlots()
    {
        for (int i = contents.Count - 1; i >= 0; i--)
        {
            Destroy(contents[i].gameObject);
        }

        contents.Clear();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public enum Resource {
    WOOD, WHEAT, STONE, BREAD, WATER, HEALTH
}

public static class ResourceMethods
{
    private static Dictionary<Resource, Sprite> sprites = new Dictionary<Resource, Sprite>();


    //load icon from Resources/UI/Icon_XXX (e.g. Icon_WOOD)
    public static Sprite GetSprite(Resource r)
    {
        if(sprites.ContainsKey(r))
        { 
            return sprites[r];
        }

        string path = "UI/Icon_" + Enum.GetName(typeof(Resource), r);

        Sprite sprite = Resources.Load<Sprite>(path);

        sprites[r] = sprite;

        return sprite;
    }
}

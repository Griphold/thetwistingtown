﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Scenario Options", menuName = "TheTwistingTown/Scenario Options", order = 2)]
public class ScenarioOptions : ScriptableObject {
    public Building TownCenter;
    public List<ResourceOption> ResourceOptions;
    public List<BuildingOption> BuildingOptions;

    public bool PeacefulMode = false;
    public SpawnBehaviour SpawnBehaviour;

    [System.Serializable]
    public struct ResourceOption
    {
        public NaturalResourceSupply SupplyPrefab;
        public int AmountToPlace;
        public int MinimumRing;
        public int MaximumRing;
    }

    [System.Serializable]
    public struct BuildingOption
    {
        public Building BuildingPrefab;
        public int AmountToPlace;
        public int MaximumRing;
    }
}

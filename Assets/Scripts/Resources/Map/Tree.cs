﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour, VillagerInteractable {
    [SerializeField]
    private WoodSupply forest;
    [SerializeField]
    private GameObject GrownModel;
    [SerializeField]
    private GameObject StumpModel;

    private bool assigned = false;
    private Villager assignedVillager;

    private bool isGrown = true;
    public bool IsGrown { get { return isGrown; } }

    public void OnInteract(Villager villager)
    {
        GrownModel.SetActive(false);
        StumpModel.SetActive(true);

        isGrown = false;

        forest.OnChopTree(this);

        assigned = false;
    }

    public void UnGrow()
    {
        if(isGrown)
        {
            GrownModel.SetActive(false);
            StumpModel.SetActive(true);

            isGrown = false;
        }
    }

    public void Grow()
    {
        if (isGrown)
            return;

        GrownModel.SetActive(true);
        StumpModel.SetActive(false);

        isGrown = true;
    }

    // Use this for initialization
    void Start () {
        forest = GetComponentInParent<WoodSupply>();
	}

    public bool IsAssigned()
    {
        return assigned;
    }

    public void Assign(Villager v)
    {
        assignedVillager = v;
        assigned = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Linq;
using Newtonsoft.Json.Linq;


//TODO BUGS:
// can click through front ui to drag things in the back UI (low priority - low severity)
public class InventorySlot : MonoBehaviour, InventoryListener, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler, TwistingSerializable
{
    //load from resources
    private static InventorySlot slotPrefab;
    public static InventorySlot InventorySlotPrefab
    {
        get
        {
            if (slotPrefab == null)
            {
                slotPrefab = Resources.Load<InventorySlot>("UI/InventorySlot");
            }

            return slotPrefab;
        }
    }

    private static LimitedInventorySlot limitedSlotPrefab;
    public static LimitedInventorySlot LimitedInventorySlotPrefab
    {
        get
        {
            if (limitedSlotPrefab == null)
            {
                limitedSlotPrefab = Resources.Load<LimitedInventorySlot>("UI/LimitedInventorySlot");
            }

            return limitedSlotPrefab;
        }
    }



    public Image icon;
    public Text text;

    public List<InventoryListener> listeners = new List<InventoryListener>();

    public Resource ResourceType { get { return resourceType; } set { resourceType = value; Notify(); } }
    public int Amount { get { return amount; } set { amount = value; Notify(); } }
    public int Limit { get { return limit; } set { limit = value; Notify(); } }
    public bool TownAccessible { get { return townAccessible; } set { townAccessible = value; Notify(); } }
    public bool IsDraggable { get { return isDraggable; } set { isDraggable = value; Notify(); } }
    public Inventory Inventory { get { return inventory; } set { if (inventory == null) inventory = value; } }


    [SerializeField]
    private Resource resourceType = Resource.WOOD;
    [SerializeField]
    private int amount = 0;
    [SerializeField]
    private int limit = 0;
    [SerializeField]
    private bool townAccessible = false;
    [SerializeField]
    private bool isDraggable = true;
    private Inventory inventory;

    public virtual void Start()
    {
        //add event listeners for dragging if not available
        if (gameObject.GetComponent<EventTrigger>() == null)
        {
            gameObject.AddComponent<EventTrigger>();
        }

        AddListener(this);
        OnChanged();
    }

    private int RemainingSpace()
    {
        return Limit - Amount;
    }

    public int RemainingSpace(Resource r)
    {
        if (IsFull() || !CanAddResource(r))
            return 0;

        return RemainingSpace();
    }

    public bool IsEmpty()
    {
        return Amount <= 0;
    }

    public bool IsFull()
    {
        return RemainingSpace() <= 0;
    }

    public void AddListener(InventoryListener inventoryListener)
    {
        listeners.Add(inventoryListener);
    }

    public void RemoveListener(InventoryListener inventoryListener)
    {
        listeners.Remove(inventoryListener);
    }

    public void Notify()
    {
        //Notify All Listeners
        foreach (InventoryListener inventoryListener in listeners)
        {
            inventoryListener.OnChanged();
        }
    }

    public void OnChanged()
    {
        //update the ui 
        UpdateSprite();

        text.text = Amount + "/" + Limit;
    }

    protected virtual void UpdateSprite()
    {
        icon.sprite = ResourceMethods.GetSprite(ResourceType);
        icon.gameObject.SetActive(!IsEmpty());
    }

    public void Add(InventorySlot slotToAdd)
    {
        //Determine whether we can add the slot
        if (slotToAdd.IsEmpty() || slotToAdd == this) return;

        int rest = Add(slotToAdd.ResourceType, slotToAdd.Amount);

        slotToAdd.Amount = rest;
    }

    public int Add(Resource r, int amountToAdd)
    {
        if (IsFull()) return amountToAdd;
        if (!CanAddResource(r)) return amountToAdd;

        //Check limits
        int rest = 0;
        int space = RemainingSpace();

        //not enough space in slot
        if (space < amountToAdd)
        {
            rest = amountToAdd - space;
        }

        //add as much as possible
        Amount += amountToAdd - rest;
        ResourceType = r;

        return rest;
    }

    public int Remove(Resource r, int amountToRemove)
    {
        if (IsEmpty() || r != ResourceType || amountToRemove <= 0)
            return 0;

        //remove as much as possible
        if(Amount >= amountToRemove)
        {
            Amount -= amountToRemove;
            return amountToRemove;
        }

        int toReturn = Amount;
        Amount = 0;
        return toReturn;
    }

    protected virtual bool CanAddResource(Resource r)
    {
        return IsEmpty() || r == ResourceType;
    }

    //dragging code (https://docs.unity3d.com/ScriptReference/EventSystems.IDragHandler.html)
    private bool isBeingDragged;
    private RectTransform draggingCanvasTransform;
    private InventorySlot emptyCopy;

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!PlayerInput.Instance.CanDragAndDrop() || !isDraggable)
            return;

        if (eventData.button != PointerEventData.InputButton.Left)
            return;

        if (IsEmpty() || isBeingDragged) return;

        // We have clicked something that can be dragged.
        //make the actual inventory slot follow the mouse and leave an empty copy in original position
        emptyCopy = EmptyCopy();

        isBeingDragged = true;

        ChildMeToCanvasOf(transform);

        SetDraggedPosition(eventData);
        
        PlayerInput.Instance.SetCursor(PlayerInput.CURSOR_HANDCLOSED);
    }

    public void OnDrag(PointerEventData data)
    {
        if (isBeingDragged)
            SetDraggedPosition(data);
    }

    private void SetDraggedPosition(PointerEventData data)
    {
        //did we enter a new canvas
        List<RaycastResult> UIAtMousePos = PlayerInput.RaycastWorldUI();

        //remove self and children of self
        UIAtMousePos.RemoveAll(x =>
        {
            if (x.gameObject.Equals(this.gameObject)) return true;
            return IsDescendantOf(this.transform, x.gameObject.transform);
        });

        //child to canvas first result
        IOrderedEnumerable<RaycastResult> orderedResults = UIAtMousePos.OrderBy(x => x.distance);

        if (orderedResults.Count() > 0)
            ChildMeToCanvasOf(orderedResults.First().gameObject.transform);

        //transform mouse into current dragging plane
        var rt = (RectTransform)transform;
        Vector3 globalMousePos;
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(draggingCanvasTransform, data.position, data.pressEventCamera, out globalMousePos))
        {
            rt.position = globalMousePos;
            rt.rotation = draggingCanvasTransform.rotation;
        }
    }

    private void ChildMeToCanvasOf(Transform toFindCanvasOn)
    {
        var canvas = FindInParents<Canvas>(toFindCanvasOn.gameObject);
        if (canvas == null)
            return;

        transform.SetParent(canvas.transform, false);
        transform.SetAsLastSibling();

        draggingCanvasTransform = (RectTransform)canvas.transform;
    }

    //destroy icon if we end the drag
    public void OnEndDrag(PointerEventData eventData)
    {
        if (eventData.button != PointerEventData.InputButton.Left)
            return;

        if (isBeingDragged)
        {
            InventorySlot otherSlot = FindInventorySlot();
            if (otherSlot != null && CanTransferTo(otherSlot))
            {
                otherSlot.Add(this);
            }

            transform.SetParent(emptyCopy.transform.parent, false);

            //retain same hierarchy order
            transform.SetSiblingIndex(emptyCopy.transform.GetSiblingIndex());

            Destroy(emptyCopy.gameObject);
            isBeingDragged = false;
            
            PlayerInput.Instance.SetCursor(PlayerInput.CURSOR_HANDOPEN);
        }
    }

    static private T FindInParents<T>(GameObject go) where T : Component
    {
        if (go == null) return null;
        var comp = go.GetComponent<T>();

        if (comp != null)
            return comp;

        Transform t = go.transform.parent;
        while (t != null && comp == null)
        {
            comp = t.gameObject.GetComponent<T>();
            t = t.parent;
        }

        return comp;
    }

    static public bool IsDescendantOf(Transform parent, Transform descendant)
    {
        if (descendant == null || parent == null)
            return false;

        Transform current = descendant.parent;

        while (current != null)
        {
            if (current == parent)
            {
                return true;
            }
            else
            {
                current = current.parent;
            }
        }

        return false;
    }

    private InventorySlot EmptyCopy()
    {
        GameObject clone = Instantiate(gameObject, transform.parent);

        //clone slot values
        InventorySlot empty = clone.GetComponent<InventorySlot>();
        empty.Amount = 0;
        empty.ResourceType = ResourceType;
        empty.Limit = Limit;
        empty.TownAccessible = TownAccessible;

        //resize drag icon to match the current size
        /*RectTransform myRect = (RectTransform) transform;
        RectTransform cloneRect = clone.GetComponent<RectTransform>();
        cloneRect.sizeDelta =  myRect.sizeDelta;*/

        //retain same hierarchy order
        empty.transform.SetSiblingIndex(transform.GetSiblingIndex());

        return empty;
    }

    private InventorySlot FindInventorySlot()
    {
        //Check whether we are clicking on world space UI
        List<RaycastResult> clickedUI = PlayerInput.RaycastWorldUI();

        //return first other inventory slot
        //List<InventorySlot> slotCandidates = new List<InventorySlot>();

        foreach (RaycastResult result in clickedUI)
        {
            InventorySlot slot = FindInParents<InventorySlot>(result.gameObject);

            if (slot != null && slot != this && slot != emptyCopy)
            {
                return slot;
            }
        }

        return null;
    }

    private bool CanTransferTo(InventorySlot other)
    {
        return inventory.CanTransferTo(other.inventory);
    }

    //CURSOR code
    public void OnPointerEnter(PointerEventData eventData)
    {
        //only signal to player that item slot is draggable whne there is stuff to drag and he isnt currently dragging anything
        if(CanDrag(eventData))
            PlayerInput.Instance.SetCursor(PlayerInput.CURSOR_HANDOPEN);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if(!eventData.dragging)
            PlayerInput.Instance.SetCursor(PlayerInput.CURSOR_DEFAULT);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button.Equals(PointerEventData.InputButton.Left) && CanDrag(eventData))
            PlayerInput.Instance.SetCursor(PlayerInput.CURSOR_HANDCLOSED);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (eventData.button.Equals(PointerEventData.InputButton.Left) && CanDrag(eventData))
            PlayerInput.Instance.SetCursor(PlayerInput.CURSOR_HANDOPEN);
    }

    private bool CanDrag(PointerEventData eventData)
    {
        return !IsEmpty() && !eventData.dragging && isDraggable;
    }

    //Serialisation
    public JObject SaveData { get; set; }

    public void OnSave()
    {
        SaveData = new JObject();

        //are we a limited inventory slot?
        bool amILimited = this is LimitedInventorySlot;

        SaveData.Add("Limited", amILimited);
        SaveData.Add("ResourceType", (int)resourceType);
        SaveData.Add("Amount", amount);
        SaveData.Add("Limit", limit);
        SaveData.Add("TownAccessible", townAccessible);
        SaveData.Add("Draggable", IsDraggable);
    }

    public void LoadData()
    {
        if (SaveData == null)
            return;

        ResourceType = (Resource)(int)SaveData["ResourceType"];
        Amount = (int)SaveData["Amount"];
        Limit = (int)SaveData["Limit"];
        TownAccessible = (bool)SaveData["TownAccessible"];
        IsDraggable = (bool)SaveData["Draggable"];
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// provides access to AudioSources that are attached to its children.
/// </summary>
public class AudioSourceCollection : MonoBehaviour {


    private AudioSource[] sources = null;
    /// <summary>
    /// array of AudioSources attached to child objects this AudioSourceCollection
    /// </summary>
    public AudioSource[] Sources
    {
        get
        {
            if (sources == null)
                loadRefs();
            return sources;
        }
    }

    // Use this for initialization
    private void loadRefs()
    {
        sources = GetComponentsInChildren<AudioSource>();
    }
}

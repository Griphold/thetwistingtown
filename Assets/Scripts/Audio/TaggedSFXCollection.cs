﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// sets SFXCategories for all AudioSources attached to its child objects
/// </summary>
[RequireComponent(typeof(AudioSourceCollection))]
public class TaggedSFXCollection : MonoBehaviour {
    
    [Tooltip("Category tags for all AudioSources in children of this object.")]
    [SerializeField]
    private SFXCategory[] categoryTags;
    /// <summary>
    /// Category tags for all AudioSources in children of this object.
    /// </summary>
    public SFXCategory[] CategoryTags { get { return categoryTags; } }
}

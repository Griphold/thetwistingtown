﻿/// <summary>
/// Identifiers for all existing categories of soundeffects
/// </summary>
[System.Serializable]
public enum SFXCategory {

    //----------------------------------------------------------------
    // NEVER change any of these numbers.
    // If you do you'll have to reassign every sfx category in the project.
    // Adding new ones is fine.
    //----------------------------------------------------------------

    // General UI SFX 0 - 1000
    // General UI Elements 0 - 100
    UI_Button_Press = 0,
    UI_Invalid_Action = 1,
    UI_Test_Volume = 99,
    // UI Windows 100 - 300
    // Open
    Open_BuildingUI = 100,
    Open_VillagerUI = 101,
    Open_FormulaUI = 102,
    Open_OptionsUI = 103,
    Open_Help,
    // Close
    Close_BuildingUI = 200,
    Close_VillagerUI = 201,
    Close_FormulaUI = 202,
    Close_OptionsUI = 203,
    Close_Help = 204,

// Selection SFX 1000 - 2000
    // Select
    Villager_Select = 1000,
    Building_Select = 1001,
    Buildorder_Select = 1002,
    Towncenter_Select = 1003,
    Lumberjack_Select = 1004,
    Townhouse_Select = 1005,
    Windmill_Select = 1006,
    Storage_Select = 1007,
    Watchtower_Select = 1008,
    Enemy_Select = 1009,
    // Deselect ?

    // Villager SFX 2000 - 3000
    Villager_Death = 2000,
    Villager_Receive_Move_Command = 2001,
    Villager_Eat = 2002,
    Villager_Spawn = 2003,

    // End of Turn Action SFX 3000 - 4000
    EOT_Construction = 3000,
    EOT_Wood_Chop = 3001,
    EOT_Wheat_Scythe = 3002,
    EOT_Watchtower_Fire = 3003,

    // Buildings SFX 4000 - 5000
    Building_Death = 4000,
    Building_Receive_Damage = 4001,
    Buildorder_Appear = 4002,
    Age_Advance = 4003,

// Enemies 5000 - 6000
    Enemy_Attack = 5000,
    Enemy_Spawn = 5001,
    Enemy_Get_Hit = 5002,
    Enemy_Start_Walking = 5003,
    Enemy_Death = 5004
}


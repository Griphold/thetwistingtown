﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXAnimationEvent : MonoBehaviour {

    [Header("Position to play soundeffects from")]
    [SerializeField]
    private Transform sfxOrigin;

    private void playSFX(SFXCategory category)
    {
        SFX.PlayAtPosition(category, sfxOrigin.position);
    }
}

﻿//#define _LOG_SFX_WARNINGS

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Responsible for playing all soundeffects. Attach SFX Audiosources in child objects, then they can be played using SFX.Instance.Play...()
/// </summary>
public class SFX : MonoBehaviour {

    //=====================================================================================
    // Static access for SFX functionality
    //=====================================================================================

    private static SFX _instance = null;

    public static SFX Instance
    {
        get { return _instance; }
    }


    /// <summary>
    /// plays a random soundeffect from the given sfx category at the position set in the sfx object.
    /// Does nothing if there is no sfx for the given category.
    /// </summary>
    /// <param name="category">identifies the desired soundeffect category</param>
    public static void PlayGlobal(SFXCategory category)
    {
        if (_instance)
            _instance.PlayGlobal_obj(category);
        else
            Debug.LogError("Missing SFX instance. Is there one in the scene?");
    }


    /// <summary>
    /// plays a random soundeffect from the given sfx category at the specified position.
    /// Does nothing if there is no sfx for the given category.
    /// </summary>
    /// <param name="category">identifies the desired soundeffect category</param>
    /// <param name="worldPosition">position (in worldspace) at which the soundeffect should be played</param>
    public static void PlayAtPosition(SFXCategory category, Vector3 worldPosition)
    {
        if (_instance)
            _instance.PlayAtPosition_obj(category, worldPosition);
        else
            Debug.LogError("Missing SFX instance. Is there one in the scene?");
    }


    //===========================================================================
    // Instance functionality
    //===========================================================================

    [Header("'Global' SFX are played here:")]
    [Tooltip("uses the AudioListener transform if not set in inspector")]
    [SerializeField]
    private Transform globalTarget;

    /// <summary>
    /// contains lists of soundeffect audiosources grouped by category
    /// </summary>
    private Dictionary<SFXCategory, List<AudioSource>> sfxDict = new Dictionary<SFXCategory, List<AudioSource>>();


    [Header("Debugstuff: Play SFX with P")]
    public bool enableDebugStuff;
    public SFXCategory group;
    public bool random = true;
    [Range(0, 10)]
    public int index = 0;



    // Use this for initialization
    void Start() {

        // register self as instance for this run
#if _LOG_SFX_WARNINGS
        if (_instance != null)
            Debug.LogWarning("There already is an SFX object registered. This scene ain't big enough fer two. (New object will overwrite the existing registration.)");
#endif
        _instance = this;

        // find global sfx position
        if (globalTarget == null)
            globalTarget = FindObjectOfType<AudioListener>().transform;

        // inital registration of all soundeffect audiosources. single & is important. both helper methods need to run.
        if(!registerSingleAudioSources() & !registerCollections())
            Debug.LogError("SFX object did not find any attached tagged AudioSources. Use TaggedAudioSource or TaggedSFXCollection.");
    }

    /// <summary>
    /// helper for initialization. registers attached TaggedAudioSources
    /// </summary>
    /// <returns>true if any were registered, false if not</returns>
    private bool registerSingleAudioSources()
    {
        TaggedAudioSource[] taggedSources = GetComponentsInChildren<TaggedAudioSource>();

        if (taggedSources == null || taggedSources.Length == 0)
            return false;

        bool successFlag = false;
        foreach(TaggedAudioSource taggedSource in taggedSources)
        {
            AudioSource attachedSource = taggedSource.GetComponent<AudioSource>();
            if (attachedSource == null)
            {
                Debug.LogError("Could not find AudioSource of TaggedAudioSource '" + taggedSource.name + "'");
            }
            else if (!attachedSource.enabled)
            {
#if _LOG_SFX_WARNINGS
                Debug.LogWarning("Did not register TaggedAudioSource '" + taggedSource.name + "' because it was disabled.");
#endif
            }
            else
            {
                // register the source under all tagged categories
                foreach (SFXCategory tag in taggedSource.CategoryTags)
                {
                    if (!sfxDict.ContainsKey(tag))
                        sfxDict.Add(tag, new List<AudioSource>(1));
                    else
                        sfxDict[tag].Capacity += 1;
                    sfxDict[tag].Add(attachedSource);
                    successFlag = true;
                }
            }
        }
        return successFlag;
    }

    /// <summary>
    /// helper for initialization. registers attached TaggedSFXCollections
    /// </summary>
    /// <returns>true if any were registered, false if not</returns>
    private bool registerCollections()
    {
        TaggedSFXCollection[] sfxCollections = GetComponentsInChildren<TaggedSFXCollection>();

        if (sfxCollections == null || sfxCollections.Length == 0)
        {
            return false;
        }

        bool successFlag = false;
        foreach (TaggedSFXCollection taggedCollection in sfxCollections)
        {
            // read all audioSources attached to the collection into a list
            List<AudioSource> list = new List<AudioSource>();
            AudioSourceCollection asc = taggedCollection.GetComponent<AudioSourceCollection>();
            foreach (AudioSource source in asc.Sources)
            {
                if(source.enabled)
                    list.Add(source);
            }

            if (list.Count == 0)
            {
#if _LOG_SFX_WARNINGS
                Debug.LogWarning("TaggedSFXCollection '" + taggedCollection.name + "' has no active AudioSources.");
#endif
                continue;
            }

            // register the list under all tagged categories
            foreach (SFXCategory tag in taggedCollection.CategoryTags)
            {
                if (!sfxDict.ContainsKey(tag))
                    sfxDict.Add(tag, list);
                else
                {
                    // there are two collections with this tag
                    // combine their source lists for this tag only
                    sfxDict[tag].Capacity += list.Capacity;
                    sfxDict[tag].AddRange(list);
                }
                successFlag = true;
            }
        }
        return successFlag;
    }


    // only used for testing soundeffects - aka Debugstuff
    void Update()
    {
        if(enableDebugStuff && Input.GetKeyDown(KeyCode.P))
        {
            if (random) PlayGlobal_obj(group);
            else playByID(group, index);
        }
    }


    /// <summary>
    /// draws a random sound effect audiosource from the given category.
    /// will interrupt a random audiosource if no idle one is found.
    /// </summary>
    /// <param name="category">identifies the desired soundeffect category</param>
    /// <returns>a random sound effect audiosource from the given category</returns>
    protected AudioSource drawRandomFromCategory(SFXCategory category)
    {
        List<AudioSource> categoryList = sfxDict[category];
        AudioSource source = categoryList[Random.Range(0, categoryList.Count)];

        // re-draw if a currently active source was drawn.
        if (source.isPlaying)
        {
            categoryList = categoryList.FindAll(s => !s.isPlaying);
            if (categoryList.Count > 0)
                source = categoryList[Random.Range(0, categoryList.Count)];
            // if there are no idle audiosources stop the originally drawn one and use it.
            else source.Stop();
        }

        return source;
    }


    /// <summary>
    /// plays a random soundeffect from the given sfx category at the position set in the sfx object.
    /// Does nothing if there is no sfx for the given category.
    /// </summary>
    /// <param name="category">identifies the desired soundeffect category</param>
	public void PlayGlobal_obj(SFXCategory category)
    {
        if (!isRegistered(category)) return;

        Vector3 position = globalTarget.position;
        PlayAtPosition_obj(category, position);
    }


    /// <summary>
    /// plays a random soundeffect from the given sfx category at the specified position.
    /// Does nothing if there is no sfx for the given category.
    /// </summary>
    /// <param name="category">identifies the desired soundeffect category</param>
    /// <param name="worldPosition">position (in worldspace) at which the soundeffect should be played</param>
    public void PlayAtPosition_obj(SFXCategory category, Vector3 worldPosition)
    {
        if (!isRegistered(category)) return;
            
        // draw audiosource
        AudioSource source = drawRandomFromCategory(category);

        // move it to the specified position
        source.transform.position = worldPosition;

        // play it
        source.PlayOneShot(source.clip);
    }


    /// <summary>
    /// plays the specified sfx audiosource. Source still must be registered at the SFX object.
    /// </summary>
    /// <param name="category">identifyies the desired soundeffect category</param>
    /// <param name="name">title of the desired audioclip (can also be name of the object containing the audiosource, but you should use title)</param>
    /// <param name="worldPosition">position (in worldspace) at which the soundeffect should be played</param>
    public void PlaySpecificSFX(SFXCategory category, string name, Vector3 worldPosition)
    {
        List<AudioSource> categoryList = sfxDict[category];

        AudioSource source = categoryList.Find(s => s.clip.name == name); 
        if(source == null)
            source = categoryList.Find(s => s.name == name);

        if (source == null)
            return;

        source.transform.position = worldPosition;
        source.PlayOneShot(source.clip);
    }


    /// <summary>
    /// stops all active sfx audiosources of the specified category. Source still must registered at the SFX object.
    /// </summary>
    /// <param name="category">identifyies the desired soundeffect category</param>
    public void StopSFXCategory(SFXCategory category)
    {
        List<AudioSource> categoryList = sfxDict[category];

        foreach(AudioSource source in categoryList)
        {
            if (source.isPlaying)
                source.Stop();
        }
    }


    /// <summary>
    /// stops the specified sfx audiosource. Source still must registered at the SFX object.
    /// </summary>
    /// <param name="category">identifyies the desired soundeffect category</param>
    /// <param name="name">title of the desired audioclip (can also be name of the object containing the audiosource, but you should use title)</param>
    public void StopSpecificSFX(SFXCategory category, string name)
    {
        List<AudioSource> categoryList = sfxDict[category];

        AudioSource source = categoryList.Find(s => s.clip.name == name);
        if (source == null)
            source = categoryList.Find(s => s.name == name);

        if (source != null && source.isPlaying)
            source.Stop();
    }


    /// <summary>
    /// checks if the given category is registered in the sfxDict.
    /// </summary>
    private bool isRegistered(SFXCategory category)
    {
        // This does not directly check if AudioSources for the given category exist,
        // but the way initialization is set up atm categories are only registered while adding a Source,
        // so this should also tell if there is at least one AudioSource for the given category.
        if (!sfxDict.ContainsKey(category))
        {
#if _LOG_SFX_WARNINGS
            Debug.LogWarning("No SFX of category " + category.ToString() + " registered.");
#endif
            return false;
        }
        else return true;
    }

    private void playByID(SFXCategory category, int index)
    {
        AudioSource s = sfxDict[category][index];
        s.PlayOneShot(s.clip);
    }
}

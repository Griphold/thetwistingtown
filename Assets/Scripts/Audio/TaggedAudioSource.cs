﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// sets SFXCategories for a single attached AudioSource
/// </summary>
[RequireComponent(typeof(AudioSource))]
public class TaggedAudioSource : MonoBehaviour
{

    [Tooltip("Category tags for the AudioSource.")]
    [SerializeField]
    private SFXCategory[] categoryTags;
    /// <summary>
    /// Category tags for the attached AudioSource.
    /// </summary>
    public SFXCategory[] CategoryTags { get { return categoryTags; } }
}


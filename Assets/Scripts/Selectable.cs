﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Selectable {
    void OnSelect();
    void OnDeselect();

    bool IsSelected();

    WorldSpaceUI GetUI();
}

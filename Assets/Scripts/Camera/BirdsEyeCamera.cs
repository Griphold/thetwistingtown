﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

using UnityEngine.PostProcessing;

//Pan at screen bounds or wasd
//rotation with holding middle mouse button
//zoom with scroll wheel
//TODO smooth while rotation without krebs
public class BirdsEyeCamera : MonoBehaviour, EventForwarder.ManagedScrollEventReceiver, GameStateListener
{

    [Header("Speeds")]
    public float ZoomSensitivity = 1;
    public float PanSensitivity = 1;
    public float HorizontalRotationSensitivity = 1;
    public float VerticalRotationSensitivity = 1;
    public bool InvertHorizontal = false;
    public bool InvertVertical = false;

    [Header("Smoothness")]
    public float TranslationSmoothing = 20;
    public float RotationSmoothing = 20;
    public float PhaseTransitionSmoothing = 5;

    [Header("Edge Panning")]
    public bool EnableEdgePanning = true;
    public float PixelCutoff = 50;

    [Header("Limits")]
    public Transform BoundsCenter;
    public float BoundsRadius;
    public float BoundsReductionAtMax;
    public CameraConstraints CommandState;
    public CameraConstraints RotationState;
    private CameraConstraints currentConstraints;

    private float zoomTarget;
    private float currentZoomLerpSpeed;
    private Vector3 pivotTargetPosition;
    private float pivotTargetEulerX;
    private float pivotTargetEulerY;
    private Transform Pivot;

    //focus stuff
    private bool newFocusTarget = false;

    private bool receivedScrollEvent = false;

    private PostProcessingProfile postProfile;

    private GameManager gameManager;

    [Header("Focus")]
    public AnimationCurve ApertureByZoom;
    public float FocusSpeed = 10.0f;

    // Use this for initialization
    void Start()
    {
        gameManager = GameObject.FindObjectOfType<GameManager>();
        gameManager.AddListener(this);
        currentConstraints = CommandState;
        
        zoomTarget = -transform.localPosition.z;
        currentZoomLerpSpeed = TranslationSmoothing;

        Pivot = transform.parent;
        pivotTargetPosition = Pivot.position;
        pivotTargetEulerX = Pivot.eulerAngles.x;
        pivotTargetEulerY = Pivot.eulerAngles.y;

        // Load the post processing profile
        postProfile = GetComponent<PostProcessingBehaviour>().profile;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        bool zoomInput = HandleZoomInput();

        bool panInput = HandlePanInput();

        bool rotateInput = HandleRotateInput();

        //if camera is focussing a target the player may interrupt it
        if (newFocusTarget && (zoomInput || panInput || rotateInput))
        {
            newFocusTarget = false;
            zoomTarget = -transform.localPosition.z;
            pivotTargetPosition = Pivot.position;
            pivotTargetEulerX = Pivot.eulerAngles.x;
            pivotTargetEulerY = Pivot.eulerAngles.y;
        }

        ApplyLimits();

        transform.localPosition = new Vector3(0, 0, Mathf.Lerp(transform.localPosition.z, -zoomTarget, Time.deltaTime * currentZoomLerpSpeed));

        Pivot.position = Vector3.Lerp(Pivot.position, pivotTargetPosition, Time.deltaTime * TranslationSmoothing);

        /*
        Vector3 currentEulers = Pivot.eulerAngles;
        float targetX = Mathf.LerpAngle(currentEulers.x, pivotTargetEulerX, Time.deltaTime * RotationSmoothing);
        float targetY = Mathf.LerpAngle(currentEulers.y, pivotTargetEulerY, Time.deltaTime * RotationSmoothing);

        //Pivot.eulerAngles = Vector3.Lerp(currentEulers, new Vector3(targetX, targetY, 0), Time.deltaTime * RotationSmoothing);
        Pivot.eulerAngles = new Vector3(targetX, targetY, 0);*/

        Pivot.rotation = Quaternion.Slerp(Pivot.rotation, Quaternion.Euler(pivotTargetEulerX, pivotTargetEulerY, 0), Time.deltaTime * RotationSmoothing);


        // Get distance from camera and target
        //float dist = Vector3.Distance(transform.position, Pivot.position);
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit))
        {

            // Get reference to the DoF settings
            var dof = postProfile.depthOfField.settings;

            // Set variables
            dof.focusDistance = Mathf.Lerp(dof.focusDistance, hit.distance, Time.deltaTime * FocusSpeed);
            dof.aperture = ApertureByZoom.Evaluate(GetZoomLevel());
            //dof.aperture = hit.distance * aperture;

            // Apply settings
            postProfile.depthOfField.settings = dof;
        }
    }

    public void LookAt(Vector3 position)
    {
        LookAt(position, zoomTarget);
    }

    public void LookAt(Vector3 position, float distance)
    {
        distance = Mathf.Clamp(distance, currentConstraints.MinZoom, currentConstraints.MaxZoom);

        pivotTargetPosition = position;
        zoomTarget = distance;
        newFocusTarget = true;
    }

    public void LookAt(Transform target)
    {
        LookAt(target, zoomTarget);
    }

    public void LookAt(Transform target, float distance)
    {
        distance = Mathf.Clamp(distance, currentConstraints.MinZoom, currentConstraints.MaxZoom);

        pivotTargetPosition = target.position;
        zoomTarget = distance;
        newFocusTarget = true;
    }

    private bool HandleZoomInput()
    {
        if (receivedScrollEvent)
        {
            float scrollwheel = Input.GetAxisRaw("Mouse ScrollWheel");

            zoomTarget -= scrollwheel * ZoomSensitivity * Time.deltaTime;
            currentZoomLerpSpeed = TranslationSmoothing;

            receivedScrollEvent = false;
            return scrollwheel != 0f;
        }
        else return false;
    }

    public float GetZoomLevel()
    {
        return Mathf.Clamp01((zoomTarget - currentConstraints.MinZoom) / (currentConstraints.MaxZoom - currentConstraints.MinZoom));
    }

    private bool HandlePanInput()
    {
        Vector2 edgePan = EnableEdgePanning ? GetEdgePanningAxis() : Vector2.zero;

        float xAxis = Mathf.Clamp(PlayerInput.Instance.Keys.Horizontal() + edgePan.x, -1f, 1f);
        float yAxis = Mathf.Clamp(PlayerInput.Instance.Keys.Vertical() + edgePan.y, -1f, 1f);

        Vector2 input = Vector2.ClampMagnitude(new Vector2(xAxis, yAxis), 1);

        Vector3 camPanForward = Vector3.ProjectOnPlane(transform.forward, Vector3.up);
        Vector3 camPanRight = Vector3.ProjectOnPlane(transform.right, Vector3.up);

        pivotTargetPosition += (camPanRight * input.x + camPanForward * input.y) * PanSensitivity * Time.deltaTime * (GetZoomLevel() + 0.1f);

        return input != Vector2.zero;
    }

    private Vector2 lastBoardPosition;
    private bool GetRightClickPanAxis(out Vector2 result)
    {
        //player currently villager selected
        if (PlayerInput.Instance.HasSelectedVillager())
        {
            result = Vector2.zero;
            return false;
        }

        //player starts right clicking
        if(Input.GetMouseButtonDown(1))
        {
            result = Vector2.zero;
            return true;
        }
        //player holding right click
        else if (Input.GetMouseButton(1))
        {
            result = new Vector2();
            return true;
        }
        //player not doing anything with right click
        else
        {
            result = Vector2.zero;
            return false;
        }
    }

    private Vector2 GetEdgePanningAxis()
    {
        Vector2 mousePos = Input.mousePosition;
        mousePos = new Vector2(Mathf.Clamp(mousePos.x, 0, Screen.width), Mathf.Clamp(mousePos.y, 0, Screen.height));
        float x = 0;
        float y = 0;


        if (mousePos.x < PixelCutoff)
        {
            x = -Mathf.Clamp01(1 - mousePos.x / PixelCutoff);

        }
        else if (mousePos.x >= Screen.width - PixelCutoff)
        {
            x = Mathf.Clamp01(1 - (Screen.width - 1 - mousePos.x) / PixelCutoff);
        }

        if (mousePos.y < PixelCutoff)
        {
            y = -Mathf.Clamp01(1 - mousePos.y / PixelCutoff);
        }
        else if (mousePos.y >= Screen.height - PixelCutoff)
        {
            y = Mathf.Clamp01(1 - (Screen.height - 1 - mousePos.y) / PixelCutoff);
        }

        return new Vector2(x, y);

    }


    private bool HandleRotateInput()
    {
        //if holding middle mouse button
        if (Input.GetMouseButton(2))
        {
            float mouseXAxis = Input.GetAxis("Mouse X") * Time.deltaTime * HorizontalRotationSensitivity;
            float mouseYAxis = Input.GetAxis("Mouse Y") * Time.deltaTime * VerticalRotationSensitivity;

            //rotate pivot
            pivotTargetEulerY += InvertHorizontal ? -mouseXAxis : mouseXAxis;
            pivotTargetEulerX += InvertVertical ? -mouseYAxis : mouseYAxis;

            return true;
        }

        return false;
    }

    private void ApplyLimits()
    {
        //x,z bounds

        Vector3 closestPointInBounds = ClosestPointInBounds(pivotTargetPosition);
        closestPointInBounds = new Vector3(closestPointInBounds.x, Pivot.position.y, closestPointInBounds.z);//make sure we dont change camera height

        pivotTargetPosition = closestPointInBounds;


        //zoom limits
        zoomTarget = Mathf.Clamp(zoomTarget, currentConstraints.MinZoom, currentConstraints.MaxZoom);

        //tilt limits
        pivotTargetEulerX = Mathf.Clamp(pivotTargetEulerX, currentConstraints.MinTiltAngle, currentConstraints.MaxTiltAngle);

    }

    private Vector3 ClosestPointInBounds(Vector3 position)
    {
        float distanceFromMin = zoomTarget - currentConstraints.MinZoom;
        float zoomRange = currentConstraints.MaxZoom - currentConstraints.MinZoom;

        float t = Mathf.Clamp01(distanceFromMin / zoomRange);
        float currentBoundsReduction = Mathf.Lerp(0, BoundsReductionAtMax, t);

        float currentBoundsRadius = BoundsRadius - currentBoundsReduction;

        return Vector3.ClampMagnitude(position - BoundsCenter.position, currentBoundsRadius);
    }

    /// <summary>
    /// called from EventForwarder when a PointerDownEvent made it past the UI
    /// </summary>
    public bool ReceiveScrollEvent(Vector2 scrollDelta)
    {
        if (scrollDelta.y != 0)
        {
            receivedScrollEvent = true;
            return true;
        }
        else return false;
    }

    public void OnEnterCommandState()
    {
        currentConstraints = CommandState;
        currentZoomLerpSpeed = PhaseTransitionSmoothing;
    }

    public void OnExitCommandState()
    {
    }

    public void OnEvaluateBuildingAction()
    {
    }

    public void OnEvaluateEnemyAction()
    {
    }

    public void OnEnterRotationState()
    {
        currentConstraints = RotationState;

        zoomTarget = RotationState.MaxZoom;
        currentZoomLerpSpeed = PhaseTransitionSmoothing;
    }

    public void OnExitRotationState()
    {
    }

    [System.Serializable]
    public class CameraConstraints
    {
        public float MinTiltAngle = 20;
        public float MaxTiltAngle = 45;
        public float MinZoom = 10;
        public float MaxZoom = 40;
    }
}

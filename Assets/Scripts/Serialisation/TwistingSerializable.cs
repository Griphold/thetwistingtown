﻿using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface TwistingSerializable {
    void OnSave();
    JObject SaveData
    {
        get;
        set;
    }
    void LoadData();
}

public class Vector3JSon
{
    public float x;
    public float y;
    public float z;

    public Vector3JSon(Vector3 vector3)
    {
        this.x = vector3.x;
        this.y = vector3.y;
        this.z = vector3.z;
    }
    
    public Vector3 GetVector3()
    {
        return new Vector3(x, y, z);
    }
}


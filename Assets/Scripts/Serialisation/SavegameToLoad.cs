﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SavegameToLoad : MonoBehaviour {

    public static void SetSaveGameToLoad(SaveGame saveGame)
    {
        GameObject saveGameObject = new GameObject("SaveGameToLoad");
        SavegameToLoad s = saveGameObject.AddComponent<SavegameToLoad>();
        s.saveGame = saveGame;
        DontDestroyOnLoad(saveGameObject);
        SceneManager.sceneLoaded += s.OnEnterNewLevel;
    }

    public SaveGame saveGame;

    private void Consume()
    {
        SceneManager.sceneLoaded -= OnEnterNewLevel;
        Destroy(this.gameObject);
    }

    private void OnEnterNewLevel(Scene scene, LoadSceneMode mode)
    {
        //find serialisationmanager and tell it to load this savegame
        if(scene.name.Equals(SaveGame.EmptySceneForLoadingGames))
        {
            SerialisationManager sm = FindObjectOfType<SerialisationManager>();
            if(sm)
            {
                sm.InitializeLoad(saveGame);
                Consume();
            }
            else
            {
                Debug.Log("No serialisation manager found but in correct scene. Deleting...");
                Consume();
            }
        }
        else
        {
            Debug.Log("Wrong scene loaded. Deleting...");
            Consume();
        }
    }
}

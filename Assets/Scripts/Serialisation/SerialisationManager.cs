﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SerialisationManager : MonoBehaviour {
    public bool Autosave = true;

    [SerializeField]
    private AlwaysPresentGameobjects References;

    [System.Serializable]
    public class AlwaysPresentGameobjects
    {
        public GameManager gameManager;
        public Board board;
        public TownManager townManager;
        public GlobalEnemySpawner enemySpawner;
        public BuildManager buildManager;
    }

    private Dictionary<NaturalResourceSupply.TypeID, NaturalResourceSupply> naturalResourceSuppliesDict = new Dictionary<NaturalResourceSupply.TypeID, NaturalResourceSupply>();
    private Dictionary<string, BoardTemplate> boardTemplateDict = new Dictionary<string, BoardTemplate>();
    private Dictionary<NavigatableActor.TypeID, NavigatableActor> actorDict = new Dictionary<NavigatableActor.TypeID, NavigatableActor>();
    private Dictionary<Building.TypeID, Building> buildingDict = new Dictionary<Building.TypeID, Building>();

    private bool ShouldAutosaveThisRound = false;//save every second round

	// Use this for initialization
	void Start () {
        CachePrefabs();
	}
    
    private void CachePrefabs()
    {
        //Cache Natural Resource Supply Prefabs
        NaturalResourceSupply[] supplyPrefabs = Resources.LoadAll<NaturalResourceSupply>("NaturalResourceSupply");
        foreach (NaturalResourceSupply supply in supplyPrefabs)
        {
            naturalResourceSuppliesDict.Add(supply.GetTypeID(), supply);
        }

        //Cache Board Templates
        BoardTemplate[] boardTemplates = Resources.LoadAll<BoardTemplate>("BoardTemplate");
        foreach (BoardTemplate bt in boardTemplates)
        {
            boardTemplateDict.Add(bt.Identifier, bt);
        }

        //cache actor prefabs
        NavigatableActor[] actorPrefabs = Resources.LoadAll<NavigatableActor>("Actor");
        foreach(NavigatableActor n in actorPrefabs)
        {
            actorDict.Add(n.GetTypeID(), n);
        }

        //cache building prefabs
        Building[] buildingPrefabs = Resources.LoadAll<Building>("Building");
        foreach(Building b in buildingPrefabs)
        {
            buildingDict.Add(b.Type(), b);
        }
    }

    public void NotifyObjectsToSave()
    {
        References.gameManager.OnSave();
        References.enemySpawner.OnSave();
        References.townManager.OnSave();
        References.board.OnSave();

        List<NaturalResourceSupply> supplies = References.townManager.GetNaturalResourceSupplies<NaturalResourceSupply>();
        foreach (NaturalResourceSupply supply in supplies)
        {
            supply.OnSave();
        }
        
        List<Villager> villagers = References.townManager.GetVillagers();
        foreach(Villager v in villagers)
        {
            v.OnSave();
        }

        List<Enemy> enemies = References.enemySpawner.GetEnemies();
        foreach(Enemy e in enemies)
        {
            e.OnSave();
        }

        List<Building> buildings = References.townManager.GetBuildings<Building>();
        foreach(Building b in buildings)
        {
            b.OnSave();
        }

        if(Autosave && ShouldAutosaveThisRound)
        {
            SaveGame("autosave");
        }

        ShouldAutosaveThisRound = !ShouldAutosaveThisRound;
    }

    public void SaveGame(string name)
    {
        Debug.Log("Saving Game: " + name);

        //gather all save data
        JObject saveGameData = new JObject();
        saveGameData.Add("Game", References.gameManager.SaveData);
        saveGameData.Add("Town", References.townManager.SaveData);
        saveGameData.Add("Board", References.board.SaveData); //Board status
        saveGameData.Add("EnemySpawner", References.enemySpawner.SaveData);//enemy Spawner(s)
        saveGameData.Add("NaturalResourceSupply", GatherSaveDataForNRSupplies());//Natural Resource supplies
        saveGameData.Add("Actor", GatherSaveDataForActors());//villagers and enemies
        saveGameData.Add("Building", GatherSaveDataForBuildings()); //buildings

        //string output = JsonConvert.SerializeObject(saveGameData);

        SaveGame s = new SaveGame(name, DateTime.Now, saveGameData);
        s.SaveToDisk();
    }

    private JArray GatherSaveDataForNRSupplies()
    {
        JArray suppliesArray = new JArray();
        List<NaturalResourceSupply> supplies = References.townManager.GetNaturalResourceSupplies<NaturalResourceSupply>(); //todo get the list of supplies better
        foreach (NaturalResourceSupply supply in supplies)
        {
            suppliesArray.Add(supply.SaveData);
        }
        return suppliesArray;
    }

    private JArray GatherSaveDataForActors()
    {
        JArray actorsArray = new JArray();
        // get villagers
        List<Villager> villagers = References.townManager.GetVillagers();
        foreach(Villager v in villagers)
        {
            actorsArray.Add(v.SaveData);
        }

        //get enemies
        List<Enemy> enemies = References.enemySpawner.GetEnemies();
        foreach(Enemy e in enemies)
        {
            actorsArray.Add(e.SaveData);
        }

        return actorsArray;
    }

    private JArray GatherSaveDataForBuildings()
    {
        JArray buildingsArray = new JArray();
        List<Building> buildings = References.townManager.GetBuildings<Building>();
        foreach(Building b in buildings)
        {
            buildingsArray.Add(b.SaveData);
        }

        return buildingsArray;
    }

    private SaveGame saveGameToLoad;
    public void InitializeLoad(SaveGame s)
    {
        saveGameToLoad = s;
        References.gameManager.InitiateLoading();
    }

    public void LoadGame()
    {
        if (saveGameToLoad == null)
            Debug.LogError("No savegame set to load. Aborting Load process.");
        ShouldAutosaveThisRound = false;

        Debug.Log("Loading Game");
        
        JObject loadedGameData = saveGameToLoad.LoadFromDisk();

        //buildmanager
        References.buildManager.LoadData();
        
        //board
        JObject boardData = (JObject) loadedGameData["Board"];
        References.board.SaveData = boardData;
        string boardTemplateID = (string) boardData["TemplateIdentifier"];
        References.board.template = GetBoardTemplate(boardTemplateID);
        References.board.LoadData();
        
        //town
        JObject townData = (JObject)loadedGameData["Town"];
        References.townManager.SaveData = townData;
        References.townManager.LoadData();

        //gamemanager
        JObject gameData = (JObject)loadedGameData["Game"];
        References.gameManager.SaveData = gameData;
        References.gameManager.LoadData();

        //enemy spawner(s)
        JObject spawnerData = (JObject)loadedGameData["EnemySpawner"];
        References.enemySpawner.SaveData = spawnerData;
        References.enemySpawner.LoadData();

        //natural resource supplies
        JArray supplies = (JArray) loadedGameData["NaturalResourceSupply"];
        foreach(JObject supplyData in supplies)
        {
            //read location
            int ringID = (int) supplyData["RingID"];
            int cellID = (int)supplyData["CellID"];
            Cell cell = FindCell(ringID, cellID);

            //read prefab type
            NaturalResourceSupply.TypeID typeID = (NaturalResourceSupply.TypeID)(int)supplyData["TypeID"];
            NaturalResourceSupply prefab = GetPrefab(typeID);

            if(cell != null && prefab != null)
            {
                //spawn and set state
                NaturalResourceSupply supply = Instantiate(prefab);
                cell.SetNaturalResource(supply);
                supply.SaveData = supplyData;
            }
        }
        
        References.enemySpawner.LoadData();

        //villagers and enemies
        JArray actors = (JArray)loadedGameData["Actor"];
        foreach(JObject actorData in actors)
        {
            NavigatableActor.TypeID typeID = (NavigatableActor.TypeID)(int)actorData["TypeID"];
            NavigatableActor prefab = GetPrefab(typeID);
            if (prefab != null)
            {
                Vector3JSon positionJson = JsonConvert.DeserializeObject<Vector3JSon>((string)actorData["Position"]);
                Vector3 actorPos = positionJson.GetVector3();

                Vector3JSon rotationJson = JsonConvert.DeserializeObject<Vector3JSon>((string)actorData["Rotation"]);
                Vector3 actorEulers = rotationJson.GetVector3();

                NavigatableActor actor = Instantiate(prefab, actorPos, Quaternion.Euler(actorEulers));
                actor.SaveData = actorData;

                actor.LoadData();
            }
        }

        //buildings
        JArray buildings = (JArray)loadedGameData["Building"];
        foreach(JObject buildingData in buildings)
        {
            //building prefab
            Building.TypeID typeID = (Building.TypeID)(int)buildingData["TypeID"];
            Building prefab = GetPrefab(typeID);

            //building position on board
            int ringID = (int) buildingData["RingID"];
            int cellID = (int) buildingData["CellID"];
            Cell cell = FindCell(ringID, cellID);


            if (prefab != null && cell != null)
            {
                Building b = Instantiate(prefab, cell.BuildingPosition);
                cell.SetBuilding(b);

                b.SaveData = buildingData;
                b.LoadData();
            }
        }
    }

    private BoardTemplate GetBoardTemplate(string templateIdentifier)
    {
        BoardTemplate bt;
        if(boardTemplateDict.TryGetValue(templateIdentifier, out bt))
        {
            return bt;
        }
        else
        {
            return null;
        }
    }

    private NaturalResourceSupply GetPrefab(NaturalResourceSupply.TypeID typeID)
    {
        NaturalResourceSupply prefabToSpawn;
        if (naturalResourceSuppliesDict.TryGetValue(typeID, out prefabToSpawn))
        {
            return prefabToSpawn;
        }
        else
        {
            return null;
        }
    }

    private NavigatableActor GetPrefab(NavigatableActor.TypeID typeID)
    {
        NavigatableActor prefabToSpawn;
        if(actorDict.TryGetValue(typeID, out prefabToSpawn))
        {
            return prefabToSpawn;
        }
        else
        {
            return null;
        }
    }

    private Building GetPrefab(Building.TypeID typeID)
    {
        Building prefabToSpawn;
        if(buildingDict.TryGetValue(typeID, out prefabToSpawn))
        {
            return prefabToSpawn;
        }
        else
        {
            return null;
        }
    }

    private Cell FindCell(int ringIndex, int cellIndex)
    {
        Board b = References.board;

        if(b)
        {
            Ring ring = b.GetRing(ringIndex);
            if(ring)
            {
                return ring.GetCell(cellIndex);
            }
        }

        return null;
    }
    
}

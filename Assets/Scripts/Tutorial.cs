﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//todo adjust when selecting mill villager first(they are slightly different)
//todo write why wheat and wood are important
//todo finish last tutorial state
public class Tutorial : MonoBehaviour
{
    public GameManager gameManager;
    public BuildManager buildManager;
    public Board board;
    public Button PhaseButton;
    public Button BuildButton;
    public Button DemolishButton;

    public Text tutorialText;
    public Text textProgress;
    public Button nextText;
    public Button previousText;

    public Ring innerRing;
    public Ring outerRing;

    public Villager lumberjackVillager;
    public Villager millVillager;
    public Lumberjack lumberjack;
    public Windmill windmill;

    public Cell cellHighlighted;

    private State currentState;
    private State nextState;

    private int shownTextIndex = 0;
    private string[] currentTexts;

    private Animator animator;

    private void Start()
    {
        nextText.onClick.AddListener(ShowNextText);
        previousText.onClick.AddListener(ShowPreviousText);

        SetCurrentState(new TurnARing());

        animator = GetComponent<Animator>();

        //Disable Keyboard Hotkeys for Tutorial
        PlayerInput.Instance.Keys.DisableHotkeys = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (nextState != null)
        {
            currentState = nextState;
            currentState.Entry(this);
            nextState = null;
        }

        currentState.Update(this);
    }

    void SetCurrentState(State state)
    {
        if (state != null)
        {
            if (currentState != null)
            {
                currentState.Exit(this);
            }

            nextState = state;
        }
    }

    void SetText(params string[] text)
    {
        currentTexts = text;

        ShowText(0);

        //Play new info animation
        animator.SetTrigger("NewInfo");

        UpdateUI();
    }

    private void ShowText(int index)
    {
        if (index < 0 || index > currentTexts.Length)
        {
            return;
        }

        shownTextIndex = index;
        tutorialText.text = currentTexts[shownTextIndex];

        UpdateUI();
    }

    private void ShowPreviousText()
    {
        ShowText(shownTextIndex - 1);
    }

    private void ShowNextText()
    {
        ShowText(shownTextIndex + 1);
    }

    private void UpdateUI()
    {
        //activate or deactivate buttons
        previousText.interactable = shownTextIndex > 0;
        nextText.interactable = shownTextIndex < currentTexts.Length - 1;

        //update text progress
        textProgress.text = (shownTextIndex + 1) + "/" + currentTexts.Length;
    }

    private bool IsCommandingToCell(Villager v, Cell c, Vector3 commandedPosition)
    {
        //Check if destination is lumberjack cell
        Cell commandedCell = v.GetCellBelow(commandedPosition);

        //If commanded to lumberjack
        if (commandedCell != null && commandedCell.Equals(c))
        {
            v.CurrentEnergy = v.MaxEnergy;
            v.commandable = false;
            return true;
        }
        else
        {
            v.CurrentEnergy = 0;
            return false;
        }
    }

    public interface State
    {
        void Entry(Tutorial tutorial);//Enters state on first update

        void Exit(Tutorial tutorial);//Exits immediately this frame

        void Update(Tutorial tutorial); //Update will be called on the next frame when changed to a certain state
    }

    private class TurnARing : State, RingListener, GameStateListener
    {
        private Tutorial tut;

        public void Entry(Tutorial tutorial)
        {
            tut = tutorial;

            tutorial.innerRing.Unlocked = false;
            tutorial.outerRing.AddListener(this);
            tutorial.gameManager.AddListener(this);
            tutorial.SetText("Welcome to The Twisting Town!\n\nThis curious little village needs your help to survive. Feed your villagers, expand the town and protect it from angry barbarians.",
                "But there is a twist! Every Rotation Phase you can rotate buildings around the town hall.\n\n" +
                "<b>Left click the outer ring and drag the mouse to turn it.</b>\n");
        }

        public void Exit(Tutorial tutorial)
        {
            tutorial.outerRing.RemoveListener(this);
            tutorial.gameManager.RemoveListener(this);
        }

        public void OnEnterCommandState()
        {
            tut.SetCurrentState(new CommandTheVillager());
        }

        public void OnEnterRotationState()
        {
        }

        public void OnEvaluateBuildingAction()
        {
        }

        public void OnEvaluateEnemyAction()
        {
        }

        public void OnExitCommandState()
        {
        }

        public void OnExitRotationState()
        {
        }

        public void OnRingReset()
        {
        }

        private bool playerStartedDraggingRing = false;

        public void OnRingTurned(float degreesClockwise)
        {
            if (!playerStartedDraggingRing)
            {
                tut.SetText("Turn the ring clockwise until an arrow appears. " +
                    "This means you have rotated it far enough.\n\n" +
                    "<b>Press the button at the top left to advance the Command Phase.</b>");

                playerStartedDraggingRing = true;
            }
        }

        public void Update(Tutorial tutorial)
        {

        }
    }

    private class CommandTheVillager : State, GameStateListener
    {
        private Tutorial tut;
        private bool hasCompletedLumberjackSubTutorial = false;
        private bool hasCompletedMillSubTutorial = false;

        public void Entry(Tutorial tutorial)
        {
            tut = tutorial;
            tut.gameManager.AddListener(this);

            tut.SetText("You have now entered the Command Phase. Here your goal is to make sure each villager has a job to do for the day.\n\n" +
                "<b>Select a villager with left click.</b>");

            tutorial.lumberjackVillager.OnSelectVillager.AddListener(OnSelectLumberjackVillager);
            tutorial.millVillager.OnSelectVillager.AddListener(OnSelectMillVillager);

            tut.PhaseButton.interactable = false;

            tut.BuildButton.interactable = false;
            tut.DemolishButton.interactable = false;
        }

        public void Exit(Tutorial tutorial)
        {
            tut.gameManager.RemoveListener(this);
        }

        public void Update(Tutorial tutorial)
        {
            tut.BuildButton.interactable = false;
            tut.DemolishButton.interactable = false;
        }

        private void OnSelectLumberjackVillager()
        {
            tut.lumberjackVillager.OnSelectVillager.RemoveAllListeners();
            tut.millVillager.OnSelectVillager.RemoveAllListeners();

            tut.millVillager.commandable = false;

            if (hasCompletedMillSubTutorial)
            {
                tut.SetText("<b>Move the villager next to the lumberjack hut by right clicking there.</b>");
            }
            else
            {
                tut.SetText("The path from the villager to your mouse shows how far they can move.\n\n<b>Move the villager next to the lumberjack hut by right clicking there.</b>");
            }


            tut.lumberjackVillager.OnCommandVillager.AddListener(OnCommandLumberjackVillager);
        }

        private void OnCommandLumberjackVillager(Vector3 destination)
        {
            //Check if destination is lumberjack cell
            if (tut.IsCommandingToCell(tut.lumberjackVillager, tut.lumberjack.OwnCell, destination))
            {
                if (hasCompletedMillSubTutorial)
                {
                    tut.SetText("The villager is now on the same board space as the lumberjack hut. They will therefore chop wood at the end of the phase.\n\n<b>Left click the lumberjack hut to show how many trees are in range.</b>");
                }
                else
                {
                    tut.SetText("Each villager can perform one action at the end of the Command Phase. Since this villager is on the same board space as the lumberjack hut, they will chop wood.\n\n" +
                                "<b>Left click the lumberjack hut to show how many trees are in range.</b>");
                }

                tut.lumberjackVillager.OnCommandVillager.RemoveListener(OnCommandLumberjackVillager);
                tut.lumberjack.OnSelectBuilding.AddListener(OnSelectLumberjack);
            }
        }

        private void OnSelectLumberjack()
        {
            hasCompletedLumberjackSubTutorial = true;
            tut.lumberjack.OnSelectBuilding.RemoveListener(OnSelectLumberjack);

            if (!hasCompletedMillSubTutorial)
            {
                tut.SetText("Left clicking any building will show you its information. The lumberjack hut shows how much wood is stored and how many trees are in range.\n\n" +
                    "<b>Select the other villager to continue.</b>");
                tut.millVillager.OnSelectVillager.AddListener(OnSelectMillVillager);
                tut.millVillager.commandable = true;
            }
            else
            {
                tut.SetText("Selecting a lumberjack hut shows how much wood is stored and how many trees are in range.\n\n" +
                    "<b>End the Command Phase by clicking the button in the top left.</b>");
                tut.PhaseButton.interactable = true;
            }

        }

        private void OnSelectMillVillager()
        {
            tut.lumberjackVillager.OnSelectVillager.RemoveAllListeners();
            tut.millVillager.OnSelectVillager.RemoveAllListeners();

            tut.lumberjackVillager.commandable = false;

            if (hasCompletedLumberjackSubTutorial)
            {
                tut.SetText("<b>Move this villager near the windmill by right clicking on its board space.</b>");
            }
            else
            {
                tut.SetText("The path from the villager to your mouse shows how far they can move.\n\n<b>Move the villager next to the windmill by right clicking there.</b>");
            }


            tut.millVillager.OnCommandVillager.AddListener(OnCommandMillVillager);
        }

        private void OnCommandMillVillager(Vector3 destination)
        {
            if (tut.IsCommandingToCell(tut.millVillager, tut.windmill.OwnCell, destination))
            {
                if (hasCompletedLumberjackSubTutorial)
                {
                    tut.SetText("The villager is now on the same board space as the windmill. They will therefore harvest wheat at the end of the phase.\n\n<b>Left click the windmill to see how much wheat will be produced.</b>");
                }
                else
                {
                    tut.SetText("Each villager can perform one action at the end of the Command Phase. Since this villager is on the same board space as the windmill, they will harvest wheat.\n\n" +
                                "<b>Left click the windmill to show how much wheat will be produced.</b>");
                }

                tut.millVillager.OnCommandVillager.RemoveListener(OnCommandMillVillager);
                tut.windmill.OnSelectBuilding.AddListener(OnSelectWindmill);
            }
        }

        private void OnSelectWindmill()
        {
            hasCompletedMillSubTutorial = true;
            tut.windmill.OnSelectBuilding.RemoveListener(OnSelectWindmill);

            if (!hasCompletedLumberjackSubTutorial)
            {
                tut.SetText("Left clicking any building will show you its information.\n\nThe windmill produces more wheat the further it was turned in the Rotation Phase.\n", 
                            "On the right you can see how much wheat will be produced at the end of the turn. On the left the amount of wheat stored in the windmill is shown.\n\n" +
                        "<b>Select the other villager to continue.</b>");

                tut.lumberjackVillager.OnSelectVillager.AddListener(OnSelectLumberjackVillager);
                tut.lumberjackVillager.commandable = true;

                //tut.millVillager.commandable = false;
            }
            else
            {
                tut.SetText("The windmill produces more wheat the further it was turned in the Rotation Phase. You can see how much will be produced on the right and the stored amount on the left.\n\n" +
                    "<b>End the Command Phase by clicking the button in the top left corner.</b>");
                tut.PhaseButton.interactable = true;
            }
        }

        public void OnEnterCommandState()
        {
            tut.PhaseButton.interactable = false;
            tut.BuildButton.interactable = false;
            tut.DemolishButton.interactable = false;
        }

        public void OnExitCommandState()
        {
            tut.innerRing.Unlocked = true;
            tut.outerRing.Unlocked = false;
            tut.SetCurrentState(new RotateAny());
        }

        public void OnEvaluateBuildingAction()
        {
        }

        public void OnEvaluateEnemyAction()
        {
        }

        public void OnEnterRotationState()
        {
        }

        public void OnExitRotationState()
        {
        }
    }

    private class RotateAny : State, GameStateListener
    {
        private Tutorial tut;

        public void Entry(Tutorial tutorial)
        {
            tut = tutorial;

            tutorial.SetText("Now your villagers will work. After they are done you will enter the Rotation Phase again.", 
                "For the purposes of this tutorial, we will now rotate a different ring.\n\n<b>Rotate the inner ring counter-clockwise and continue to the Command Phase.</b>");
            tutorial.gameManager.AddListener(this);
        }

        public void Exit(Tutorial tutorial)
        {
            tutorial.gameManager.RemoveListener(this);
        }

        public void OnExitRotationState()
        {
        }

        public void OnEnterCommandState()
        {
            tut.SetCurrentState(new SetABuildOrder());
        }

        public void OnEnterRotationState()
        {
        }

        public void OnEvaluateBuildingAction()
        {
        }

        public void OnEvaluateEnemyAction()
        {
        }

        public void OnExitCommandState()
        {
        }

        public void Update(Tutorial tutorial)
        {
        }
    }

    private class SetABuildOrder : State
    {
        private Tutorial tut;
        private BuildOrder buildOrder;
        private bool waitForInventoryFull = false;
        private bool villager1Arrived = false;
        private bool villager2Arrived = false;

        public void Entry(Tutorial tutorial)
        {
            tut = tutorial;

            tut.innerRing.Unlocked = true;
            tut.outerRing.Unlocked = true;
            tut.innerRing.Mode = Ring.TurnMode.BOTH;
            tut.outerRing.Mode = Ring.TurnMode.BOTH;

            tut.cellHighlighted.SetHighlightSettings(tut.board.HighlightSpeed, tut.board.HoverHighlightGradient, tut.board.DragHighlightGradient);

            tut.SetText("To expand your village you can select a build order from the build menu.\n\n<b>Click the hammer symbol in the bottom right corner to open the build menu.</b>");
            tut.buildManager.OnBuildMenuOpened.AddListener(OnBuildMenuOpened);

            tut.PhaseButton.interactable = false;
            tut.lumberjackVillager.commandable = false;
            tut.millVillager.commandable = false;
        }

        public void Exit(Tutorial tutorial)
        {
        }

        public void Update(Tutorial tutorial)
        {
            if (waitForInventoryFull)
            {
                if (buildOrder.Inventory.IsFull())
                {
                    OnBuildOrderFull();
                    waitForInventoryFull = false;
                }
            }
        }

        private void OnBuildMenuOpened()
        {
            tut.buildManager.OnBuildMenuOpened.RemoveListener(OnBuildMenuOpened);
            tut.SetText("In this case you have only unlocked the townhouse.\n\n<b>Left-click the townhouse in the build menu.</b>");
            tut.buildManager.OnSelectingCell.AddListener(OnStartedSelectingCell);
        }

        private void OnStartedSelectingCell()
        {
            tut.buildManager.OnSelectingCell.RemoveListener(OnStartedSelectingCell);
            tut.cellHighlighted.Highlighted = true;
            tut.SetText("Now you can select where you want to start building your townhouse.\n\n<b>Left-click on the highlighted tile to place your build order there.</b>");
            tut.buildManager.OnPlacedBuildOrder.AddListener(OnPlacedBuildOrder);
        }

        private void OnPlacedBuildOrder(BuildOrder b)
        {
            tut.buildManager.OnPlacedBuildOrder.RemoveListener(OnPlacedBuildOrder);
            buildOrder = b;
            tut.cellHighlighted.Highlighted = false;
            tut.SetText("<b>Select the build order you just placed.</b>");
            b.OnSelectBuilding.AddListener(OnSelectBuildOrder);
        }

        private void OnSelectBuildOrder()
        {
            tut.SetText("To start construction, the build order requires the correct resources. In this case you need 3 wood.\n\n<b>Let's see if the lumberjack has any.</b>");
            buildOrder.OnSelectBuilding.RemoveListener(OnSelectBuildOrder);
            tut.lumberjack.OnSelectBuilding.AddListener(OnSelectLumberjack);
        }

        private void OnSelectLumberjack()
        {
            tut.SetText("Looks like the lumberjack hut has just enough wood stored. Villagers and most buildings have inventories. If they are close enough you can directly move resources between them <i>during the Command Phase</i>.",
                "Let's try it out! First, make sure both inventories are visible.\n\n<b>Then use your left mouse button to drag and drop the wood from the lumberjack hut to the build order.</b>");
            waitForInventoryFull = true;
            tut.lumberjack.OnSelectBuilding.RemoveListener(OnSelectLumberjack);
        }

        private void OnBuildOrderFull()
        {
            tut.SetText("Once the build order has the needed resources, construction can begin. In this case, 2 villager actions are needed to complete it.\n\n<b>Move both villagers here now.</b>");
            tut.lumberjackVillager.OnCommandVillager.AddListener(OnCommandLumberjackVillager);
            tut.millVillager.OnCommandVillager.AddListener(OnCommandWindmillVillager);
            tut.lumberjackVillager.commandable = true;
            tut.millVillager.commandable = true;
        }

        private void OnCommandLumberjackVillager(Vector3 destination)
        {
            //If commanded to lumberjack
            if (tut.IsCommandingToCell(tut.lumberjackVillager, buildOrder.OwnCell, destination))
            {
                villager1Arrived = true;
                tut.lumberjackVillager.OnCommandVillager.RemoveListener(OnCommandLumberjackVillager);

                if (villager1Arrived && villager2Arrived)
                {
                    OnBothVillagersArrived();
                }
            }
        }

        private void OnCommandWindmillVillager(Vector3 destination)
        {
            if (tut.IsCommandingToCell(tut.millVillager, buildOrder.OwnCell, destination))
            {
                villager2Arrived = true;
                tut.millVillager.OnCommandVillager.RemoveListener(OnCommandWindmillVillager);

                if (villager1Arrived && villager2Arrived)
                {
                    OnBothVillagersArrived();
                }
            }
        }

        private void OnBothVillagersArrived()
        {
            tut.PhaseButton.interactable = true;
            tut.SetText("Usually only one villager can work per building. <i>Build orders are the only exception.</i> " +
                "Using multiple villagers makes construction faster.\n\n<b>End the command phase to continue.</b>");

            tut.SetCurrentState(new ShowHungerMechanics());
        }
    }

    private class ShowHungerMechanics : State, GameStateListener
    {
        private Tutorial tut;
        private bool firstRotationPhase = true;

        public void Entry(Tutorial tutorial)
        {
            tut = tutorial;


            tut.millVillager.turnsTillHunger = 2;
            tut.lumberjackVillager.turnsTillHunger = 2;

            tut.gameManager.AddListener(this);
        }

        public void Exit(Tutorial tutorial)
        {
            tut.gameManager.RemoveListener(this);
        }

        public void OnEnterCommandState()
        {
        }

        public void OnEnterRotationState()
        {
            if (!firstRotationPhase)
            {
                Building tc = tut.board.rings[0].cells[0].GetBuilding();
                PlayerInput.Instance.Select(tc);
                Camera.main.GetComponent<BirdsEyeCamera>().LookAt(tc.transform.position);
                tut.SetText("To unlock new buildings and rings you need to advance to the next age. Check the town hall for the requirements! <i>You can only advance during Command Phase!</i>",
                    "Thanks for the playing the tutorial! Now you know everything to get started.\n\n<b>You can return to the main menu using the cog button in the bottom right corner.</b>");
            }
        }

        public void OnEvaluateBuildingAction()
        {

        }

        public void OnEvaluateEnemyAction()
        {

        }

        public void OnExitCommandState()
        {
            if(firstRotationPhase)
                tut.SetText("During the actual game, you can freely decide which ring to rotate. Choose wisely which ring you want to rotate, how far and in which direction.",
                    "In this tutorial it does not really matter how you rotate, however."
                    + "\n\n<b>Rotate and advance to the next Command Phase to continue.</b>");


        }

        public void OnExitRotationState()
        {
            if (firstRotationPhase)
            {
                tut.SetText("Villagers get hungry every 5 turns. When this happens they return home and eat 3 wheat. If there is not enough, they will starve. You can check a villager's hunger by selecting them or their house.\n\n" +
                    "<b>Finish the command phase to see how this looks.</b>");
                firstRotationPhase = false;
            }

        }

        public void Update(Tutorial tutorial)
        {

        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerInput : MonoBehaviour, GameStateListener, EventForwarder.ManagedPointerDownReceiver {

    public static PlayerInput Instance;
    public const string CURSOR_DEFAULT = "CURSOR_DEFAULT";
    public const string CURSOR_BUILD = "CURSOR_BUILD";
    public const string CURSOR_DEMOLISH = "CURSOR_DEMOLISH";
    public const string CURSOR_HANDOPEN = "CURSOR_HANDOPEN";
    public const string CURSOR_HANDCLOSED = "CURSOR_HANDCLOSED";
    private Dictionary<string, Texture2D> cursorDict = new Dictionary<string, Texture2D>();

    public KeyBindings Keys;

    public struct KeyBindings
    {
        public bool DisableKeyboard;
        public bool DisableHotkeys;

        public float Horizontal()
        {
            if (DisableKeyboard) return 0.0f;
            return Input.GetAxisRaw("Horizontal");
        }

        public float Vertical()
        {
            if (DisableKeyboard) return 0.0f;
            return Input.GetAxisRaw("Vertical");
        }

        public bool Cancel()
        {
            if (DisableKeyboard) return false;
            return Input.GetKeyDown(KeyCode.Escape);
        }

        public bool Confirm()
        {
            if (DisableKeyboard) return false;
            return Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return);
        }

        public bool AdvancePhase()
        {
            if (DisableKeyboard || DisableHotkeys) return false;
            return Input.GetKeyUp(KeyCode.Space);
        }

        public bool Build()
        {
            if (DisableKeyboard || DisableHotkeys) return false;
            return Input.GetKeyDown(KeyCode.B);
        }

        public bool Demolish()
        {
            if (DisableKeyboard || DisableHotkeys) return false;
            return Input.GetKeyDown(KeyCode.N);
        }

        public bool ToggleCheats()
        {
            if (DisableKeyboard) return false;
            return (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && Input.GetKeyUp(KeyCode.F1);
        }

        public bool Cheat(int number)
        {
            if (DisableKeyboard) return false;
            switch (number)
            {
                case 1:
                    return Input.GetKeyDown(KeyCode.F2);
                case 2:
                    return Input.GetKeyDown(KeyCode.F3);
                case 3:
                    return Input.GetKeyDown(KeyCode.F4);
                case 4:
                    return Input.GetKeyDown(KeyCode.F5);
                case 5:
                    return Input.GetKeyDown(KeyCode.F6);
                default:
                    return false;
            }
        }

        public bool BuildingHotkey(Building.TypeID b)
        {
            if (DisableKeyboard || DisableHotkeys) return false;
            switch (b)
            {
                case Building.TypeID.Lumberjack:
                    return Input.GetKeyDown(KeyCode.Alpha1);
                case Building.TypeID.Windmill:
                    return Input.GetKeyDown(KeyCode.Alpha2);
                case Building.TypeID.VillagerHome:
                    return Input.GetKeyDown(KeyCode.Alpha3);
                case Building.TypeID.Storage:
                    return Input.GetKeyDown(KeyCode.Alpha4);
                case Building.TypeID.Tower:
                    return Input.GetKeyDown(KeyCode.Alpha5);
                default:
                    return false;
            }
        }
    }


    public enum Context
    {
        FREE, BUILDMANAGER_FOCUS
    }

    private Cell lastHighlightedCell = null;
    private bool HighlightCurrentlyHoveredCell = false;

    private Texture2D currentCursorImage;  

    private Context context;
    private Selectable selected;
    private bool canDragAndDrop = false;

    public void Awake()
    {
        Instance = this;
    }

    public void OnDestroy()
    {
        if(Instance == this)
            Instance = null;
    }

    // Use this for initialization
    void Start () {
        SetContext(Context.FREE);
        SetCursor(CURSOR_DEFAULT);
	}

    public void SetContext(Context context)
    {
        this.context = context;

        //if a certain script sets the context to not be free, deselect anything
        if(this.context != Context.FREE)
        {
            Deselect();
            Windowmanager.Instance.HideAll();
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (Keys.Cancel())
        {
            Windowmanager.Instance.HideAll();
            Deselect();
        }

        
        //highlighting
        if (HighlightCurrentlyHoveredCell)
            HighlightCell();

        /*if (HighlightCurrentlyHoveredRing)
            HighlightRing();*/
    }

    /// <summary>
    /// called from EventForwarder when a PointerDownEvent made it past the UI
    /// </summary>
    public bool ReceivePointerDownEvent(PointerEventData.InputButton button)
    {
        if (context != Context.FREE)
            return false;

        if (button == PointerEventData.InputButton.Left)
            HandleLeftClick();
        else if (button == PointerEventData.InputButton.Right)
            HandleRightClick();
        // middle mouse button. Do nothing
        else return false;
        // both left and right click consume the event
        return true;
    }

    private void HandleLeftClick()
    {
        //prioritise villagers
        GameObject anything = FindAnythingSelectableAtMouse();
        GameObject villager = FindVillagerAtMouse();

        GameObject objectToSelect = villager != null ? villager : anything;
        if(objectToSelect != null)
        { 
            //we hit something selectable so we select it
            if (IsSelectable(objectToSelect))
            {
                Selectable s = objectToSelect.GetComponent<Selectable>();
                Select(s);
            }
            else
            {
                if(!isInRotationState)
                {
                    Deselect();
                    Windowmanager.Instance.HideAll();
                }
            }
        }
        //we didnt hit anything so we deselect
        else
        {
            if(!isInRotationState)
            {
                Deselect();
                Windowmanager.Instance.HideAll();
            }
        }
    }

    private void HandleRightClick()
    {
        // inventory UIs should be closed in any case
        Windowmanager.Instance.HideAll();

        // check selected obj:
        // villager -> send move command
        // else -> deselect
        if (HasSelectedVillager())
        {
            RaycastHit hit = new RaycastHit();

            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000f, LayerMask.GetMask("Map")) && hit.collider != null)
            {
                Villager v = (Villager)selected;
                v.CommandMove(hit.point);
                return;
            }
        }
        // no villager selected or nothing hit
        Deselect();
    }

    public bool HasSelectedVillager()
    {
        return selected != null && selected is Villager;
    }

    public bool ShouldHighlightCurrentCell()
    {
        return HighlightCurrentlyHoveredCell;
    }

    public void SetShouldHighlightCurrentCell(bool value)
    {
        HighlightCurrentlyHoveredCell = value;

        if(HighlightCurrentlyHoveredCell == false && lastHighlightedCell != null)
        {
            lastHighlightedCell.Highlighted = false;
        }
    }

    private void HighlightCell()
    {
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000f, LayerMask.GetMask("Cells")) && hit.collider != null)
        {
            Cell hitCell = hit.collider.gameObject.GetComponent<Cell>();
            if (hitCell)
            {
                if (lastHighlightedCell)
                    lastHighlightedCell.Highlighted = false;

                hitCell.Highlighted = true;
                lastHighlightedCell = hitCell;
            }
        }
    }

    public static List<RaycastResult> RaycastWorldUI()
    {
        PointerEventData pointerData = new PointerEventData(EventSystem.current);
        pointerData.position = Input.mousePosition;
        List<RaycastResult> clickedUI = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerData, clickedUI);

        return clickedUI;
    }

    private bool clickedOnWorldSpaceUI()
    {
        //Check whether we are clicking on world space UI
        List<RaycastResult> clickedUI = RaycastWorldUI();

        clickedUI.RemoveAll(x => { return x.gameObject.CompareTag("ScreenSpace UI"); });

        return clickedUI.Count > 0;
    }

    private bool IsSelectable(GameObject gameObject)
    {
        return gameObject != null && gameObject.CompareTag("Selectable");
    }

    public void Select(Selectable s)
    {
        if (s == null)
        {
            Debug.LogError("Tried to select a null object");
            return;
        }

        //we select a different thing
        if (s != selected)
            Deselect();

        selected = s;
        s.OnSelect();

        WorldSpaceUI ui = s.GetUI();
        Windowmanager.Instance.Show(ui);

        if(s is Villager)
        {
            SetShouldHighlightCurrentCell(true);
        }
        else
        {
            SetShouldHighlightCurrentCell(false);
        }
    }

    public void Deselect()
    {
        if (selected != null)
        {
            selected.OnDeselect();
            selected = null;
        }

        SetShouldHighlightCurrentCell(false);
    }

    public Selectable GetSelected()
    {
        return selected;
    }

    
    public bool CanDragAndDrop()
    {
        return canDragAndDrop;
    }

    public void SetCursor(string cursorName)
    {
        Texture2D cursorImage;

        if (!cursorDict.TryGetValue(cursorName, out cursorImage))
        {
            cursorImage = Resources.Load<Texture2D>("UI/Cursors/" + cursorName);
            if(cursorImage != null)
                cursorDict.Add(cursorName, cursorImage);
        }
        
        //we found the image and its a new one
        if (cursorImage != null)
        {
            if(currentCursorImage != cursorImage)
            {
                Vector2 offset = cursorName.Contains("HAND") ? new Vector2(40, 40) : Vector2.zero;
                Cursor.SetCursor(cursorImage, offset, CursorMode.Auto);
                currentCursorImage = cursorImage;
            }
        }
            
        else
        {
            if(cursorName != CURSOR_DEFAULT)
                Debug.Log("Could not find cursor image for " + cursorName + ". Setting back to default.");
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
            cursorImage = null;
        }
    }

    private GameObject FindAnythingSelectableAtMouse()
    {
        // check hits:
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000f, LayerMask.GetMask("Default")) && hit.collider != null)
        {
            return hit.collider.gameObject;
        }

        return null;
    }

    private GameObject FindVillagerAtMouse()
    {
        // check hits:
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000f, LayerMask.GetMask("Villagers")) && hit.collider != null)
        {
            return hit.collider.gameObject;
        }

        return null;
    }

    public void OnEnterCommandState()
    {
        canDragAndDrop = true;
    }

    public void OnExitCommandState()
    {
        canDragAndDrop = false;
        Deselect();
        
        if (lastHighlightedCell != null)
        {
            lastHighlightedCell.Highlighted = false;
            lastHighlightedCell = null;
        }

        Windowmanager.Instance.HideAll();
    }

    public void OnEvaluateBuildingAction()
    {
        
    }

    public void OnEvaluateEnemyAction()
    {
        
    }

    private bool isInRotationState;
    public void OnEnterRotationState()
    {
        isInRotationState = true;
    }

    public void OnExitRotationState()
    {
        isInRotationState = false;


        Deselect();
        Windowmanager.Instance.HideAll();
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour, GameStateListener, TwistingSerializable
{
    public SerialisationManager serialisationManager;
    public Board board;
    public BuildManager buildManager;
    public TownManager townManager;
    public ScenarioOptions ScenarioOptions;
    private GamePhaseAlert gamePhaseAlert;

    private List<GameStateListener> gameStateListeners = new List<GameStateListener>();
    private Queue<GameStateListener> listenersToRemove = new Queue<GameStateListener>();
    private Queue<GameStateListener> listenersToAdd = new Queue<GameStateListener>();

    private State currentState;
    private State nextState;

    private int turnNumber = 0;
    private bool requestedEndTurn = false;

    // Use this for initialization
    void Start()
    {
        gamePhaseAlert = FindObjectOfType<GamePhaseAlert>();

        //if we arent loading anything
        if(nextState == null)
            SetCurrentState(new Initialize());

        //register player input
        AddListener(PlayerInput.Instance);
    }

    // Update is called once per frame
    void Update()
    {
        if (nextState != null)
        {
            currentState = nextState;
            nextState = null;
            currentState.Entry(this);
        }
        
        currentState.Update(this);


        RemoveFlaggedListeners();
        AddNewListeners();
    }

    public void AddListener(GameStateListener listener)
    {
        if (!gameStateListeners.Contains(listener))
            listenersToAdd.Enqueue(listener);
    }

    public void RemoveListener(GameStateListener listener)
    {
        listenersToRemove.Enqueue(listener);
    }

    private void RemoveFlaggedListeners()
    {
        while (listenersToRemove.Count > 0)
        {
            gameStateListeners.Remove(listenersToRemove.Dequeue());
        }
    }

    private void CleanListeners()
    {
        for (int i = gameStateListeners.Count - 1; i >= 0 ; i--)
        {
            GameStateListener gsl = gameStateListeners[i];

            if(gsl == null)
            {
                gameStateListeners.Remove(gsl);
            }
            else
            {
                MonoBehaviour gslMono = gsl as MonoBehaviour;

                if(gslMono != null && gslMono.gameObject == null)
                {
                    gameStateListeners.Remove(gsl);
                }
            }
        }
    }

    private void AddNewListeners()
    {
        while (listenersToAdd.Count > 0)
        {
            gameStateListeners.Add(listenersToAdd.Dequeue());
        }
    }

    public void OnEnterCommandState()
    {
        foreach (GameStateListener listener in gameStateListeners)
        {
            if (listener != null)
            {
                try
                {
                    listener.OnEnterCommandState();
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
        }
    }

    public void OnExitCommandState()
    {
        foreach (GameStateListener listener in gameStateListeners)
        {
            if (listener != null)
            {
                try
                {
                    listener.OnExitCommandState();
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
        }
    }

    public void OnEvaluateBuildingAction()
    {
        foreach (GameStateListener listener in gameStateListeners)
        {
            if (listener != null)
            {
                try
                {
                    listener.OnEvaluateBuildingAction();
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
        }
    }

    public void OnEvaluateEnemyAction()
    {
        foreach (GameStateListener listener in gameStateListeners)
        {
            if (listener != null)
            {
                try
                {
                    listener.OnEvaluateEnemyAction();
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
        }
    }

    public void OnEnterRotationState()
    {
        foreach (GameStateListener listener in gameStateListeners)
        {
            if (listener != null)
            {
                try
                {
                    listener.OnEnterRotationState();
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
        }


        if (IsGameOver())
        {
            SetCurrentState(new GameOver());
        }
    }

    public void OnExitRotationState()
    {
        foreach (GameStateListener listener in gameStateListeners)
        {
            if (listener != null)
            {
                try
                {
                    listener.OnExitRotationState();
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
        }
    }

    public int GetTurnNumber()
    {
        return turnNumber;
    }

    public bool IsRotationState()
    {
        return currentState.GetType().Equals(typeof(Rotation));
    }

    public bool IsCommandState()
    {
        return currentState.GetType().Equals(typeof(Command));
    }

    public bool IsGameOver()
    {
        return townManager.HasNoVillagersLeft();
    }
    
    public void InitiateLoading()
    {
        SetCurrentState(new Load());
    }

    public void EndTurn()
    {
        requestedEndTurn = true;
    }

    private bool ConsumeEndTurnRequest()
    {
        bool endTurn = requestedEndTurn;
        requestedEndTurn = false;

        return endTurn;
    }

    /*
    void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 200, 25), currentState.ToString());
    }*/

    void SetCurrentState(State state)
    {
        if (state != null)
        {
            if (currentState != null)
            {
                currentState.Exit(this);
            }

            nextState = state;
        }
    }

    //Serialisation
    public JObject SaveData { get; set; }

    public void OnSave()
    {
        SaveData = new JObject();
        SaveData.Add("Turn", turnNumber);
    }

    public void LoadData()
    {
        if (SaveData == null)
            return;

        turnNumber = (int) SaveData["Turn"];
    }

    //States
    public interface State
    {
        void Entry(GameManager gameManager);//Enters state on first update

        void Exit(GameManager gameManager);//Exits immediately this frame

        void Update(GameManager gameManager); //Update will be called on the next frame when changed to a certain state
    }

    //setup the board before the player can use it -> generate trees
    //initialize state for game objects
    class Initialize : State
    {
        private bool initialized;

        public void Entry(GameManager gameManager)
        {

            GlobalEnemySpawner.Instance.spawnBehaviour = gameManager.ScenarioOptions ? gameManager.ScenarioOptions.SpawnBehaviour : null;
            GlobalEnemySpawner.Instance.PeacefulMode = gameManager.ScenarioOptions ? gameManager.ScenarioOptions.PeacefulMode : true;

        }

        public void Exit(GameManager gameManager)
        {
        }

        public void Update(GameManager gameManager)
        {
            if (!initialized)
            {
                //PlaceStarterBuildings(gameManager);
                GenerateResources(gameManager);
                gameManager.SetCurrentState(new Rotation());

                initialized = true;
            }
        }

        private void GenerateResources(GameManager gameManager)
        {
            if (gameManager.ScenarioOptions == null || gameManager.ScenarioOptions.ResourceOptions.Count <= 0)
                return;

            foreach(ScenarioOptions.ResourceOption r in gameManager.ScenarioOptions.ResourceOptions)
            {
                GenerateSupplies(gameManager, r);
            }
        }

        private void GenerateSupplies(GameManager gameManager, ScenarioOptions.ResourceOption r)
        {
            // place forests
             NaturalResourceSupply supplyPrefab = r.SupplyPrefab;
            int amountToPlace = r.AmountToPlace;
            int MinRing = r.MinimumRing;
            int MaxRing = r.MaximumRing;

            List<Cell> allCells = gameManager.board.GetCells(MinRing, MaxRing);
            allCells.RemoveAll(x => { return x.IsOccupied(); });
            
            if(amountToPlace > allCells.Count)
            {
                amountToPlace = allCells.Count;
            }

            while (amountToPlace > 0)
            {
                int chosenIndex = UnityEngine.Random.Range(0, allCells.Count);

                NaturalResourceSupply supply = Instantiate(supplyPrefab);

                Cell cell = allCells[chosenIndex];
                cell.SetNaturalResource(supply);
                allCells.Remove(cell);

                supply.gameObject.name = supply.gameObject.name + " " + amountToPlace;

                amountToPlace--;
            }
        }


        private void PlaceStarterBuildings(GameManager gameManager)
        {
            //place center building
            Cell townCenterCell = gameManager.board.rings[0].cells[0];
            Building townCenter = Instantiate(gameManager.ScenarioOptions.TownCenter);
            townCenterCell.SetBuilding(townCenter);
            
            if (gameManager.ScenarioOptions == null || gameManager.ScenarioOptions.BuildingOptions.Count <= 0)
                return;

            //place Buildings
            foreach (ScenarioOptions.BuildingOption option in gameManager.ScenarioOptions.BuildingOptions)
            {
                int amountToPlace = option.AmountToPlace;
                List<Cell> allCells = gameManager.board.GetCells(option.MaximumRing);
                allCells.RemoveAll(x => { return x.IsOccupied(); });

                while (amountToPlace > 0)
                {
                    int chosenIndex = UnityEngine.Random.Range(0, allCells.Count);

                    Cell cell = allCells[chosenIndex];
                    if (cell.IsOccupied())
                    {
                        allCells.Remove(cell);
                        continue;
                    }

                    Building building = Instantiate(option.BuildingPrefab);
                    cell.SetBuilding(building);
                    building.OnCompletedConstruction();//let the build know it is completed

                    allCells.Remove(cell);

                    building.gameObject.name = building.gameObject.name + " " + amountToPlace;

                    amountToPlace--;
                }
            }
        }
    }

    class Load : State
    {
        public void Entry(GameManager gameManager)
        {
            gameManager.serialisationManager.LoadGame();
            gameManager.CleanListeners();
            gameManager.RemoveFlaggedListeners();
        }

        public void Exit(GameManager gameManager)
        {
        }

        public void Update(GameManager gameManager)
        {

            gameManager.SetCurrentState(new Rotation());
        }
    }

    //Player may command units and assign building orders, move resources, manage buildings
    //player must manually end his turn
    class Command : State
    {
        public void Entry(GameManager gameManager)
        {
            gameManager.OnEnterCommandState();
            ActivateAllGameObjects();

            gameManager.buildManager.SetNextState(new BuildManager.WaitingForInput());
        }

        public void Exit(GameManager gameManager)
        {
            gameManager.OnExitCommandState();
            DeactivateAllGameObjects();

            gameManager.buildManager.SetNextState(new BuildManager.Blocked());
        }

        public void Update(GameManager gameManager)
        {
            if (gameManager.ConsumeEndTurnRequest())
            {
                gameManager.SetCurrentState(new MovementFinish());
            }
        }

        private void ActivateAllGameObjects()
        {

        }

        private void DeactivateAllGameObjects()
        {

        }
    }

    //Waits only in this state if all units on the board have stopped moving
    class MovementFinish : State
    {
        public void Entry(GameManager gameManager)
        {
        }

        public void Exit(GameManager gameManager)
        {
        }

        public void Update(GameManager gameManager)
        {
            if (AllMovementFinished(gameManager))
            {
                gameManager.SetCurrentState(new Evaluation());
            }
        }

        private bool AllMovementFinished(GameManager gameManager)
        {
            foreach (Villager v in gameManager.townManager.AllVillagers)
            {
                if (v.IsMoving())
                {
                    return false;
                }
            }
            return true;
        }
    }

    //Evaluate all unit actions and building actions
    //enemy units also perform their actions here
    class Evaluation : State
    {
        public void Entry(GameManager gameManager)
        {
            gameManager.OnEvaluateBuildingAction();
        }

        public void Exit(GameManager gameManager)
        {
        }

        public void Update(GameManager gameManager)
        {
            if (ReadyToRotate(gameManager))
                gameManager.SetCurrentState(new EnemyPhaseAlert());
        }

        private bool ReadyToRotate(GameManager gameManager)
        {
            bool villagersBusy = VillagersBusy(gameManager);
            bool buildingsBusy = BuildingsBusy(gameManager);
            return !villagersBusy && !buildingsBusy;
        }

        private bool VillagersBusy(GameManager gameManager)
        {
            bool result = false;
            List<Villager> villagers = gameManager.townManager.AllVillagers;

            foreach (Villager villager in villagers)
            {
                if (villager.IsBusy())
                {
                    result = true;
                }
                else
                {
                    villager.goHomeIfHungery();
                    if (villager.IsBusy())
                    {
                        result = true;
                    }
                }
            }

            return result;
        }

        private bool BuildingsBusy(GameManager gameManager)
        {
            List<Watchtower> towers = gameManager.townManager.GetBuildings<Watchtower>();
            foreach(Watchtower w in towers)
            {
                if(w.IsBusy())
                {
                    return true;
                }
            }

            return false;
        }
    }

    class EnemyPhaseAlert : State
    {
        public void Entry(GameManager gameManager)
        {
            if(GlobalEnemySpawner.Instance.EnemyCount() >= 1)
                gameManager.gamePhaseAlert.ShowEnemyPhaseAlert();
        }

        public void Exit(GameManager gameManager)
        {
            
        }

        public void Update(GameManager gameManager)
        {
            if(!gameManager.gamePhaseAlert.IsEnemyPhaseAlertActive())
            {
                gameManager.SetCurrentState(new EnemyAttack());
            }
        }
    }

    class EnemyAttack : State
    {
        public void Entry(GameManager gameManager)
        {
            gameManager.OnEvaluateEnemyAction();
        }

        public void Exit(GameManager gameManager)
        {
            
        }

        public void Update(GameManager gameManager)
        {
            //if all enemies are done then move to next phase
            if(!GlobalEnemySpawner.Instance.AnyEnemyBusy())
            {
                gameManager.SetCurrentState(new Rotation());
            }
        }
    }

    //player may turn the rings on the board and then has to manually confirm his move
    class Rotation : State
    {
        public void Entry(GameManager gameManager)
        {
            if (!gameManager.IsGameOver())
            {
                if (gameManager.serialisationManager != null)
                {
                    gameManager.serialisationManager.NotifyObjectsToSave();
                }

                gameManager.turnNumber++;
                gameManager.board.SetPlayerCanTurn(true);
            }

            gameManager.OnEnterRotationState();
        }

        public void Exit(GameManager gameManager)
        {
            gameManager.board.SetPlayerCanTurn(false);
            gameManager.OnExitRotationState();
        }

        public void Update(GameManager gameManager)
        {
            
            if (gameManager.ConsumeEndTurnRequest() && gameManager.board.IsRotationValid())
            {
                gameManager.SetCurrentState(new Command());
            }
        }
    }

    class GameOver : State
    {
        public void Entry(GameManager gameManager)
        {
            
        }

        public void Exit(GameManager gameManager)
        {
            
        }

        public void Update(GameManager gameManager)
        {
            
        }
    }
}

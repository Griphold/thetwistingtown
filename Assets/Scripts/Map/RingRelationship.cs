﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Describes how turning ring A affects ring B
/// </summary>
[System.Serializable]
public struct RingRelationship
{
    public RingRelationship(Ring otherRing, float multiplier)
    {
        this.otherRing = otherRing;
        this.multiplier = multiplier;
    }

    public Ring otherRing;
    public float multiplier;

}

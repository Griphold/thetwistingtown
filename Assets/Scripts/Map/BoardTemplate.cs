﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(fileName = "Board", menuName = "TheTwistingTown/Board", order = 1)]
public class BoardTemplate : ScriptableObject
{
    private static float CELLOFFSET = 0.001f;
    public static float RINGINDICATOROFFSET = 0.002f;

    public string Identifier = "Default";
    public bool Debug;
    public float Overhang;
    public float HeightOffset;

    public Material DefaultCellMaterial;
    public int MeshResolutionPerCell;
    public Cell CellPrefab;

    public Material DefaultGroundMaterial;
    public RingTemplate TownCenterTemplate;
    public List<RingTemplate> rings = new List<RingTemplate>();
    public RingRelationshipOptions RingRelationships;

    [System.Serializable]
    public class RingRelationshipOptions
    {
        public int Count;
        public float MinimumMultiplier;
        public float MaximumMultiplier;

        public void Create(Board board)
        {
            //get rings
            List<Ring> rings = new List<Ring>();
            rings.AddRange(board.rings);
            
            if (rings.Count / 2f < Count)
                Count = Mathf.FloorToInt(rings.Count / 2f);
            
            if (MaximumMultiplier < MinimumMultiplier)
            {
                float temp = MaximumMultiplier;
                MaximumMultiplier = MinimumMultiplier;
                MinimumMultiplier = temp;
            }


            for (int i = 0; i < Count; i++)
            {
                Ring one = RemoveRandomRing(rings);
                Ring two = RemoveRandomRing(rings);
                float multiplier = Random.Range(MinimumMultiplier, MaximumMultiplier);

                one.ringRelationships.Add(new RingRelationship(two, multiplier));
                two.ringRelationships.Add(new RingRelationship(one, 1f/ multiplier));
            }
        }

        private Ring RemoveRandomRing(List<Ring> rings)
        {
            Ring r = rings[Random.Range(0, rings.Count)];
            rings.Remove(r);
            return r;
        }
    }

    [System.Serializable]
    public class RingTemplate
    {
        public float Thickness;
        public int NumberOfCells;
        public float MinimumRotation = 10;
        public float MaximumRotation = 90;

        //generate ring mesh with hole in middle
        public Mesh createRingMesh(float innerRadius, float outerRadius, int resolution, float NormalUVSize)
        {
            //calculate angle of each triangle fan
            float cellAngle = 360f / resolution;

            // create vertices starting from angle = 0
            Vector3 direction = Vector3.forward;

            //first half of array = inner vertices
            //second half of array = outer vertices
            Vector3[] newVertices = new Vector3[resolution * 2];
            int outerStartIndex = resolution;

            //create pairs of vertices according to specifications
            //create normals all facing up
            for (int i = 0; i < resolution; i++)
            {
                newVertices[i] = direction * innerRadius;
                newVertices[outerStartIndex + i] = direction * outerRadius;

                //turn direction for next pair
                direction = Quaternion.AngleAxis(cellAngle, Vector3.up) * direction;

            }

            //create triangle list
            int[] triangles = new int[resolution * 2 * 3]; // two slices per resolution * 3 triangle indices

            for (int i = 0; i < resolution; i++)
            {
                int innerLeft = i;
                int innerRight = (i + 1) % resolution; //% resolution so last slice uses the first two vertices
                int outerLeft = outerStartIndex + i;
                int outerRight = outerStartIndex + ((i + 1) % resolution); //% resolution so last slice uses the first two vertices

                //first slice
                triangles[6 * i] = outerRight;
                triangles[6 * i + 1] = innerLeft;
                triangles[6 * i + 2] = outerLeft;

                //second slice
                triangles[6 * i + 3] = innerLeft;
                triangles[6 * i + 4] = outerRight;
                triangles[6 * i + 5] = innerRight;
            }

            //create color uvs
            Vector2[] colorUVs = new Vector2[resolution * 2];
            for (int i = 0; i < resolution * 2; i++)
            {
                Vector3 pos = newVertices[i];
                Vector2 newUV = new Vector2(pos.x, pos.z);

                newUV = (newUV + new Vector2(outerRadius, outerRadius)) / 2; //move from [-outerRadius, outerRadius] TO [0, outerRadius]
                newUV /= outerRadius; //map to [0, 1]

                colorUVs[i] = newUV;
            }

            //create normal uvs
            Vector2[] normalUVs = new Vector2[resolution * 2];
            for (int i = 0; i < resolution * 2; i++)
            {
                Vector3 pos = newVertices[i];
                Vector2 newUV = new Vector2(pos.x, pos.z);

                newUV = (newUV + new Vector2(NormalUVSize, NormalUVSize)) / 2; //move from [-outerRadius, outerRadius] TO [0, outerRadius]
                newUV /= NormalUVSize; //map to [0, 1]

                normalUVs[i] = newUV;
            }


            // create mesh
            Mesh newMesh = new Mesh();
            newMesh.name = "Donut";
            newMesh.vertices = newVertices;
            newMesh.triangles = triangles;
            newMesh.uv = colorUVs;
            newMesh.uv2 = normalUVs;
            newMesh.RecalculateNormals();

            return newMesh;
        }

        //generate circle mesh
        public Mesh createCircleMesh(float outerRadius, int resolution, float NormalUVSize)
        {
            //calculate angle of each triangle fan
            float cellAngle = 360f / resolution;

            // create vertices starting from angle = 0
            Vector3 direction = Vector3.forward;

            //first vertex in middle
            //outer vertices following
            Vector3[] newVertices = new Vector3[resolution + 1];

            //create pairs of vertices according to specifications
            newVertices[0] = Vector3.zero;
            for (int i = 1; i < resolution + 1; i++)
            {
                newVertices[i] = direction * outerRadius;

                //turn direction for next vertex
                direction = Quaternion.AngleAxis(cellAngle, Vector3.up) * direction;
            }

            //create triangle list
            int[] triangles = new int[resolution * 3]; // one slice per resolution * 3 triangle indices

            //create triangle fans
            for (int i = 1; i < resolution; i++)
            {
                int outerLeft = i;
                int outerRight = i + 1;

                //pizza slice
                triangles[3 * i] = 0;
                triangles[3 * i + 1] = outerLeft;
                triangles[3 * i + 2] = outerRight;
            }
            //last pizza slice
            triangles[0] = 0;
            triangles[1] = resolution;
            triangles[2] = 1;

            //create color uvs
            Vector2[] colorUVs = new Vector2[resolution + 1];
            for (int i = 0; i < resolution + 1; i++)
            {
                Vector3 pos = newVertices[i];
                Vector2 newUV = new Vector2(pos.x, pos.z);

                newUV = (newUV + new Vector2(outerRadius, outerRadius)) / 2; //move from [-outerRadius, outerRadius] TO [0, outerRadius]
                newUV /= outerRadius; //map to [0, 1]

                colorUVs[i] = newUV;
            }

            Vector2[] normalUVs = new Vector2[resolution + 1];
            for (int i = 0; i < resolution + 1; i++)
            {
                Vector3 pos = newVertices[i];
                Vector2 newUV = new Vector2(pos.x, pos.z);

                newUV = (newUV + new Vector2(NormalUVSize, NormalUVSize)) / 2; //move from [-outerRadius, outerRadius] TO [0, outerRadius]
                newUV /= NormalUVSize; //map to [0, 1]

                normalUVs[i] = newUV;
            }

            // create mesh
            Mesh newMesh = new Mesh();
            newMesh.name = "Pizza";
            newMesh.vertices = newVertices;
            newMesh.triangles = triangles;
            newMesh.uv = colorUVs;
            newMesh.uv2 = normalUVs;
            newMesh.RecalculateNormals();

            return newMesh;
        }
    }

    private Mesh createCellMesh(float cellAngle, float innerRadius, float outerRadius, int resolution)
    {

        //calculate angle of each triangle fan
        float vertexAngle = cellAngle / (resolution - 1);

        // create vertices so center line of fan is on +z
        Vector3 direction = Vector3.forward;

        //first half of array = inner vertices
        //second half of array = outer vertices
        Vector3[] newVertices = new Vector3[resolution * 2];
        int outerStartIndex = resolution;

        //create pairs of vertices according to specifications
        for (int i = 0; i < resolution; i++)
        {
            newVertices[i] = direction * innerRadius;
            newVertices[outerStartIndex + i] = direction * outerRadius;

            //turn direction for next pair
            direction = Quaternion.AngleAxis(vertexAngle, Vector3.up) * direction;
        }

        //create triangle list
        int[] triangles = new int[resolution * 2 * 3]; // two slices per resolution * 3 triangle indices


        for (int i = 0; i < resolution - 1; i++)
        {
            int innerLeft = i;
            int innerRight = i + 1;
            int outerLeft = outerStartIndex + i;
            int outerRight = outerStartIndex + i + 1;

            //first slice
            triangles[6 * i] = outerRight;
            triangles[6 * i + 1] = innerLeft;
            triangles[6 * i + 2] = outerLeft;

            //second slice
            triangles[6 * i + 3] = innerLeft;
            triangles[6 * i + 4] = outerRight;
            triangles[6 * i + 5] = innerRight;
        }

        //create UV list
        Vector2[] uvs = new Vector2[resolution * 2];
        for (int i = 0; i < resolution; i++)
        {
            uvs[i] = new Vector2(Mathf.Lerp(0, 1, i * vertexAngle / cellAngle), 0);
            uvs[outerStartIndex + i] = new Vector2(Mathf.Lerp(0, 1, i * vertexAngle / cellAngle), 1);
        }

        // create mesh
        Mesh newMesh = new Mesh();
        newMesh.vertices = newVertices;
        newMesh.triangles = triangles;
        newMesh.uv = uvs;

        return newMesh;
    }

    public void Clear(Board board)
    {
        List<Ring> toDelete = board.rings;

        board.rings = new List<Ring>();

        for (int i = toDelete.Count - 1; i >= 0; i--)
        {
            DestroyImmediate(toDelete[i].gameObject);
        }
    }

    public void CreateGameObjects(Board board)
    {
        //Create town center
        CreateTownCenterRing(board);

        if (rings.Count < 1)
            return;
        
        //Create each ring according to the specification
        CreateRings(board);

        RingRelationships.Create(board);

        //debugging option to show the cells
        if (Debug)
        {
            Material white = Resources.Load<Material>("Debug_01");
            Material black = Resources.Load<Material>("Debug_02");

            //remove renderer of rings
            foreach (Ring ring in board.rings)
            {
                ring.GetComponent<Renderer>().enabled = false;

                //alternate black and white cells
                bool useWhite = true;
                foreach (Cell cell in ring.cells)
                {
                    Material mat = useWhite ? white : black;
                    cell.gameObject.AddComponent<MeshRenderer>().material = mat;

                    useWhite = !useWhite;
                }

                ring.cells[0].GetComponent<MeshRenderer>().material.color = Color.red;
            }
        }
    }

    private void CreateTownCenterRing(Board board)
    {
        float normalUVSize = TownCenterTemplate.Thickness;

        //empty gameobject which will be a ring
        GameObject townCenter = new GameObject("Town Center Circle");

        //put in center
        townCenter.transform.SetParent(board.transform, false);
        townCenter.transform.localPosition = Vector3.zero;
        townCenter.transform.rotation = Quaternion.identity;

        //player can turn it
        townCenter.gameObject.layer = LayerMask.NameToLayer("Rings");
        Ring townCenterRing = townCenter.AddComponent<Ring>();
        townCenterRing.isTownCenterRing = true;
        townCenterRing.MinimumRotation = 0;
        townCenterRing.MaximumRotation = 10000;

        //create pizza mesh
        Mesh circleMesh = TownCenterTemplate.createCircleMesh(TownCenterTemplate.Thickness, Mathf.CeilToInt(4 * Mathf.PI * TownCenterTemplate.Thickness), normalUVSize);
        townCenter.gameObject.AddComponent<MeshFilter>().mesh = circleMesh;
        townCenter.gameObject.AddComponent<MeshCollider>();
        townCenter.gameObject.AddComponent<MeshRenderer>().material = DefaultGroundMaterial;

        //additional stuff
        NavMeshModifier modifier = townCenter.gameObject.AddComponent<NavMeshModifier>();
        modifier.overrideArea = true;
        modifier.area = 1;

        //create singular cell
        Cell townCenterCell = Instantiate(CellPrefab);
        townCenterCell.gameObject.name = "Town Center Cell";

        //transform stuff
        townCenterCell.transform.SetParent(townCenter.transform, false);
        townCenterCell.transform.localPosition = Vector3.up * CELLOFFSET;
        townCenterCell.transform.rotation = Quaternion.identity;

        //add cell component
        townCenterCell.gameObject.GetComponent<MeshFilter>().mesh = circleMesh;
        townCenterCell.gameObject.GetComponent<MeshCollider>().sharedMesh = circleMesh;

        //place building spot on middle of cell
        GameObject centerBuildingSpot = new GameObject("Building Spot Town Center Cell");
        centerBuildingSpot.transform.SetParent(townCenterCell.transform, false);
        townCenterCell.BuildingPosition = centerBuildingSpot.transform;

        townCenterRing.cells.Add(townCenterCell);
        townCenterCell.Ring = townCenterRing;

        board.rings.Add(townCenterRing);
    }

    private void CreateRings(Board board)
    {
        int currentRing = 0;
        float inner = 0;
        float outer = TownCenterTemplate.Thickness;
        float normalUVSize = TownCenterTemplate.Thickness;

        foreach (RingTemplate ringTemplate in rings)
        {
            //empty gameobject which will be a ring
            GameObject newRing = new GameObject("Ring " + currentRing);

            //make sure they are all on the same spot
            newRing.transform.SetParent(board.transform, false);
            newRing.transform.position = -(currentRing + 1) * HeightOffset * board.transform.up;
            newRing.transform.rotation = Quaternion.identity;

            //add ring component
            Ring ring = newRing.AddComponent<Ring>();
            ring.gameObject.layer = LayerMask.NameToLayer("Rings");

            //set how far the player may turn this ring in a rotation phase
            ring.MinimumRotation = ringTemplate.MinimumRotation;
            ring.MaximumRotation = ringTemplate.MaximumRotation;

            inner = outer;//use outer radius of previous ring to determine new inner radius
            outer = inner + ringTemplate.Thickness;
            ring.Thickness = ringTemplate.Thickness;
            ring.InnerRadius = inner;
            ring.OuterRadius = outer;

            //create ring mesh
            //to create overhang only the inner radius is slightly decreased
            Mesh ringMesh = ringTemplate.createRingMesh(inner - Overhang, outer, (MeshResolutionPerCell - 1) * ringTemplate.NumberOfCells, normalUVSize);
            ring.gameObject.AddComponent<MeshFilter>().mesh = ringMesh;
            ring.gameObject.AddComponent<MeshCollider>();

            ring.gameObject.AddComponent<MeshRenderer>().material = DefaultGroundMaterial;

            //create first cell then copy and move
            float cellAngle = 360f / ringTemplate.NumberOfCells;

            Mesh firstCellMesh = createCellMesh(cellAngle, inner - Overhang, outer, MeshResolutionPerCell);

            int currentCell = 0;
            //create each cell
            for (int i = 0; i < ringTemplate.NumberOfCells; i++)
            {
                Cell newCell = Instantiate(CellPrefab);
                newCell.gameObject.name = "Cell " + currentRing + "-" + currentCell;

                //transform stuff
                newCell.transform.SetParent(ring.transform, false);
                newCell.transform.localPosition = Vector3.up * 0.01f;
                newCell.transform.rotation = Quaternion.identity;

                //add cell component
                MeshFilter mf = newCell.gameObject.GetComponent<MeshFilter>();
                mf.mesh = firstCellMesh;
                newCell.gameObject.GetComponent<MeshCollider>().sharedMesh = firstCellMesh;

                //rotate each cell to position
                newCell.transform.Rotate(0, cellAngle * i, 0);

                //place building spot on middle of cell
                GameObject buildingSpot = new GameObject("Building Spot " + currentRing + "-" + currentCell);
                buildingSpot.transform.SetParent(newCell.transform, false);
                buildingSpot.transform.Rotate(0, cellAngle / 2, 0);
                buildingSpot.transform.Translate(Vector3.forward * (outer + inner) / 2);
                newCell.BuildingPosition = buildingSpot.transform;

                //debugging
                //GameObject buildingPlaceholder = Instantiate(ProxyBuilding, buildingSpot.transform);

                //add cell to list
                ring.cells.Add(newCell);
                newCell.Ring = ring;

                currentCell++;
            }

            //add ring to list
            board.rings.Add(ring);

            currentRing++;
        }
    }

    private float GetTotalRadius()
    {
        float total = 0;

        if(TownCenterTemplate != null)
        {
            total += TownCenterTemplate.Thickness;
        }

        foreach(RingTemplate ring in rings)
        {
            total += ring.Thickness;
        }

        return total;
    }
   
}


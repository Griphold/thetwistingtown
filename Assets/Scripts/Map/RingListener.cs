﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface RingListener {
    void OnRingTurned(float degreesClockwise);
    void OnRingReset();
}

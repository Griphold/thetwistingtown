﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider))]

public class Cell : MonoBehaviour, GameStateListener, TwistingSerializable
{
    public bool PlayerMayBuildBuildHere = true;
    public int CellIndex;
    public Transform BuildingPosition;
    
    private Building currentBuilding;
    private NaturalResourceSupply naturalResource;

    [SerializeField]
    private List<Villager> villagers = new List<Villager>();

    public Ring Ring;

    [Header("Highlighting")]
    private bool highlighted;
    public bool Highlighted
    {
        get
        {
            return highlighted;
        }
        set
        {
            if(highlighted != value)
            {
                highlighted = value;
                AnimatingHighlight = true;
            }
        }
    }
    private bool ringDragged;
    public bool RingDragged
    {
        get
        {
            return ringDragged;
        }

        set
        {
            if(ringDragged != value)
            {
                ringDragged = value;
                AnimatingHighlight = true;
            }
        }
    }

    private Gradient HoverHighlightGradient;
    private Gradient DragHighlightGradient;
    private float HighlightSpeed;
    private Material material;
    private Color currentHighlightValue = new Color(0,0,0,0);
    private bool AnimatingHighlight = false;

    public void SetHighlightSettings(float HighlightSpeed, Gradient Hover, Gradient Drag)
    {
        this.HighlightSpeed = HighlightSpeed;
        this.HoverHighlightGradient = Hover;
        this.DragHighlightGradient = Drag;
    }

    void Start()
    {
        material = GetComponent<Renderer>().material;

        if (BuildingPosition.childCount <= 0)
            return;


        if (SaveData == null)
        {
            Transform potentialStarterObjects = BuildingPosition.GetChild(0);
            if (potentialStarterObjects)
            {
                Building b = potentialStarterObjects.GetComponent<Building>();

                if (b)
                {
                    SetBuilding(b);
                    b.OnCompletedConstruction();
                }

                NaturalResourceSupply n = potentialStarterObjects.GetComponent<NaturalResourceSupply>();
                if(n)
                {
                    SetNaturalResource(n);
                    //Debug.Log("Set starting Resource on " + gameObject.name + " to " + n.name);
                }
            }
        }
    }

    public void Update()
    {
        if(AnimatingHighlight)
            OnHighlightChanged();
    }

    private void OnHighlightChanged()
    {
        float highlightTarget = 0;
        Gradient gradient = HoverHighlightGradient;

        if (ringDragged)
        {
            highlightTarget = 1;
            gradient = DragHighlightGradient;
        }
        else if(highlighted)
        {
            highlightTarget = 1;
            gradient = HoverHighlightGradient;
        }
        else
        {
            highlightTarget = 0;
            gradient = HoverHighlightGradient;
        }

        //lerp from current color to target color
        Color targetColor = gradient != null ? gradient.Evaluate(highlightTarget) : Color.clear;
        currentHighlightValue = Color.Lerp(currentHighlightValue, targetColor, Time.deltaTime * HighlightSpeed);
        
        material.color = currentHighlightValue;

        //check if we are done animating the highlight
        if(currentHighlightValue == targetColor)
        {
            AnimatingHighlight = false;
        }
    }

    public void RegisterToCell(Villager villager)
    {
        if(!villagers.Contains(villager))
            villagers.Add(villager);
    }

    public List<Villager> GetRegisteredVillagers()
    {
        return villagers;
    }

    public Building GetBuilding()
    {
        return currentBuilding;
    }

    public void SetBuilding(Building building)
    {
        if(currentBuilding != null)
        {
            Debug.Log("Trying to set a building where there already is one in " + gameObject.name);
            return;
        }

        if(building == null)
        {
            Debug.Log("Trying to set a building which is null on " + gameObject.name);
            return;
        }

        currentBuilding = building;
        building.OwnCell = this;

        building.transform.SetParent(this.BuildingPosition, false);

        TownManager.Instance.AddBuilding(building);

        if (naturalResource != null)
        {
            naturalResource.Hide();
        }
    }

    /// <summary>
    /// Called exclusively by buildorders
    /// </summary>
    public void RemoveBuilding()
    {
        TownManager.Instance.RemoveBuilding(currentBuilding);

        currentBuilding = null;
        
        if (naturalResource != null)
        {
            naturalResource.Show();
        }
    }

    public void DemolishBuilding()
    {
        if (!currentBuilding || !currentBuilding.Demolishable)
            return;

        currentBuilding.OnDemolished();

        TownManager.Instance.RemoveBuilding(currentBuilding);
        Destroy(currentBuilding.gameObject);

        currentBuilding = null;

        if (naturalResource != null)
        {
            naturalResource.Show();
        }
    }

    public bool HasBuilding()
    {
        return currentBuilding != null;
    }

    //a cell is occupied if there is a building on it OR a natural resource that is not depleted
    public bool IsOccupied()
    {
        if (!PlayerMayBuildBuildHere)
            return true;

        if (currentBuilding != null)
            return true;

        if(naturalResource)
        {
            return !naturalResource.IsDepleted();
        }
        else
        {
            return false;
        }
    }

    public void SetNaturalResource(NaturalResourceSupply naturalResource)
    {
        this.naturalResource = naturalResource;
        naturalResource.OwnCell = this;
        naturalResource.SetPosition(BuildingPosition);
    }

    public NaturalResourceSupply GetNaturalResource()
    {
        if (currentBuilding != null)
            return null;

        return naturalResource;
    }

    public void OnEnterCommandState()
    {
        if(!IsOccupied() && naturalResource)
            naturalResource.OnEnterCommandState();
    }

    public void OnExitCommandState()
    {
        
    }

    public void OnEvaluateBuildingAction()
    {
        
    }

    public void OnEvaluateEnemyAction()
    {
        
    }

    public void OnEnterRotationState()
    {
        
    }

    public void OnExitRotationState()
    {
        villagers.Clear();
    }

    //Serialisation

    public JObject SaveData {
        get;
        set;
    }

    public void OnSave()
    {
        SaveData = new JObject();
        SaveData.Add("CellID", CellIndex);
    }

    public void LoadData()
    {
        CellIndex = (int) SaveData["CellID"];
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

public class Ring : MonoBehaviour, TwistingSerializable
{
    private const float ARROW_RESOLUTION = 1f;
    private const int ARROW_END_MARGIN_ANGLE = 2;

    public bool isTownCenterRing = false;

    [Header("Turn Limits")]
    public float MinimumRotation;
    public float MaximumRotation;
    public TurnMode Mode;

    public enum TurnMode { BOTH, CLOCKWISE, COUNTERCLOCKWISE}

    [Header("Turn Speeds")]
    public float LerpSpeed = 20;
    private float currentLerpSpeed;

    [Header("Highlighting")]
    private bool hovered;
    public bool Hovered
    {
        get { return hovered; }
        set
        {
            if (hovered != value)
            {
                hovered = value;
                SetHighlight(value, true);
            }
            else
            {
                hovered = value;
            }
        }
    }
    private bool dragged;
    public bool Dragged
    {
        get
        {
            return dragged;
        }

        set
        {
            if (dragged != value)
            {
                dragged = value;
                SetHighlight(value, true);
            }
        }
    }

    private float LeftLimit;//the furthest point the ring can be turned counter-clockwise by the player this turn
    private float LeftMinimum; //the minimum point the ring must be turned counter-clockwise by the player this turn
    private float RightLimit;//the furthest point the ring can be turned clockwise by the player this turn
    private float RightMinimum;//the minimum point the ring must be turned clockwise by the player this turn

    private float startAngle; //angle of the ring at the beginning of rotation phase
    private List<RingListener> listeners = new List<RingListener>();
    private float cumulativeAngleChanged;

    //Visual Parameters for showing limits
    [Header("Visuals")]
    public float Thickness;
    public float InnerRadius;
    public float OuterRadius;
    private Transform LimitsTransform;
    private LineRenderer leftLimitRenderer;
    private LineRenderer rightLimitRenderer;
    private CappedLineRenderer turnIndicator;
    private bool turnIndicatorFixed = false;
    private Vector3 arrowStartDirection = Vector3.zero;

    [Header("Other")]
    public int RingIndex = 0;
    public List<RingRelationship> ringRelationships = new List<RingRelationship>();
    //List of Cells (Number)
    public List<Cell> cells = new List<Cell>();
    private Ring innerNeighbor;
    private Ring outerNeighbor;

    //is this ring visible/activated
    //is this ring turnable
    private float targetAngle;
    private bool turnable = false;
    private bool unlocked = false;

    public Color UnlockedColor;
    public Color LockedColor;
    private Renderer myRenderer;
    private Color targetColor;
    private float colorLerpSpeed = 10f;

    private bool shouldShowArrow = false;

    public bool IsTurnable
    {
        get { return turnable; }
        set { turnable = value; }
    }

    public bool Unlocked
    {
        get
        {
            return unlocked;
        }

        set
        {
            unlocked = value;

        }
    }

    public Ring InnerNeighbor
    {
        get
        {
            return innerNeighbor;
        }

        set
        {
            innerNeighbor = value;
        }
    }

    public Ring OuterNeighbor
    {
        get
        {
            return outerNeighbor;
        }

        set
        {
            outerNeighbor = value;
        }
    }

    public Cell GetCell(int index)
    {
        if (index < 0 || index >= cells.Count)
        {
            return null;
        }

        return cells[index];
    }

    public void AddListener(RingListener listener)
    {
        listeners.Add(listener);
    }

    public void RemoveListener(RingListener listener)
    {
        listeners.Remove(listener);
    }

    private void RingChanged(float degrees)
    {
        foreach (RingListener listener in listeners)
        {
            listener.OnRingTurned(degrees);
        }
    }

    private void Start()
    {
        SetupLimitIndicators();
        myRenderer = GetComponent<Renderer>();
    }

    private void SetupLimitIndicators()
    {
        //create 2 line renderers for rendering the limits
        LineRenderer limitIndicator = Resources.Load<LineRenderer>("UI/RingLimitIndicator");
        CappedLineRenderer arrow = Resources.Load<CappedLineRenderer>("UI/Arrow");

        GameObject limitsGO = new GameObject(gameObject.name + " LimitsTransform");
        LimitsTransform = limitsGO.transform;
        LimitsTransform.SetParent(this.transform.parent);
        LimitsTransform.localPosition = this.transform.position;

        leftLimitRenderer = Instantiate(limitIndicator, LimitsTransform);

        rightLimitRenderer = Instantiate(limitIndicator, LimitsTransform);

        turnIndicator = Instantiate(arrow, transform);

        Vector3[] newPositions = new Vector3[2];
        newPositions[0] = new Vector3(InnerRadius + 1, 0, -BoardTemplate.RINGINDICATOROFFSET);//-0.001f so it appears above the ring
        newPositions[1] = new Vector3(OuterRadius - 1, 0, -BoardTemplate.RINGINDICATOROFFSET);

        leftLimitRenderer.SetPositions(newPositions);
        rightLimitRenderer.SetPositions(newPositions);

        newPositions[0] = new Vector3(InnerRadius, 0, -BoardTemplate.RINGINDICATOROFFSET - 0.001f);//-0.001f so it appears above the ring
        newPositions[1] = new Vector3(OuterRadius, 0, -BoardTemplate.RINGINDICATOROFFSET - 0.001f);

        SetShowLimits(false);
        SetShouldShowArrow(false);
        RecalculateArrow();
    }

    void Update()
    {
        //Turn towards target angle
        Vector3 currentAngles = transform.eulerAngles;
        float newAngle = Mathf.LerpAngle(currentAngles.y, targetAngle, Time.deltaTime * LerpSpeed);

        transform.eulerAngles = new Vector3(currentAngles.x, newAngle, currentAngles.z);

        RecalculateArrow();

        //change material color
        targetColor = unlocked ? UnlockedColor : LockedColor;
        myRenderer.material.color = Color.Lerp(myRenderer.material.color, targetColor, colorLerpSpeed * Time.deltaTime);
    }

    private void SetHighlight(bool value, bool related)
    {
        if ((!IsTurnable || !Unlocked) && value)
            return;

        foreach (Cell cell in cells)
        {
            cell.Highlighted = Hovered;
            cell.RingDragged = dragged;
        }

        if (related)
        {

            foreach (RingRelationship ringRel in ringRelationships)
            {
                ringRel.otherRing.hovered = hovered;
                ringRel.otherRing.SetHighlight(value, false);
            }
        }
    }

    public float GetCurrentAngle()
    {
        return transform.eulerAngles.y;
    }

    /// <summary>
    /// Returns how much the ring was actually turned after applying limits
    /// </summary>
    /// <param name="relativeAngle"></param>
    public float TurnRelative(float relativeAngle)
    {
        if (Unlocked && IsTurnable)
        {
            float oldTargetAngle = targetAngle;

            targetAngle += relativeAngle;
            targetAngle = ApplyLimits(targetAngle);

            float angleTurned = Mathf.DeltaAngle(oldTargetAngle, targetAngle);
            RingChanged(angleTurned);
            return angleTurned;
        }

        return 0;
    }

    public void BeginRotationPhase()
    {
        startAngle = GetCurrentAngle();

        if (Unlocked)
            IsTurnable = true;

        CalculatePlayerLimits();
    }

    public void EndRotationPhase()
    {
        IsTurnable = false;
    }

    public bool IsTurnedEnough()
    {
        return !isTownCenterRing && Mathf.Abs(GetPhaseDeltaAngle()) >= MinimumRotation;
    }

    public float TurnAmountToReachMaximum()
    {
        if (Mathf.Abs(GetPhaseDeltaAngle()) < Mathf.Abs(MinimumRotation))
            return 0;

        float result = Mathf.Clamp01((Mathf.Abs(GetPhaseDeltaAngle()) - MinimumRotation) / (MaximumRotation - MinimumRotation));

        if (result >= 0.99f)
        {
            return 1;
        }
        else
        {
            return result;
        }
    }

    public float TurnAmountToReachMinimum()
    {
        return Mathf.Clamp01(Mathf.Abs(GetPhaseDeltaAngle()) / MinimumRotation);
    }

    public void RevertRotation()
    {
        if (isTownCenterRing)
            return;

        targetAngle = startAngle;
        SetShowLimits(false, false);
        SetShouldShowArrow(false, false);
        turnIndicatorFixed = false;

        foreach (RingListener ringListener in listeners)
        {
            ringListener.OnRingReset();
        }
    }

    /// <summary>
    /// How much has the ring been turned this rotation phase
    /// </summary>
    /// <returns></returns>
    public float GetPhaseDeltaAngle()
    {
        return Mathf.DeltaAngle(startAngle, targetAngle);
    }

    private void CalculatePlayerLimits()
    {
        //get current angle
        float currentAngle = transform.eulerAngles.y;

        float currentMaxRotation = MaximumRotation;

        //go through connected rings and adjust limit
        if (ringRelationships.Count > 0)
        {
            //find smallest limit
            foreach (RingRelationship relation in ringRelationships)
            {
                float limit = relation.otherRing.MaximumRotation / Mathf.Abs(relation.multiplier);
                if (currentMaxRotation > limit)
                {
                    currentMaxRotation = limit;
                }
            }
        }

        //calculate left and right limits
        if (Mode == TurnMode.BOTH || Mode == TurnMode.COUNTERCLOCKWISE)
        {
            LeftLimit = currentAngle - currentMaxRotation;
        }
        else
        {
            LeftLimit = currentAngle - 1;
        }

        if (Mode == TurnMode.BOTH || Mode == TurnMode.CLOCKWISE)
        {
            RightLimit = currentAngle + currentMaxRotation;
        }
        else
        {
            RightLimit = currentAngle + 1;
        }

        LeftLimit = (LeftLimit + 720) % 360; //make sure left limit is within [0, 360)
        RightLimit = (RightLimit + 720) % 360;

        //recalculate visual parameter limits
        leftLimitRenderer.transform.localEulerAngles = new Vector3(90, LeftLimit - GetCurrentAngle(), 0);
        rightLimitRenderer.transform.localEulerAngles = new Vector3(90, RightLimit - GetCurrentAngle(), 0);
    }

    private float ApplyLimits(float newTargetAngle)
    {
        if (isTownCenterRing)
            return newTargetAngle;

        //make sure newTargetANgle is in [0,360)
        newTargetAngle = (newTargetAngle + 720) % 360;

        if (LeftLimit > RightLimit)
        {
            if (newTargetAngle < LeftLimit && newTargetAngle > RightLimit)
            {
                float distanceToLeft = Mathf.Abs(LeftLimit - newTargetAngle);
                float distanceToRight = Mathf.Abs(RightLimit - newTargetAngle);

                if (distanceToLeft < distanceToRight)
                {
                    return LeftLimit;
                }
                else
                {
                    return RightLimit;
                }
            }
            else
            {
                return newTargetAngle;
            }
        }
        else
        {
            return Mathf.Clamp(newTargetAngle, LeftLimit, RightLimit);
        }
    }

    public void SetShowLimits(bool value, bool includeRelated = true)
    {
        if (isTownCenterRing && value)
            return;

        if (!IsTurnable && value)
            return;

        if (leftLimitRenderer)
            leftLimitRenderer.enabled = value;
        if (rightLimitRenderer)
            rightLimitRenderer.enabled = value;

        if (includeRelated)
        {
            //show limits on related rings too
            foreach (RingRelationship r in ringRelationships)
            {
                r.otherRing.SetShowLimits(value, false);
            }
        }
    }

    public void SetShouldShowArrow(bool value, bool includeRelated = true)
    {
        if (isTownCenterRing && value)
            return;

        shouldShowArrow = value;

        if (includeRelated)
        {
            //show limits on related rings too
            foreach (RingRelationship r in ringRelationships)
            {
                r.otherRing.SetShouldShowArrow(value, false);
            }
        }
    }

    public void SetClickedPosition(Vector2 boardPosition, bool includeRelated = true)
    {
        Vector3 direction = new Vector3(boardPosition.x, transform.position.y, boardPosition.y) - transform.position;

        if (!turnIndicatorFixed)
        {
            //move limits
            float angle = Vector3.SignedAngle(Vector3.right, direction, Vector3.up);
            LimitsTransform.eulerAngles = new Vector3(0, angle - GetPhaseDeltaAngle(), 0);

            //initialize arrow position
            arrowStartDirection = direction.normalized * ((InnerRadius + OuterRadius) / 2);
            turnIndicatorFixed = true;
        }

        if (includeRelated)
        {
            //show limits on related rings too
            foreach (RingRelationship r in ringRelationships)
            {
                r.otherRing.SetClickedPosition(boardPosition, false);
            }
        }
    }

    private void RecalculateArrow()
    {
        //dont show arrow if the ring isnt turned enough
        if (!shouldShowArrow || Mathf.Abs(GetPhaseDeltaAngle()) <= 30)
        {
            if (turnIndicator)
                turnIndicator.gameObject.SetActive(false);
            return;
        }


        if (turnIndicator)
            turnIndicator.gameObject.SetActive(true);


        //calculate arrow positions
        float arrowHeightOffset = 0.2f;
        int numOfPoints = Mathf.CeilToInt((InnerRadius + OuterRadius) * Mathf.PI * (Mathf.Abs(GetPhaseDeltaAngle()) / 360f) * ARROW_RESOLUTION); //how many points
        numOfPoints = Mathf.Max(2, numOfPoints);
        Vector3[] newPoints = new Vector3[numOfPoints];
        float anglePerPoint = GetPhaseDeltaAngle() / (numOfPoints - 1);
        for (int i = 0; i < numOfPoints - 1; i++)
        {
            newPoints[i] = transform.position + Quaternion.AngleAxis(i * anglePerPoint, transform.up) * arrowStartDirection + transform.up * arrowHeightOffset;
        }

        //make last position just slightly before the limit indicator
        newPoints[numOfPoints - 1] = transform.position + Quaternion.AngleAxis((numOfPoints - 1) * anglePerPoint - ARROW_END_MARGIN_ANGLE, transform.up) * arrowStartDirection + transform.up * arrowHeightOffset;

        turnIndicator.SetPositions(newPoints);
    }

    private float GetCellAngle()
    {
        return 360f / cells.Count;
    }

    private void OnDestroy()
    {
        Destroy(LimitsTransform.gameObject);
    }

    //serialisation
    public JObject SaveData
    {
        get;
        set;
    }

    public void OnSave()
    {
        foreach(Cell c in cells)
        {
            c.OnSave();
        }

        SaveData = new JObject();
        SaveData.Add("RingIndex", RingIndex);
        //add ring relationships
        SaveData.Add("Rotation", JsonConvert.SerializeObject(new Vector3JSon(transform.eulerAngles)));
        SaveData.Add("Unlocked", Unlocked);
        JArray cellArray = new JArray();
        foreach (Cell r in cells)
        {
            cellArray.Add(r.SaveData);
        }
        SaveData.Add("Cells", cellArray);
    }

    public void LoadData()
    {
        RingIndex = (int) SaveData["RingIndex"];
        Vector3JSon rotationJson = JsonConvert.DeserializeObject<Vector3JSon>((string)SaveData["Rotation"]);
        transform.eulerAngles = rotationJson.GetVector3();
        targetAngle = GetCurrentAngle();
        Unlocked = (bool) SaveData["Unlocked"];
        JArray cellsArray = (JArray) SaveData["Cells"];

        if(cellsArray.Count != cells.Count)
        {
            Debug.Log("Failed serialisation in rings");
        }

        for (int i = 0; i < cells.Count; i++)
        {
            cells[i].SaveData = (JObject) cellsArray[i];
            cells[i].LoadData();
        }
    }
}

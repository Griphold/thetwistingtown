﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

// tod: state: turn / noturn
// check active ring
// active ring follows mouse pos
public class Board : MonoBehaviour, EventForwarder.ManagedPointerDownReceiver, TwistingSerializable
{
    public interface IBoardListener : IEventSystemHandler
    {
        void OnAnyRingChanged();
    }

    public BoardTemplate template;

    //is this interactable
    //is this visible

    private bool canTurn = false;
    private bool isTurning = false;
    private Ring selectedRing;
    private Vector2 initialRingPos;
    private Vector2 lastRingPos;
    private float initialRingAngle;
    
    //List of Rings (Number)
    public List<Ring> rings = new List<Ring>();

    [Header("Highlighting")]
    public Gradient HoverHighlightGradient;
    public Gradient DragHighlightGradient;
    [SerializeField]
    public float HighlightSpeed;
    public LayerMask BlockingObjects;

    [Header("Rings Colors")]
    public Color UnlockedColor = new Color(100f/255f, 149f/255f, 80f/255f, 1);
    public Color LockedColor = new Color(98/255f, 121/255f, 88/255f, 1);

    private List<IBoardListener> boardListeners = new List<IBoardListener>();

    // reference is set by the eventforwarder
    [System.NonSerialized]
    public EventForwarder eventForwarder;

    // Use this for initialization
    private void Start () {
        Initialize();
    }

    public void AddListener(IBoardListener listener)
    {
        boardListeners.Add(listener);
    }

    public void RemoveListener(IBoardListener listener)
    {
        boardListeners.Remove(listener);
    }

    private void Notify()
    {
        foreach(IBoardListener l in boardListeners)
        {
            l.OnAnyRingChanged();
        }
    }

    private void Initialize()
    {

        int index = 0;
        foreach(Ring ring in rings)
        {
            ring.RingIndex = index++;
            ring.UnlockedColor = UnlockedColor;
            ring.LockedColor = LockedColor;

            int cellIndex = 0;
            foreach(Cell cell in ring.cells)
            {
                cell.CellIndex = cellIndex++;
                cell.SetHighlightSettings(HighlightSpeed, HoverHighlightGradient, DragHighlightGradient);
            }
        }

        if (rings.Count <= 1)
            return;

        //set neighbors
        for (int i = 0; i < rings.Count; i++)
        {
            if(i == 0)
            {
                rings[i].OuterNeighbor = rings[i + 1];
            }
            else if(i == rings.Count - 1)
            {
                Ring inner = rings[i - 1];
                rings[i].InnerNeighbor = inner.isTownCenterRing ? null : inner;
            }
            else
            {
                Ring inner = rings[i - 1];
                rings[i].InnerNeighbor = inner.isTownCenterRing ? null : inner;
                rings[i].OuterNeighbor = rings[i + 1];
            }
        }


        //register all cells to game state
        foreach (Ring ring in rings)
        {
            foreach (Cell cell in ring.cells)
            {
                TownManager.Instance.gameManager.AddListener(cell);
            }
        }
    }

    // Update is called once per frame
    void Update () {
        if(canTurn)
        {
            HandlePlayerInput();
        }
    }
    
    public bool ReceivePointerDownEvent(PointerEventData.InputButton button)
    {
        if (!canTurn || button != 0)
            return false;

        //save clicked ring
        Ring ringClicked = GetRingClickedOn();

        // save initial pos of click
        Vector3 mousePosOnMap = GetMousePointOnMap();
        // abort if GetMousePointOnMap does not find a point
        if (mousePosOnMap.y < -1000f)
            return false;

        initialRingPos = new Vector2(mousePosOnMap.x, mousePosOnMap.z);
        lastRingPos = initialRingPos;

        //clicked any ring
        if (ringClicked && ringClicked.IsTurnable)
        {
            isTurning = true;

            //clicked a new ring
            if (selectedRing != ringClicked)
            {
                //reset all rings except the one clicked on, unless we select town center ring
                if(!ringClicked.isTownCenterRing)
                {
                    foreach (Ring ring in rings)
                    {
                        bool connectedWithSelected = ringClicked.ringRelationships.Exists(x => { return x.otherRing.Equals(ring); });

                        //either the ring is connected with the new ring OR it is the new ring
                        if (connectedWithSelected || ring == ringClicked)
                        {
                            continue;
                        }
                        //otherwise reset the rotation
                        else
                        {
                            ring.RevertRotation();
                        }
                    }
                }

                selectedRing = ringClicked;
                selectedRing.Hovered = true;
                selectedRing.Dragged = true;
                selectedRing.SetShowLimits(true, false);
                selectedRing.SetShouldShowArrow(true);
                selectedRing.SetClickedPosition(initialRingPos);

                Notify();

                PlayerInput.Instance.SetCursor(PlayerInput.CURSOR_HANDCLOSED);
            }

            return true;
        }

        return false;

    }

    private void HandlePlayerInput()
    {
        if (isTurning)
        {
            if (Input.GetMouseButton(0))
            {
                // turn controlled ring
                Vector3 mousePosOnMap = GetMousePointOnMap();
                // abort if GetMousePointOnMap does not find a point
                if (mousePosOnMap.y < -1000f)
                    return;

                Vector2 boardToMouse = new Vector2(mousePosOnMap.x, mousePosOnMap.z);

                float targetAngleRel = -Vector2.SignedAngle(lastRingPos, boardToMouse);
                TurnRing(selectedRing, targetAngleRel);
                
                lastRingPos = boardToMouse;
                Notify();
            }
            else
            {
                isTurning = false;
                selectedRing.Hovered = false;
                selectedRing.Dragged = false;
                selectedRing.SetShowLimits(false);
                selectedRing = null;

                PlayerInput.Instance.SetCursor(PlayerInput.CURSOR_HANDOPEN);
            }
        }

        DoHighlighting();
    }

    private Ring lastHighlightedRing = null;

    private void DoHighlighting()
    {
        if (canTurn && selectedRing == null)
        {
            Ring hitRing = GetRingClickedOn();
            //hit a ring, dehighlight last ring and highlight new ring
            if (hitRing && hitRing.IsTurnable)
            {
                PlayerInput.Instance.SetCursor(PlayerInput.CURSOR_HANDOPEN);

                if (lastHighlightedRing)
                {
                    lastHighlightedRing.Hovered = false;
                }

                hitRing.Hovered = true;
                lastHighlightedRing = hitRing;
            }
            //did not hit a ring, dehighlight last ring only
            else
            {

                PlayerInput.Instance.SetCursor(PlayerInput.CURSOR_DEFAULT);

                if (lastHighlightedRing)
                    lastHighlightedRing.Hovered = false;
            }
        }
    }

    private void TurnRing(Ring ring, float amtRel)
    {
        float actualTurnAngle = ring.TurnRelative(amtRel);

        foreach(RingRelationship relation in ring.ringRelationships)
        {
            relation.otherRing.TurnRelative(relation.multiplier * actualTurnAngle);
        }
    }

    public void SetPlayerCanTurn(bool canTurn)
    {
        this.canTurn = canTurn;

        if(canTurn)
        {
            PlayerInput.Instance.SetCursor(PlayerInput.CURSOR_HANDOPEN);
            
            foreach (Ring ring in rings)
            {
                ring.BeginRotationPhase();
            }
        }
        else
        {
            foreach (Ring ring in rings)
            {
                ring.EndRotationPhase();
            }

            PlayerInput.Instance.SetCursor(PlayerInput.CURSOR_DEFAULT);
            
            lastHighlightedRing = null;

            foreach(Ring ring in rings)
            {
                ring.Hovered = false;
                ring.Dragged = false;
                ring.SetShowLimits(false, false);
                ring.SetShouldShowArrow(false, false);
            }
        }
    }

    //has the player turned a ring enough to end his/her turn
    public bool IsRotationValid()
    {
        foreach(Ring ring in rings)
        {
            if(ring.IsTurnedEnough())
            {
                return true;
            }
        }

        return false;
    }

    public float GetRemainingTurnAmt()
    {
        float max = -1;
        foreach (Ring ring in rings)
        {
            float remainingAmt = ring.TurnAmountToReachMinimum();
            if (remainingAmt > max)
            {
                max = remainingAmt;
            }
        }

        return max;
    }

    public void RevertRotation()
    {
        foreach(Ring ring in rings)
        {
            ring.RevertRotation();
        }
    }

    public bool CanTurn()
    {
        return canTurn;
    }

    public Ring GetRingClickedOn(bool blockedbyObjects = true)
    {
        // abort if the mouse is over UI
        if (!eventForwarder.IsMouseAvailableForGameWorld)
            return null;

        //raycast from mouse position on camera to map
        RaycastHit hitInfo = new RaycastHit();
        LayerMask mask = LayerMask.GetMask("Rings");
        if (blockedbyObjects) { mask = LayerMask.GetMask("Rings") | BlockingObjects; }

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo, 4284383f, mask))
        {
            return hitInfo.collider.gameObject.GetComponent<Ring>();
        }

        return null;
    }


    public Vector3 GetMousePointOnMap()
    {
        //raycast from mouse position on camera to map
        RaycastHit hitInfo;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo, 1338f , LayerMask.GetMask("Map")))
        {
            return transform.TransformPoint(hitInfo.point);
        }

        return Vector3.up * -1338f;
    }

    public void UnlockRing(int index)
    {
        if (index < 0 || index >= rings.Count)
            return;

        rings[index].Unlocked = true;
    }

    public int NumberOfUnlockedRings()
    {
        int result = 0;
        foreach (Ring ring in rings)
        {
            if (ring.Unlocked)
            {
                result++;
            }
        }

        return result;
    }

    public Ring GetRing(int index)
    {
        if (index < 0 || index >= rings.Count)
            return null;

        return rings[index];
    }

    public List<Cell> GetCells()
    {
        return GetCells(0, rings.Count - 1);
    }

    /// <summary>
    /// Adds all cells excluding maxRingIndex
    /// </summary>
    /// <param name="maxRingIndex">Inclusive</param>
    /// <returns></returns>
    public List<Cell> GetCells(int maxRingIndex)
    {
        return GetCells(0, maxRingIndex);
    }

    /// <summary>
    /// Cell all cells within these ring indices
    /// </summary>
    /// <param name="minRingIndex">Inclusive</param>
    /// <param name="maxRingIndex">Inclusive</param>
    /// <returns></returns>
    public List<Cell> GetCells(int minRingIndex, int maxRingIndex)
    {
        if (minRingIndex > maxRingIndex)
            return new List<Cell>();

        if (maxRingIndex > rings.Count)
            maxRingIndex = rings.Count;

        List<Cell> cells = new List<Cell>();
        for (int i = minRingIndex; i <= maxRingIndex; i++)
        {
            cells.AddRange(rings[i].cells);
        }

        return cells;
    }

    public void Create(BoardTemplate template)
    {
        //clean up
        Debug.Log("Clearing existing rings");
        Clear();

        Debug.Log("Creating new rings");
        template.CreateGameObjects(this);
    }
    
    private void Clear()
    {
        template.Clear(this);
    }

    //serialisation

    public JObject SaveData {
        get;
        set;
    }

    public void OnSave()
    {
        foreach(Ring r in rings)
        {
            r.OnSave();
        }

        SaveData = new JObject();
        SaveData.Add("TemplateIdentifier", template.Identifier);
        JArray ringsArray = new JArray();
        foreach(Ring r in rings)
        {
            ringsArray.Add(r.SaveData);
        }
        SaveData.Add("Rings", ringsArray);
    }

    public void LoadData()
    {
        Clear();

        //create the board from board template
        Create(template);
        Initialize();

        //call load data on all rings when done
        JArray ringsArray = (JArray)SaveData["Rings"];

        if(ringsArray.Count != rings.Count)
        {
            Debug.Log("Failed serialisation in rings");
        }

        for (int i = 0; i < ringsArray.Count; i++)
        {
            rings[i].SaveData = (JObject) ringsArray[i];
            rings[i].LoadData();
        }

        Initialize();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using UnityEngine;

public class TownManager : MonoBehaviour, TwistingSerializable
{
    public static TownManager Instance;

    public GameManager gameManager;
    public bool cheats = false;
    public Villager VillagerPrefabForCheating;
    public Enemy EnemyPrefabForCheating;

    //list of all villagers
    public List<Villager> AllVillagers = new List<Villager>();
    //list of all buildings
    public List<Building> AllBuildings = new List<Building>();


    /// <summary>
    /// used for determining who gets to eat and who starves
    /// before the villagers start walking home to eat.
    /// 
    /// Villagers must call ConsumeReservedFood to reset this.
    /// </summary>
    private int reservedFoodThisTurn = 0;
    private int villagerIDCounter;

    private Age currentAge;

    void Start()
    {
        cheats = false;

        if (currentAge == null)
        {
            Age age = new StartingAge();
            age.OnEnter(this);

            currentAge = age;
        }
    }

    private void Update()
    {
        if(PlayerInput.Instance.Keys.ToggleCheats())
        {
            cheats = !cheats;
            if(cheats)
            {
                Debug.Log("Cheats are now enabled.");
            }
            else
            {
                Debug.Log("Cheats are now disabled.");
            }
        }

        if(cheats)
        {
            //copy a villager (debugging)
            if (PlayerInput.Instance.Keys.Cheat(3))
            {
                Vector3 position = gameManager.board.GetMousePointOnMap();

                Villager v = Instantiate(VillagerPrefabForCheating, position, Quaternion.identity);
                v.home = null;
                v.CurrentEnergy = v.MaxEnergy;

                AddVillager(v);
            }

            if (PlayerInput.Instance.Keys.Cheat(4))
            {
                Vector3 position = gameManager.board.GetMousePointOnMap();
                GlobalEnemySpawner.Instance.SpawnEnemyAt(position);
            }

            if (PlayerInput.Instance.Keys.Cheat(5))
            {
                AdvanceToNextAgeWithoutRequirements();
            }
        }
    }

    public void AdvanceToNextAge()
    {
        if (!CanEnterNextAge())
        {
            Debug.Log("Resource requirements not fulfilled for next age or there is no next age.");
            return;
        }

        AdvanceToNextAgeWithoutRequirements();
    }

    private void AdvanceToNextAgeWithoutRequirements()
    {
        Age nextAge = currentAge.GetNextAge();
        if (nextAge == null)
            return;

        //consume the age's resources
        foreach (ResourceRequirement r in currentAge.GetNextAge().GetResourceCosts())
        {
            ConsumeResource(r.type, r.amount);
        }

        nextAge.OnEnter(this);

        currentAge = nextAge;
    }

    public bool CanEnterNextAge()
    {
        if (!gameManager.IsCommandState())
            return false;

        Age nextAge = currentAge.GetNextAge();

        if (currentAge == null || nextAge == null)
        {
            return false;
        }

        //check town accessible totals for the next ages resource requirements
        foreach (ResourceRequirement r in nextAge.GetResourceCosts())
        {
            if (GetTownAccessibleTotal(r.type) < r.amount)
                return false;
        }

        //check enough villagers
        if (AllVillagers.Count < nextAge.GetVillagerRequirement())
            return false;

        return true;
    }

    public Age GetNextAge()
    {
        return currentAge.GetNextAge();
    }

    // Use this for initialization
    void OnEnable()
    {
        Instance = this;
    }

    void OnDisable()
    {
        Instance = null;
    }

    public void AddVillager(Villager villager)
    {
        if (AllVillagers.Contains(villager))
            throw new System.Exception("Villager is already registered");

        AllVillagers.Add(villager);
        gameManager.AddListener(villager);

        if (villager.ID == -1)
        {
            villager.ID = villagerIDCounter++;

            Debug.Log("New VIllager with ID " + villager.ID);
        }
    }

    public void RemoveVillager(Villager villager)
    {
        AllVillagers.Remove(villager);
        gameManager.RemoveListener(villager);
    }

    public void AddBuilding(Building building)
    {
        if (AllBuildings.Contains(building))
            return;

        AllBuildings.Add(building);
        gameManager.AddListener(building);
    }

    public void RemoveBuilding(Building building)
    {
        AllBuildings.Remove(building);
        gameManager.RemoveListener(building);
    }

    public Villager GetVillager(int ID)
    {
        return AllVillagers.Find(v => { return v.ID == ID;});
    }

    public List<Villager> GetVillagers()
    {
        return new List<Villager>(AllVillagers);
    }

    public List<Villager> GetVillagers(Vector3 position, float radius, bool sortByDistance = true)
    {
        List<Villager> result = GetVillagers();

        //check distance
        result.RemoveAll(x =>
        {
            float dist = Vector3.Distance(position, x.transform.position);

            return dist > radius;
        });

        if (sortByDistance)
        {
            result.Sort((a, b) =>
            {
                float aDist = Vector3.Distance(position, a.transform.position);
                float bDist = Vector3.Distance(position, b.transform.position);

                return (int)Mathf.Sign(aDist - bDist);
            });
        }

        return result;
    }


    public List<T> GetBuildings<T>() where T : Building
    {
        //get all cells
        List<Cell> cells = gameManager.board.GetCells();

        //get only cells with this building type
        List<Cell> allTCells = cells.FindAll(x => { return x.GetBuilding() as T != null; });

        //get all the buildings on these cells
        List<T> allTBuildings = new List<T>();
        foreach (Cell cell in allTCells)
        {
            allTBuildings.Add(cell.GetBuilding() as T);
        }

        return allTBuildings;
    }

    public List<T> GetBuildings<T>(Vector3 position, float radius, bool sortByDistance = true) where T : Building
    {
        List<T> allTBuildings = GetBuildings<T>();

        //check distance
        allTBuildings.RemoveAll(x =>
        {
            float dist = Vector3.Distance(position, x.transform.position);

            return dist > radius;
        });

        if (sortByDistance)
        {
            allTBuildings.Sort((a, b) =>
            {
                float aDist = Vector3.Distance(position, a.transform.position);
                float bDist = Vector3.Distance(position, b.transform.position);

                return (int)Mathf.Sign(aDist - bDist);
            });
        }

        return allTBuildings;
    }

    public List<T> GetNaturalResourceSupplies<T>() where T : NaturalResourceSupply
    {
        //get all cells
        List<Cell> cells = gameManager.board.GetCells();

        //get only cells with this resource type
        List<Cell> allTCells = cells.FindAll(x => { return x.GetNaturalResource() as T != null; });

        //get all the supplies on these cells
        List<T> allTSupplies = new List<T>();
        foreach (Cell cell in allTCells)
        {
            allTSupplies.Add(cell.GetNaturalResource() as T);
        }

        return allTSupplies;
    }

    public List<T> GetNaturalResourceSupplies<T>(Vector3 position, float radius, bool sortByDistance = true) where T : NaturalResourceSupply
    {
        List<T> allTSupplies = GetNaturalResourceSupplies<T>();

        //check distance
        allTSupplies.RemoveAll(x =>
        {
            float dist = Vector3.Distance(position, x.transform.position);

            return dist > radius;
        });

        if (sortByDistance)
        {
            allTSupplies.Sort((a, b) =>
            {
                float aDist = Vector3.Distance(position, a.transform.position);
                float bDist = Vector3.Distance(position, b.transform.position);

                return (int)Mathf.Sign(aDist - bDist);
            });
        }

        return allTSupplies;
    }

    public bool ConsumeResource(Resource resource, int amount)
    {
        //look for resource
        int total = GetTownAccessibleTotal(resource);

        if (total >= amount)
        {
            //we assume there is enough
            RemoveResource(resource, amount);

            return true;
        }
        else
        {
            return false;
        }
    }

    //villagers never have townaccessible inventories
    public int GetTownAccessibleTotal(Resource resource)
    {
        int amount = 0;

        foreach (Building building in AllBuildings)
        {
            if (building.Inventory)
                amount += building.Inventory.GetTownAccessibleTotal(resource);
        }

        return amount;
    }

    /// <summary>
    /// returns how many resources of the specified type 
    /// are available to the player in the entire town.
    /// This excludes already allocated resources e.g. in BuildOrders.
    /// </summary>
    public int GetPlayerAvailableTotal(Resource resource)
    {
        int amount = 0;
        foreach (Building building in AllBuildings)
        {
            if (building.Inventory)
                amount += building.Inventory.GetPlayerAvailableTotal(resource);
        }
        foreach (Villager villager in AllVillagers)
        {
            amount += villager.GetInventory().GetPlayerAvailableTotal(resource);
        }

        return amount;
    }

    //TODO make event-based so we dont have to calculate totals each frame
    //TODO always get inventory through property getter
    //TODO refactor total and remove methods in inventory and inventoryslot
    public int GetTotal(Resource resource)
    {
        int amount = 0;

        foreach (Building building in AllBuildings)
        {
            if (building.Inventory)
                amount += building.Inventory.GetTotal(resource);
        }

        foreach (Villager villager in AllVillagers)
        {
            amount += villager.GetInventory().GetTotal(resource);
        }

        return amount;
    }


    /// <summary>
    /// Use this when consuming reserved food. 
    /// Clears the reserved amount,
    /// and consumes the specified amount
    /// from townaccessible inventories.
    /// </summary>
    public bool ConsumeReservedFood(int amount)
    {
        if (amount == 0) return true;

        int available = GetTownAccessibleTotal(Resource.WHEAT);
        if (available < amount)
            Debug.LogError("Tried to consume reserved food, but there wasn't enough left. Who stole my lunch?");
        else
        {
            reservedFoodThisTurn -= amount;
            return ConsumeResource(Resource.WHEAT, amount);
        }
        return false;
    }


    private void RemoveResource(Resource resource, int amount)
    {
        foreach (Building building in AllBuildings)
        {
            if (building.Inventory != null)
                amount -= building.Inventory.Remove(resource, amount, true);
        }
    }

    public bool HasNoVillagersLeft()
    {
        return AllVillagers.Count <= 0;
    }

    //Serialisation
    public JObject SaveData { get; set; }

    public void OnSave()
    {
        SaveData = new JObject();
        //save age here
        SaveData.Add("Age", currentAge.GetName());
        SaveData.Add("villagerIDCounter", villagerIDCounter);
    }

    public void LoadData()
    {
        AllVillagers.Clear();
        AllBuildings.Clear();

        List<Age> allAges = GetAllAges();
        Age anAge = allAges[0];
        anAge.OnEnter(this);
        string savedAge = (string) SaveData["Age"];

        while (!anAge.GetName().Equals(savedAge))
        {
            anAge = anAge.GetNextAge();
            anAge.OnEnter(this, false);
        }

        currentAge = anAge;

        villagerIDCounter = (int)SaveData["villagerIDCounter"];
    }

    private List<Age> GetAllAges()
    {
        List<Age> allAges = new List<Age>();
        Age currentAge = new StartingAge();
        allAges.Add(currentAge);

        while(currentAge.GetNextAge() != null)
        {
            allAges.Add(currentAge);
            currentAge = currentAge.GetNextAge();
        }

        return allAges;
    }
}

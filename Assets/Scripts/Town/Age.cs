﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Age
{
    protected AgeEditor LoadAgeCosts()
    {
        AgeEditor ages = Resources.Load<AgeEditor>("AgeCosts");

        if (ages != null)
        {
            return ages;
        }
        else
        {
            Debug.LogError("Could not find AgeCosts in Resources");
            return new AgeEditor();
        }
    }

    public abstract string GetName();

    public abstract List<ResourceRequirement> GetResourceCosts();

    public abstract int GetVillagerRequirement();

    public abstract void OnEnter(TownManager town, bool playEffect = true);

    public abstract Age GetNextAge();

    public abstract string GetNewUnlocksText();

    protected void LoadAndPlayEffect(string effectName, TownManager town)
    {
        //Instantiate Celebration Effect
        GameObject celebrationEffect = Resources.Load<GameObject>(effectName);
        TownCenter townCenter = town.GetBuildings<TownCenter>()[0];
        GameObject.Instantiate(celebrationEffect, townCenter.transform.position, celebrationEffect.transform.rotation);
    }
}

public class StartingAge : Age
{
    public override string GetName()
    {
        return "Yarn Age";
    }

    public override string GetNewUnlocksText()
    {
        return "";
    }

    public override Age GetNextAge()
    {
        return new SecondAge();
    }

    public override List<ResourceRequirement> GetResourceCosts()
    {
        return new List<ResourceRequirement>();
    }

    public override int GetVillagerRequirement()
    {
        return 0;
    }

    public override void OnEnter(TownManager town, bool playEffect = true)
    {
        //do stuff for the start
        Debug.Log("Entered " + GetName());
        town.gameManager.board.UnlockRing(0);
        town.gameManager.board.UnlockRing(1);
        town.gameManager.board.UnlockRing(2);
    }
}

public class SecondAge : Age
{
    public override string GetName()
    {
        return "Wool Age";
    }

    public override string GetNewUnlocksText()
    {
        return "+ 2 New Rings\n" +
            "+ Storage House\n" +
            "+ Watch Tower";
    }

    public override Age GetNextAge()
    {
        //it would look like this for the last age
        return new ThirdAge();
    }

    public override List<ResourceRequirement> GetResourceCosts()
    {
        return LoadAgeCosts().AgeTwoCost;
    }

    public override int GetVillagerRequirement()
    {
        return LoadAgeCosts().AgeTwoVillagerRequired;
    }

    public override void OnEnter(TownManager town, bool playEffect = true)
    {
        //add new building formulas
        Debug.Log("Entered " + GetName());
        town.gameManager.board.UnlockRing(3);
        town.gameManager.board.UnlockRing(4);

        BuildFormula bf = town.gameManager.buildManager.formulaDict[Building.TypeID.Storage];

        town.gameManager.buildManager.MakeFormulaAvailable(bf);

        BuildFormula t = town.gameManager.buildManager.formulaDict[Building.TypeID.Tower];

        town.gameManager.buildManager.MakeFormulaAvailable(t);

        if(playEffect)
            LoadAndPlayEffect("Effects/CelebrationEffect", town);
    }
}

public class ThirdAge : Age
{
    public override string GetName()
    {
        return "Knitted Age";
    }

    public override string GetNewUnlocksText()
    {
        return "+ ????";
    }

    public override Age GetNextAge()
    {
        return null;
    }

    public override List<ResourceRequirement> GetResourceCosts()
    {
        return LoadAgeCosts().AgeThreeCost;
    }

    public override int GetVillagerRequirement()
    {
        return LoadAgeCosts().AgeThreeVillagersRequired;
    }

    public override void OnEnter(TownManager town, bool playEffect = true)
    {
        //add new building formulas
        Debug.Log("Entered " + GetName());

        //Win if we reach the third age
        GamePhaseAlert alerts = GameObject.FindObjectOfType<GamePhaseAlert>();
        if (alerts) { alerts.ShowWinAlert(); }
        
        town.gameManager.board.UnlockRing(5);
        town.gameManager.board.UnlockRing(6);
        
        if(playEffect)
            LoadAndPlayEffect("Effects/CelebrationEffect", town);
    }
}

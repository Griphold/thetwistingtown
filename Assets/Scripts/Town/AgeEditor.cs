﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Ages", menuName = "TheTwistingTown/AgesEditor", order = 3)]
public class AgeEditor : ScriptableObject {
    public List<ResourceRequirement> AgeTwoCost = new List<ResourceRequirement>();
    public int AgeTwoVillagerRequired = 5;

    public List<ResourceRequirement> AgeThreeCost = new List<ResourceRequirement>();
    public int AgeThreeVillagersRequired = 10;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {
    public delegate void OnArrowArrived(Attackable attackable);

    [Header("Animation")]
    public float TravelTime = 5f;
    public AnimationCurve ArcBehaviour;

    private Vector3 startPosition;
    private Vector3 endPosition;
    private float timeElapsed = 0f;
    private Vector3 lastPosition;
    private Attackable target;
    private OnArrowArrived arrowArrivedCallback;
    private bool arrived = false;

    // Use this for initialization
    void Start () {
        lastPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (arrived)
            return;

        timeElapsed += Time.deltaTime;

        if(timeElapsed < TravelTime)
        {
            float t = Mathf.Clamp01(timeElapsed / TravelTime);
            
            //calc position
            transform.position = GetPosition(t);

            //calculate rotation
            Vector3 arrowDirection = transform.position - lastPosition;
            transform.rotation = Quaternion.LookRotation(arrowDirection);

            lastPosition = transform.position;
        }
        else
        {
            ArrivedAtTarget();
        }
	}

    private Vector3 GetPosition(float t)
    {
        //calculate position
        Vector3 linearPosition = Vector3.Lerp(startPosition, endPosition, t);
        float yOffSetFromArc = ArcBehaviour.Evaluate(t);

        return linearPosition + yOffSetFromArc * Vector3.up;
    }

    public void Init(Vector3 startPosition, Vector3 endPosition, Attackable target, OnArrowArrived callback)
    {
        this.startPosition = startPosition;
        this.endPosition = endPosition;
        this.target = target;
        this.arrowArrivedCallback = callback;

        transform.position = startPosition;
    }

    private void ArrivedAtTarget()
    {
        arrived = true;
        if(target.gameObject() != null)
        {
            Transform t = target.gameObject().transform;
            if(t.gameObject.layer.Equals(LayerMask.NameToLayer("Enemies")))
            {
                GameObject head = FindInDescendants(t.gameObject, "Head");
                if (head)
                {
                    t = head.transform;
                }
            }

            transform.SetParent(t);
        }

        arrowArrivedCallback(target);
    }

    static private GameObject FindInDescendants (GameObject go, string name)
    {
        if (go == null) return null;
        GameObject comp = go;

        if (comp.name == name)
            return comp;

        int numOfChildren = go.transform.childCount;
        int index = 0;
        while(index < numOfChildren)
        {
            GameObject obj = FindInDescendants(comp.transform.GetChild(index++).gameObject, name);
            if(obj != null)
            {
                return obj;
            }
        }
        return null;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Attackable {

    /// <summary>
    /// Gets Current Health of Attackable
    /// </summary>
    /// <returns></returns>
    int GetHealth();

    /// <summary>
    /// Has this attack killed the attackable
    /// </summary>
    /// <param name="damage">Amount of damage done</param>
    /// <returns></returns>
    bool Attack(int damage);

    /// <summary>
    /// Returns if this attackable is alive
    /// </summary>
    /// <returns></returns>
    bool isAlive();

    void OnHealthChanged(int delta);
}

public static class AttackableMethods
{
    public static GameObject gameObject(this Attackable attackable)
    {
        if (attackable == null)
            return null;
        if (attackable as MonoBehaviour == null)
            return null;

        return (attackable as MonoBehaviour).gameObject;  //every attackable is a gameobject in our game so this cast will always work
    }
}

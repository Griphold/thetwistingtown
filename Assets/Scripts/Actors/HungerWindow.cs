﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HungerWindow : MonoBehaviour {
    private static HungerWindow HungerWindowPrefab;
  
    public static HungerWindow CreateHungerWindow(Villager villager)
    {
        if(HungerWindowPrefab == null)
        {
            HungerWindowPrefab = Resources.Load<HungerWindow>("UI/HungerWindow");
            if(HungerWindowPrefab == null)
            {
                Debug.LogError("Could not find HungerWindow Prefab in Resources/UI folder.");
                return null;
            }
        }

        HungerWindow h = Instantiate(HungerWindowPrefab, villager.transform.position + HungerWindowPrefab.offset, Quaternion.identity);
        h.villager = villager;
        h.transform.SetParent(villager.transform);

        return h;
    }

    [SerializeField]
    private Vector3 offset;
    [SerializeField]
    private Text text;
    private Villager villager;
    private Transform mainCamera;
    private Animator animator;

    [SerializeField]
    private float duration = 2.5f;
    private float lifetime;

    // Use this for initialization
    void Start ()
    {
        lifetime = duration;
        animator = GetComponent<Animator>();
        mainCamera = Camera.main.transform;
        text.text = "" + villager.turnsTillHunger;
    }
	
	// Update is called once per frame
	void Update () {

        //always face camera
        transform.LookAt(transform.position - mainCamera.rotation * Vector3.back, mainCamera.rotation * Vector3.up);

        if (lifetime > 0.0f)
        {
            lifetime -= Time.deltaTime;

            if (lifetime <= 0.0f)
            {
                animator.SetTrigger("Close");
            }
        }
    }

    public void OnClose()
    {
        Destroy(this.gameObject);
    }
}

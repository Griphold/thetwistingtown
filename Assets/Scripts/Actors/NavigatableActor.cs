﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(PathDrawer))]
public abstract class NavigatableActor : MonoBehaviour, GameStateListener, Attackable, TwistingSerializable
{
    public enum TypeID
    {
        Villager, Enemy
    }

    protected NavMeshAgent agent;

    protected PathDrawer pathDrawer;

    [Header("Navigatable Actor:")]
    public float MaxEnergy = 3;
    /// <summary>
    /// base variable for the CurrentEnergy property.
    /// USE ONLY if you DONT want to update the animator.
    /// </summary>
    [SerializeField]
    private float _CurrentEnergy;
    /// <summary>
    /// automatically updates the energy float in the animator
    /// </summary>
    public float CurrentEnergy
    {
        get { return _CurrentEnergy; }
        set
        {
            _CurrentEnergy = value;
            animator.SetFloat("Energy", _CurrentEnergy / MaxEnergy);
        }
    }

    // For is moving it has to be check if the agent rotates or moves
    private Vector3 lastPosition = Vector3.zero;
    private Quaternion lastRotation = Quaternion.identity;

    public float WalkSpeed = 5.0f;
    public float WalkSpeedTired = 2.0f;


	// Is the actor currently moving?
    protected bool isMoving =false;
	// Is the actor currently commandable? 
    public bool commandable = false;
	// The Actor may move
    protected bool canMove = false;
    // The Actor was commanded to Move
    protected bool hasMoveCommand = false;


    //Animation
    public Animator animator;

    [Header("Health")]
    public int MaxHealth = 1;
    public int currentHealth = 1;

    // Navigation
    protected Vector3[] allPathCorners;
    protected Vector3[] reachablePathCorners;
    protected Vector3 currentDestination;
    

    public virtual void Start()
    {
        pathDrawer = GetComponent<PathDrawer>();
        agent = GetComponent<NavMeshAgent>();
        commandable = false;
		CurrentEnergy = MaxEnergy;
        lastPosition = transform.position;
        lastRotation = transform.rotation;
    }
    public virtual void Update()
    {
        if (lastPosition != this.transform.position || lastRotation!= this.transform.rotation)
        {
            float amountMoved = Math.Abs(Vector3.Distance(lastPosition, transform.position));
			lastPosition = this.transform.position;
            lastRotation = this.transform.rotation;
           
            isMoving = true;
            if (commandable)
            {
                CurrentEnergy -= amountMoved;
            }
            if (CurrentEnergy < 0)
            {
                CurrentEnergy = 0;
            }

            // Set Current Energy in Animator
            // replaced with CurrentEnergy prop
            // animator.SetFloat("Energy", CurrentEnergy / MaxEnergy);
            
            //Lower Walking Speed Based on Energy
            agent.speed = Mathf.Lerp(WalkSpeedTired, WalkSpeed, CurrentEnergy / MaxEnergy);

            //Set Current movement Speed in Animator
            animator.SetFloat("Speed", agent.velocity.magnitude);
        }
        else
        {
           isMoving = false;
        }

        /*if (!IsMoving() && canMove && !commandable)
        {
           agent.enabled = false;
           canMove = false;
        }*/

        // If the agent currently has a MoveCommand but has reached his target, then he no longer has a moveCommand
        if (hasMoveCommand && isCloseToDestination())
        {
            hasMoveCommand = false;
        }
        // When the agent does not play an animation or has a movecommand the locks at the current position
        if (isIdle())
        {
            agent.enabled = false;

            //Set Speed to 0 in Animator
            animator.SetFloat("Speed", 0.0f);
        }
        
    }
    protected virtual bool isIdle()
    {
        return agent.enabled && !hasMoveCommand && !IsBusy();
    }

    public void OnDrawGizmos()
    {
        if(IsBusy())
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawCube(transform.position, Vector3.one);
        }
    }

    protected bool isCloseToDestination()
    {
        return Vector2.Distance(new Vector2(agent.destination.x, agent.destination.z), new Vector2(transform.position.x, transform.position.z)) < 0.1f;
    }

    public void lockAndDeactivateAgent()
    {
        lockToCell(transform.position);
        agent = GetComponent<NavMeshAgent>();
        agent.enabled = false;
    }
    protected void stopAgent()
    {
        if (agent.enabled)
        {
            SetDestination(transform.position);
        }
    }

    protected virtual void OnDestroy()
    {
        if(TownManager.Instance != null) TownManager.Instance.gameManager.RemoveListener(this);
    }

    protected void SetDestination(Vector3 destination)
    {
        hasMoveCommand = true;
        agent.enabled = true;
        agent.SetDestination(destination);
    }

    public abstract TypeID GetTypeID();

    protected void NavigateTo(Vector3 Destination)
    {
        SetDestination(Destination);
    }
    protected void lockToCell(Vector3 pos)
    {
        this.transform.parent = GetCellBelow(pos).transform;
    }
    
    public Cell GetCellBelow(Vector3 pos)
    {
        RaycastHit ray;
        int layerMask = 1 << LayerMask.NameToLayer("Cells");
        if (!Physics.Raycast(pos + new Vector3(0f, 0.5f, 0), new Vector3(0, -1, 0), out ray, Mathf.Infinity, layerMask))
        {
            Physics.Raycast(pos + new Vector3(0.1f, 0.5f, 0), new Vector3(0, -1, 0), out ray, Mathf.Infinity, layerMask);
        }

        if (ray.collider != null)
            return ray.collider.gameObject.GetComponent<Cell>();
        else
            return null;
    }
    public abstract bool IsBusy();
    public bool IsMoving()
    {
        return isMoving;
    }

    public virtual void OnEnterCommandState()
    {
        this.transform.parent = null;

        agent.enabled = true;
        CurrentEnergy = MaxEnergy;
        commandable = true;
        canMove = true;
    }
    public virtual void OnExitCommandState()
    {
        CurrentEnergy = MaxEnergy;
        commandable = false;
        canMove = false;
        if (reachablePathCorners==null ||reachablePathCorners.Length == 0)
        {
            lockToCell(transform.position);
        }
        else
        {
            lockToCell(reachablePathCorners[reachablePathCorners.Length - 1]);
        }
    }

    public virtual void OnEnterRotationState()
    {
        agent.enabled = false;
        canMove = false;
    }

    public void OnExitRotationState()
    {

    } 

    public virtual void OnEvaluateBuildingAction()
    {
        return;
    }
    public virtual void OnEvaluateEnemyAction()
    {
    }


    // Returns the finalDestination for the currentTurn. The Variable reachedCorners returns how many corners have been reached
    protected Vector3 TryReachDestination(Vector3 destination, out int reachedCorners, out UnityEngine.AI.NavMeshPath path)
    {
        path = new UnityEngine.AI.NavMeshPath();
        agent.CalculatePath(destination, path);
        float testEnergie = CurrentEnergy;
        Vector3 finalDestination = destination;
        reachedCorners = 0;
        for (int i = 1; i < path.corners.Length; i++)
        {
            float pathPartLength = Vector3.Distance(path.corners[i - 1], path.corners[i]);
            if (pathPartLength < testEnergie)
            {
                reachedCorners = i;
                testEnergie -= pathPartLength;
            }
            else
            {
                finalDestination = path.corners[i - 1] + Vector3.Normalize(path.corners[i] - path.corners[i - 1]) * (testEnergie);
                reachedCorners = i;
                break;
            }
        }

        return finalDestination;
    }

    #region Attackable stuff

    public int GetHealth()
    {
        return currentHealth;
    }

    public bool Attack(int damage)
    {
        if (damage <= 0)
            return false;

        int healthBeforeDamage = currentHealth;
        currentHealth -= damage;
        if (currentHealth < 0)
            currentHealth = 0;
        int delta = healthBeforeDamage - currentHealth;

        OnHealthChanged(-delta);
        return currentHealth <= 0 && delta > 0;
    }

    public bool isAlive()
    {
        return currentHealth > 0;
    }

    public virtual void OnHealthChanged(int delta)
    {
        if (delta == 0)
            return;


        Vector2 randomModifier = UnityEngine.Random.insideUnitCircle;
        ResourcePopup.CreateResourcePopup(Resource.HEALTH, delta, transform.position + transform.up * 3 + new Vector3(randomModifier.x, 0, randomModifier.y));
    }

    //Serialisation
    public JObject SaveData { get; set; }

    public virtual void OnSave()
    {
        SaveData = new JObject();
        SaveData.Add("TypeID", (int) GetTypeID());
        SaveData.Add("Position", JsonConvert.SerializeObject(new Vector3JSon(transform.position)));
        SaveData.Add("Rotation", JsonConvert.SerializeObject(new Vector3JSon(transform.eulerAngles)));
        SaveData.Add("MaximumEnergy", MaxEnergy);
        SaveData.Add("CurrentEnergy", CurrentEnergy);
        SaveData.Add("MaximumHealth", MaxHealth);
        SaveData.Add("CurrentHealth", currentHealth);
    }

    public virtual void LoadData()
    {
        Vector3JSon positionJson = JsonConvert.DeserializeObject<Vector3JSon>((string)SaveData["Position"]);
        transform.position = positionJson.GetVector3();
        lastPosition = transform.position;

        Vector3JSon rotationJson = JsonConvert.DeserializeObject<Vector3JSon>((string)SaveData["Rotation"]);
        transform.eulerAngles = rotationJson.GetVector3();
        lastRotation = transform.rotation;

        MaxEnergy = (float)SaveData["MaximumEnergy"];
        CurrentEnergy = (float)SaveData["CurrentEnergy"];

        MaxHealth = (int) SaveData["MaximumHealth"];
        currentHealth = (int) SaveData["CurrentHealth"];
    }
    #endregion


    private void OnCollisionExit(Collision collision)
    {
        
    }

    public NavMeshAgent GetAgent()
    {
        return agent;
    }
    

}
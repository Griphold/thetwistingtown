﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Villager : NavigatableActor, Selectable
{
    public UnityEvent OnSelectVillager;
    public CommandEvent OnCommandVillager = new CommandEvent();
    public class CommandEvent : UnityEvent<Vector3> { }

    /// <summary>
    /// reference to the health display on the building's UI.
    /// this is NOT a separate UI window.
    /// </summary>
    [SerializeField]
    private HealthUI healthUI;

    [Header("Villager:")]
    [SerializeField]
    private VillagerUI UI;

    public VillagerHome home;
    [SerializeField]
    private Inventory inventory;
    private bool isSelected;

    private string villagerName;
    private int id = -1;

    /// <summary>
    /// name of the villager
    /// </summary>
    public string Name { get { return villagerName; } }

    public int ID
    {
        get
        {
            return id;
        }

        set
        {
            id = value;
        }
    }


    // Hunger
    private bool isHungry;
    public int maxTurnsTillHunger = 1;
    public int turnsTillHunger;
    private bool firstHungerCheckThisTurn = true;

    // Eating
    [SerializeField]
    private int foodAmountForOneMeal = 1;

    //animations
    private Queue<AnimationItem> AnimationQueue = new Queue<AnimationItem>();
    private AnimationItem currentAnimation;
    private Vector3 originalPosition;
    private Quaternion originalRotation;
    private bool busy = false;  // Villager is currently animating
    private bool isWalkingToAnimation = false; // Villager started to walk to an animation position
    private bool isRotatingToAnimation = false; // Villager started to rotate to the animation rotation
    private AnimationData animationData;
    private float rotationSpeed = 5f;
    private bool animationQueueIsRuning = false;

    [Header("Effects & Animation")]
    public GameObject DeathEffect;
    public Color HighlightedColor = Color.yellow;
    public Renderer[] RenderersToHighlight;
    [Header("popups when consuming food")]
    [Tooltip("If > 0: \nindividual -1 popups will spawn over this duration. " +
        "\nThe first spawns immediately, the last spawn at the end of the duration. " +
        "\nIf <= 0: \na single popup with -X (consumed amount) will be spawned. " +
        "\n |time| is treated as offset from the start off the eat animation.")]
    [SerializeField]
    private float popupSpawnTime;
    [SerializeField]
    private Transform popupSpawnLocation;

    // Use this for initialization
    public override void Start()
    {
        base.Start();

        animationData = new AnimationData();
        isSelected = false;
        isHungry = false;
        inventory.Owner = this.gameObject;

        if(SaveData == null)
        {
            villagerName = CharacterNames.GetNextVillagerName();
            name = name.Replace("Clone", villagerName);
            turnsTillHunger = maxTurnsTillHunger;
        }

        UI.Initialize(this);
        healthUI.Initialize(MaxHealth);
        healthUI.SetDisplayedHealth(currentHealth);
    }
    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        // Updates the path while the villager is moving
        if (IsMoving() && canMove&&agent.enabled)
        {
            if (allPathCorners != null && allPathCorners.Length != 0)
            {
                //Vector3 x = allPathCorners[allPathCorners.Length - 1];
                reachablePathCorners = agent.path.corners;
                UnityEngine.AI.NavMeshPath path = new UnityEngine.AI.NavMeshPath();
                
                agent.CalculatePath(allPathCorners[allPathCorners.Length - 1], path);
                allPathCorners = path.corners;
                pathDrawer.UpdateDrawPath(allPathCorners, reachablePathCorners);
            }
        }
        // Draws the planing path
        if (!IsMoving() && canMove && isSelected)
        {
           RaycastHit hit =new RaycastHit();
            // does the ray hit an object
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000f, LayerMask.GetMask("Map")))
            {
                UnityEngine.AI.NavMeshPath path = new UnityEngine.AI.NavMeshPath();
                Vector3 destination = hit.point;
                if (isOnNavMesh(destination, out destination, path, 1))
                {
                    
                    int reachedCorners;
                    Vector3 finalDestinationForThisTurn = TryReachDestination(destination, out reachedCorners,out path);
                    Vector3[] reachablePathCorners = reachablePathCornersThisTurn(path, reachedCorners, finalDestinationForThisTurn);
                    pathDrawer.DrawPath(path.corners,finalDestinationForThisTurn,reachablePathCorners);

                }
            }
        }



        ////---- Animation part

        // Checks if the Navmesh has been changed during Animation
        if (isWalkingToAnimation && agent.destination != animationData.finalPosition)
        {
            // If so it is checked if the destination is still on the navmesh and if not, the closest point on the navmesh is taken.
            moveAnimationDestinationToNavmesh(4);
        }

        // Gets the position whith out y.The hight can be ignored
        Vector3 myPosNoY = new Vector3(transform.position.x, 0, transform.position.z);
        Vector3 finalPosNoY = new Vector3(animationData.finalPosition.x, 0, animationData.finalPosition.z);

        // The Villager has reached the animation place
        

        if (isWalkingToAnimation && Vector3.Distance(myPosNoY, finalPosNoY) < 0.25f)
        {
            isWalkingToAnimation = false;
            rotateToAnimationRot();
        }
        else if (isRotatingToAnimation)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, animationData.finalRotation, rotationSpeed);
        }

        if (isRotatingToAnimation && Quaternion.Angle(transform.rotation, animationData.finalRotation) < 1f)
        {
            isRotatingToAnimation = false;
            ContinueAnimation();
        }

    }
    protected override bool isIdle()
    {
        return base.isIdle()&&!isSelected;
    }

    //call this when villager dies or his house is deleted
    public void OnDeath()
    {
        TownManager.Instance.RemoveVillager(this);
    }

    public void SpawnDeathEffect()
    {
        Instantiate(DeathEffect, transform.position, DeathEffect.transform.rotation, null);
    }

    // isSeclected?
    // is in CommandFase?
    // is he not hungry?
    public void CommandMove(Vector3 destination)
    {
        if (!isSelected)
        {
            return;
        }
        if (!commandable)
        {
            return;
        }
        //hides UI when a move command is given
        UI.HideViaManager();
        
        UnityEngine.AI.NavMeshPath path = new UnityEngine.AI.NavMeshPath();
        
        // If the final location is not on the Navmesh
        if (!isOnNavMesh(destination,out destination,path,40))
        {
            return;
        }
        // Destroys the old Path
        stopAgent();
        pathDrawer.DestroyPath();

        agent.enabled = true;
        #region Command Event
        if (OnCommandVillager != null)
        {
            try
            {
                OnCommandVillager.Invoke(destination);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
        #endregion

        int reachedCorners;
        currentDestination = TryReachDestination(destination, out reachedCorners, out path);
        allPathCorners = path.corners;
        reachablePathCorners= reachablePathCornersThisTurn(path, reachedCorners, currentDestination);
        pathDrawer.DrawPath(path.corners, currentDestination, reachablePathCorners);
        agent.velocity = Vector3.zero;
        if (CurrentEnergy > 0)
        {
            NavigateTo(currentDestination);
        }


    }
    // Checks if the destination is on the NavMesh if not he locks for a point that is in samlingRadius distance. If he can not find one while sampling he returns flase
    // else he returns true
    private bool isOnNavMesh(Vector3 destination,out Vector3 newDestination, UnityEngine.AI.NavMeshPath path, float samplingRadius)
    {
        newDestination = destination;
        // If the final location is not on the Navmesh
        if (!agent.CalculatePath(destination, path))
        {
            UnityEngine.AI.NavMeshHit hit;
            if (!UnityEngine.AI.NavMesh.SamplePosition(destination, out hit, agent.height * samplingRadius, UnityEngine.AI.NavMesh.AllAreas))
            {
                return false;
            }
            newDestination = hit.position;
        }
        return true;
    }


    private Vector3[] reachablePathCornersThisTurn(UnityEngine.AI.NavMeshPath path, int reachedCorners, Vector3 destinationThisTurn)
    {
        Vector3[] reachablePathCorners = new Vector3[reachedCorners + 1];
        for (int i = 0; i < reachedCorners; i++)
        {
            reachablePathCorners[i] = path.corners[i];
        }
        reachablePathCorners[reachedCorners] = destinationThisTurn;
        return reachablePathCorners;
    }

    public void OnSelect()
    {
        foreach (Renderer r in RenderersToHighlight)
        {
            r.material.SetColor("_RimColor", HighlightedColor);
            r.material.SetFloat("_RimStrength", 1.0f);
        }


        pathDrawer.unHidePath();
        isSelected = true;
        agent.enabled = true;

        SFX.PlayAtPosition(SFXCategory.Villager_Select, transform.position);

        if (OnSelectVillager != null)
        {
            OnSelectVillager.Invoke();
        }
    }

    public void OnDeselect()
    {
        foreach (Renderer r in RenderersToHighlight)
        {
            r.material.SetColor("_RimColor", Color.black);
            r.material.SetFloat("_RimStrength", 0.0f);
        }
        
        pathDrawer.hidePath();
        isSelected = false;
    }

    public bool IsSelected()
    {
        return isSelected;
    }

    public WorldSpaceUI GetUI()
    {
        return UI;
    }

    public Inventory GetInventory()
    {
        return inventory;
    }

    private void hungerCheck()
    {
        if (turnsTillHunger <= 1)// it should never be smaller then 0
        {
            isHungry = true;
        }
    }
    // Ki
    public void goHomeIfHungery()
    {
        if (!firstHungerCheckThisTurn)
        {
            return;
        }
        if (isHungry)
        {
            firstHungerCheckThisTurn = false;
            // Kills the villager if he does not have a home
            if (home == null)
            {
                killVillager();
                return;
            }
            UnityEngine.AI.NavMeshHit hit;
            UnityEngine.AI.NavMesh.SamplePosition(home.GetVillagerSpawnPoint().position, out hit, agent.height * 2, UnityEngine.AI.NavMesh.AllAreas);
            //NavigateTo(hit.position);
            UnityEngine.AI.NavMeshPath path = new UnityEngine.AI.NavMeshPath();
            agent.enabled = true;
            agent.CalculatePath(hit.position, path);
            reachablePathCorners = path.corners;
            allPathCorners = path.corners;

            goHome();
        }
    }

    private void goHome()
    {
        AnimationItem goHomeMovment = new AnimationItem(null, "GOHOME", home.GetVillagerSpawnPoint().position, home.GetVillagerSpawnPoint().rotation, null, checkIfThereIsEnoughFood);
        this.QueueAnimation(goHomeMovment);
    }

    private void checkIfThereIsEnoughFood(Villager villager, VillagerInteractable interactable)
    {
        // calculate required townaccessible amount (villager inventory has priority)
        int requiredFromTown = getFoodAmountForOneMeal();
        requiredFromTown = System.Math.Max(requiredFromTown - inventory.GetTotal(Resource.WHEAT), 0);

        // try to reserve it
        if(TownManager.Instance.GetTownAccessibleTotal(Resource.WHEAT)>= requiredFromTown)
        {
            queueEatAnimation();
        }
        else
        {
            // not enough available -> kill self. don't consume anything
            this.killVillager();
        }
    }


    // Kills the villager and removes him from the TownManager
    public void killVillager()
    {
        AnimationItem killAnimation = new AnimationItem(null,"Die",transform.position,transform.rotation,null,deleteVillager);
        this.QueueAnimation(killAnimation);
    }

    private void deleteVillager(Villager villager, VillagerInteractable interactable)
    {
        OnDeath();
        Destroy(this.gameObject);
    }

    private void queueEatAnimation()
    {
        AnimationItem eatAnimation = new AnimationItem(home, "Eat", home.GetVillagerSpawnPoint().transform.position, home.transform.rotation, null, resetHungerTimer);
        this.QueueAnimation(eatAnimation);        
    }

    // called at the start of the eat animation (via animation callback)
    public void OnEat()
    {
        // consume resources
        StartCoroutine(FoodPopupsCoroutine());
    }

    /// <summary>
    /// immediately tries to consume the specified amount of food.
    /// Prioritizes food from the inventory.
    /// Kills the villager if not successful.
    /// </summary>
    private void ConsumeFood(int amount)
    {
        // first try to consume from own inventory
        amount -= inventory.Remove(Resource.WHEAT, amount);
        
        if(amount > 0)
        {
            if (!TownManager.Instance.ConsumeReservedFood(amount))
            {
               // killVillager();
            }
        }
    }

    IEnumerator FoodPopupsCoroutine()
    {
        float consumeDelayInSec = 0.5f;
        if(popupSpawnTime <= 0 || foodAmountForOneMeal < 2) // Spawn a single popup 
        {
            // delay if specified
            if(popupSpawnTime < 0)
                yield return new WaitForSeconds(Mathf.Abs(popupSpawnTime));
            // spawn the popup and consume the food
            ResourcePopup.CreateResourcePopup(Resource.WHEAT, -foodAmountForOneMeal, popupSpawnLocation.position);
            if (consumeDelayInSec > 0)
                yield return new WaitForSeconds(consumeDelayInSec);
            ConsumeFood(foodAmountForOneMeal);
        }
        else // spawn individual -1 popups over the specified duration
        {
            float interval = popupSpawnTime / (float)(foodAmountForOneMeal - 1);

            // adjust inteval length to account for consume delay
            if (consumeDelayInSec > interval)
                consumeDelayInSec = interval;
            interval -= consumeDelayInSec;

            for(int i = 0; i < foodAmountForOneMeal; i++)
            {
                ResourcePopup.CreateResourcePopup(Resource.WHEAT, -1, popupSpawnLocation.position);
                yield return new WaitForSeconds(consumeDelayInSec);
                ConsumeFood(1);
                yield return new WaitForSeconds(interval);
            }
        }
    }


    private void resetHungerTimer(Villager villager, VillagerInteractable interactable)
    {
        lockToCell(home.transform.position);
        GetCellBelow(home.transform.position).RegisterToCell(this);
        turnsTillHunger = maxTurnsTillHunger;
        isHungry = false;
        // refresh UIs showing the hunger timer
        UI.UpdateUIElements();
        home.GetUI().UpdateUIElements();
    }

    public int getFoodAmountForOneMeal()
    {
        return foodAmountForOneMeal;
    }
    public bool getIsHungry()
    {
        return isHungry;
    }

    private void setPathsVariableToCurrentLocation()
    {
        pathDrawer.DestroyPath();
        allPathCorners = new Vector3[1];
        allPathCorners[0] = transform.position;
        reachablePathCorners = new Vector3[1];
        reachablePathCorners[0] = transform.position;
        currentDestination = transform.position;
    }

    private HungerWindow lastHungerWindow;
    public override void OnEnterCommandState()
    {
        base.OnEnterCommandState();
        setPathsVariableToCurrentLocation();

        hungerCheck();

        if (lastHungerWindow != null)
            Destroy(lastHungerWindow.gameObject);

        lastHungerWindow = HungerWindow.CreateHungerWindow(this);
    }
    public override void OnExitCommandState()
    {
        firstHungerCheckThisTurn = true;
        base.OnExitCommandState();
        if (reachablePathCorners != null &&reachablePathCorners.Length != 0)
        {
            GetCellBelow(reachablePathCorners[reachablePathCorners.Length - 1]).RegisterToCell(this);
        }
        else
        {
            GetCellBelow(transform.position).RegisterToCell(this);
        }        
    }

    public override void OnEnterRotationState()
    {
        base.OnEnterRotationState();
        pathDrawer.DestroyPath();
    }

    public override void OnEvaluateBuildingAction()
    {
        base.OnEvaluateBuildingAction();
        turnsTillHunger -= 1;
        // refresh UIs showing the hunger timer
        UI.UpdateUIElements();
        if(home)
            home.GetUI().UpdateUIElements();
    }

    /// <summary>
    /// Starts a villager animation depending on the building. Expects the callback to be executed when animation complete
    /// </summary>
    /// <param name="building">Building to Work on</param>
    /// <param name="position">Where to perform animation</param>
    /// <param name="rotation">How am I rotated during the animation</param>
    /// <param name="callback">Method to execute after animation is complete</param>
    public void QueueAnimation(params AnimationItem[] animations)
    {
        if(animations == null || animations.Length < 1)
        {
            return;
        }
        busy = true;
        originalPosition = transform.position;
        originalRotation = transform.rotation;
        //queue all animations
        foreach (AnimationItem animation in animations)
        {
            AnimationQueue.Enqueue(animation);
        }
        // IF other animations have allready been started it is not nessecery to to call this
        if (!animationQueueIsRuning)
        {
            animationQueueIsRuning = true;
            StartNextAnimation();
        }
    }

    private void moveAnimationDestinationToNavmesh(float samplingRange)
    {
        UnityEngine.AI.NavMeshHit hit;
        UnityEngine.AI.NavMesh.SamplePosition(currentAnimation.position, out hit, samplingRange, UnityEngine.AI.NavMesh.AllAreas);
        animationData.finalPosition = hit.position;
        SetDestination(hit.position);
    }

    private void StartNextAnimation()
    {
        // turn the agent on but stops movment for now
        agent.enabled = true;
        stopAgent();

        animationData.lastAnimation = false;
        if (AnimationQueue.Count < 1)
        {
            animationData.lastAnimation = true;
            if (isHungry && animationData.isWalkingHomeToEat)
            {
                originalPosition = home.GetVillagerSpawnPoint().position;
                originalRotation = home.GetVillagerSpawnPoint().rotation;

            }
                AnimationQueue.Enqueue(new AnimationItem(null, "NONE", originalPosition, originalRotation, null, null));
        }
        currentAnimation = AnimationQueue.Dequeue();
        if (currentAnimation.animationName.Equals("GOHOME"))
        {
            animationData.isWalkingHomeToEat = true;
        }
        else
        {
            animationData.isWalkingHomeToEat = false;
        }

        moveAnimationDestinationToNavmesh(0.25f);
        animationData.finalRotation = currentAnimation.rotation;
        goToAnimationPlace(currentAnimation.position);
        
    }
    private void ContinueAnimation()
    {
        //then check if there is an animation
        if (currentAnimation.animationName != "NONE" && currentAnimation.animationName != "GOHOME")
        {
            animator.SetTrigger(currentAnimation.animationName);
        }
        else
        {
            // if not start next animation already
            CompletedAnimation(animationData.lastAnimation);
        }    
    }
    private void goToAnimationPlace(Vector3 finalPos)
    {
        isWalkingToAnimation = true;
        SetDestination(finalPos);
    }

    private void rotateToAnimationRot()
    {
        isRotatingToAnimation = true;
    }

    public void CompletedAnimation(bool lastAnimation)
    {
        if(currentAnimation.callback != null)
            currentAnimation.callback(this, currentAnimation.interactable);

        if(!lastAnimation)
        {
            StartNextAnimation();
        }
        else
        {
            animationQueueIsRuning = false;
            busy = false;
            stopAgent();
            agent.enabled =false;
        }
    }

    public override TypeID GetTypeID()
    {
        return TypeID.Villager;
    }

    public override bool IsBusy()
    {
        return busy;
    }
    // save some values which are used for animation
    private struct AnimationData
    {
        public bool lastAnimation;        // Checks if the Animationends
        public Quaternion finalRotation; // The Rotation which is need for the current Animation
        public Vector3 finalPosition;
        public bool isWalkingHomeToEat;
    }


    protected override void OnDestroy()
    {
        base.OnDestroy();

        if(isSelected && PlayerInput.Instance != null)
            PlayerInput.Instance.Deselect();
        
        if(home)
            home.MyVillager = null;
        Destroy(GetUI());
    }

    public override void OnHealthChanged(int delta)
    {
        base.OnHealthChanged(delta);

        if(!isAlive())
        {
            killVillager();
        }
        else
        {
            healthUI.SetDisplayedHealth(currentHealth);
        }
        
    }

    [System.Serializable]
    public struct AnimationItem
    {
        public Building building;
        public string animationName;
        public Vector3 position;
        public Quaternion rotation;
        public VillagerInteractable interactable;
        public Building.OnVillagerCompleteAction callback;

        public AnimationItem(Building building, string animationName, Vector3 position, Quaternion rotation, VillagerInteractable interactable, Building.OnVillagerCompleteAction callback)
        {
            this.building = building;
            this.animationName = animationName;
            this.position = position;
            this.rotation = rotation;
            this.interactable = interactable;
            this.callback = callback;
        }
    }


    //Serialisation
    public override void OnSave()
    {
        base.OnSave();
        GetInventory().OnSave();

        SaveData.Add("TurnsUntilHungry", turnsTillHunger);
        SaveData.Add("Name", villagerName);
        SaveData.Add("ID", id);
        SaveData.Add("Inventory", GetInventory().SaveData);

    }

    public override void LoadData()
    {
        if (SaveData == null)
            return;

        base.LoadData();

        turnsTillHunger = (int) SaveData["TurnsUntilHungry"];
        villagerName = (string) SaveData["Name"];
        id = (int)SaveData["ID"];

        GetInventory().SaveData = (JObject) SaveData["Inventory"];
        GetInventory().LoadData();
        
        TownManager.Instance.AddVillager(this);

        lockAndDeactivateAgent();
    }
}
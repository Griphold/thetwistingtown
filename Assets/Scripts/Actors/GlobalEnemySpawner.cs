﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using UnityEngine;

public class GlobalEnemySpawner : MonoBehaviour, GameStateListener, TwistingSerializable
{
    public static GlobalEnemySpawner Instance;

    private void Awake()
    {
        if (Instance != this)
        {
            Instance = this;
        }
    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }
    [SerializeField]
    private GameManager gameManager;
    
    public SpawnBehaviour spawnBehaviour;
    public List<EnemySpawner> Spawners = new List<EnemySpawner>();

    [SerializeField]
    private EnemySpawner SpawnerObject;
    [SerializeField]
    private Enemy EnemyPrefab;

    public bool PeacefulMode = false;

    private List<Enemy> allEnemies = new List<Enemy>();
    
    public int EnemyCount()
    {
        return allEnemies.Count;
    }

    public List<Enemy> GetEnemies()
    {
        return new List<Enemy>(allEnemies);
    }

    public List<Enemy> GetEnemies(Vector3 position, float radius, bool sortByDistance = true)
    {
        List<Enemy> result = GetEnemies();

        //check distance
        result.RemoveAll(x =>
        {
            float dist = Vector3.Distance(position, x.transform.position);

            return dist > radius;
        });

        if (sortByDistance)
        {
            result.Sort((a, b) =>
            {
                float aDist = Vector3.Distance(position, a.transform.position);
                float bDist = Vector3.Distance(position, b.transform.position);

                return (int)Mathf.Sign(aDist - bDist);
            });
        }

        return result;
    }

    public void SpawnEnemyAt(Vector3 position)
    {
        Enemy e = Instantiate(EnemyPrefab, position, Quaternion.identity);
        RegisterEnemy(e);
    }

    
    public void RegisterEnemy(Enemy enemy)
    {
        if (allEnemies.Contains(enemy))
            return;

        allEnemies.Add(enemy);
        gameManager.AddListener(enemy);
    }

    public void DeregisterEnemy(Enemy enemy)
    {
        allEnemies.Remove(enemy);
        gameManager.RemoveListener(enemy);
    }

    // Use this for initialization
    void Start()
    {
        if(PeacefulMode)
            Debug.Log("Peaceful mode: No enemies will spawn.");
        
        gameManager.AddListener(this);

        if(Spawners.Count <= 0)
        {
            Debug.LogError("No spawners set for enemies");
        }
    }

    public void SpawnEnemies(int count)
    {
        //choose a spawner
        EnemySpawner spawnPosition = ChooseSpawner();

        //spawn the enemies
        for (int i = 0; i < count; i++)
        {
            RegisterEnemy(spawnPosition.Spawn(EnemyPrefab));
        }
    }

    //spawn enemies at each spawner once before resetting them again
    private EnemySpawner ChooseSpawner()
    {
        List<EnemySpawner> potentialSpawns = Spawners.Where(x => !x.SpawnedLastTime).ToList();
        if(potentialSpawns.Count <= 0)
        {
            potentialSpawns = Spawners;
            foreach(EnemySpawner e in potentialSpawns)
            {
                e.SpawnedLastTime = false;
            }
        }


        int index = Random.Range(0, potentialSpawns.Count);

        EnemySpawner spawnerThisTime = potentialSpawns[index];

        spawnerThisTime.SpawnedLastTime = true;

        return spawnerThisTime;
    }

    public bool AnyEnemyBusy()
    {
        foreach (Enemy enemy in allEnemies)
        {
            if (enemy.IsBusy())
            {
                return true;
            }
        }

        return false;
    }

    public void OnEnterCommandState()
    {
    }

    public void OnExitCommandState()
    {
    }

    public void OnEvaluateBuildingAction()
    {
    }

    public void OnEvaluateEnemyAction()
    {
        if (PeacefulMode)
            return;

        spawnBehaviour.OnEnterEnemyPhase(gameManager.GetTurnNumber(), this);
    }

    public void OnEnterRotationState()
    {
    }

    public void OnExitRotationState()
    {
    }

    //Serialisation
    public JObject SaveData { get; set; }
    
    public void OnSave()
    {
        //save spawner positions here
        SaveData = new JObject();
        JArray array = new JArray();
        foreach(EnemySpawner spawner in Spawners)
        {
            spawner.OnSave();
            array.Add(spawner.SaveData);
        }

        SaveData.Add("Spawners", array);
    }

    public void LoadData()
    {
        allEnemies.Clear();

        //Delete All Spawners
        for (int i = Spawners.Count - 1; i >= 0; i--)
        {
            DestroyImmediate(Spawners[i].gameObject);
        }
        Spawners.Clear();


        //Load Saved Spawners
        JArray array = (JArray)SaveData["Spawners"];

        foreach (JObject spawnerData in array)
        {
            EnemySpawner spawner = Instantiate<EnemySpawner>(SpawnerObject);
            spawner.SaveData = spawnerData;
            spawner.LoadData();
            Spawners.Add(spawner);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "DefaultSpawnBehaviour", menuName = "TheTwistingTown/Spawn Behaviour", order = 1)]
public class SpawnBehaviour : ScriptableObject
{
    public List<Wave> waves = new List<Wave>();

    public void OnEnterEnemyPhase(int roundNumber, GlobalEnemySpawner spawner)
    {
        List<Enemy> enemiesSpawned = new List<Enemy>();
        if (roundNumber < waves[0].FirstSpawn)
        {
            return;
        }
        //spawn the first wave
        if(roundNumber == waves[0].FirstSpawn)
        {
            spawner.SpawnEnemies(waves[0].count);
        }

        //use second wave as the endlessly spawning wave
        Wave lastWave = waves[waves.Count - 1];
        if (roundNumber >= lastWave.FirstSpawn && (roundNumber - lastWave.FirstSpawn) % lastWave.TurnsBetweenSpawns == 0)
        {
            spawner.SpawnEnemies(lastWave.count);
        }
    }

    [System.Serializable]
    public class Wave
    {
        public int FirstSpawn = 20;
        public int TurnsBetweenSpawns = 4;
        public int count;
    }
}

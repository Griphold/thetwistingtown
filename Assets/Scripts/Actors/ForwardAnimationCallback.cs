﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ForwardAnimationCallback : MonoBehaviour {

    public MyEvent ForwardTo;
    public MyEvent SecondaryEvent;
    public MyEvent Event3;
    public MyEvent TheFourthEventuality;

    [System.Serializable]
    public class MyEvent : UnityEvent { }

    public void OnEvent()
    {
        ForwardTo.Invoke();
    }

    public void OnSecondaryEvent()
    {
        SecondaryEvent.Invoke();
    }

    public void OnEvent3()
    {
        Event3.Invoke();
    }

    public void OnTheFourthEventuality()
    {
        TheFourthEventuality.Invoke();
    }
}

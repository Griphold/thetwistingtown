﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PathDrawer : MonoBehaviour {

    public GameObject pathPrefab;
    public Material canReach;
    public Material canNotReach;

    // The List of elements on the complete and the reachable path
    //private List<GameObject> pathParts;     // All object off the path 
    //private List<GameObject> activePathParts; // Only the reachable parts

    private Vector3[] allCurrentNavPathCorners;
    //private Vector3[] reachableNavPathCorners;
    private Vector3 currentDestination; // is Vector.zero if there is no current destination


    public LineRenderer lineRendererGreen;
    public LineRenderer lineRendererRed;

    public float MaxLengthRedPart = 10;

    // Use this for initialization
    void Start() {
        /*pathParts = new List<GameObject>();
        activePathParts = new List<GameObject>();*/
    }

    // PathCorners does not include the point destinaionthisTurn
    public void DrawPath(Vector3[] pathCorners, Vector3 destinationThisTurn, Vector3[] reachableCorners)
    {
        if (pathCorners == null || reachableCorners == null || pathCorners.Length == 0)
            return;

        // Set class variables
        allCurrentNavPathCorners = pathCorners;
        currentDestination = destinationThisTurn;
        //reachableNavPathCorners = reachableCorners;

        // draw Green Line
        subdivideAndRenderLine(lineRendererGreen, revert(reachableCorners), 2);
        
        // Remove Redline if the destination is reachable
        if (pathCorners.Length == reachableCorners.Length&& Vector2.Distance(new Vector2(destinationThisTurn.x,destinationThisTurn.z),
           new Vector2 (pathCorners[pathCorners.Length-1].x, pathCorners[pathCorners.Length - 1].z)) <0.01f)
        {
            lineRendererRed.positionCount = 0;
            return;
        }
        // draw Red Line

        if (allCurrentNavPathCorners.Length == 0)
        {
            return;
        }
        // Saves the Corners of the unreachableLinepart
        List<Vector3> redCorners = new List<Vector3>();
        // The first part of the redline is the last part of the greenline
        redCorners.Add(reachableCorners[reachableCorners.Length - 1]);
        // The distance is counted to check how far the line should go
        float distanceCount = 0;
        for (int i = 1; i < allCurrentNavPathCorners.Length - reachableCorners.Length + 2; i++)
        {
            Vector3 currentCorner = allCurrentNavPathCorners[reachableCorners.Length + i - 2];
            Vector3 lastCorner = redCorners[redCorners.Count - 1];
            float distanceTemp = distanceCount +Vector2.Distance(new Vector2(currentCorner.x, currentCorner.z), new Vector2(lastCorner.x, lastCorner.z));
            // Check if the distance to the current corner is allready to far
            if (distanceTemp <= MaxLengthRedPart)
            {
                // if not add the corner and increase the distanceCount;
                redCorners.Add(currentCorner);
                distanceCount = distanceTemp;
            }
            else
            {
                float remaningLength = MaxLengthRedPart - distanceCount;
                Vector3 seperationPoint = lastCorner + Vector3.Normalize(currentCorner - lastCorner) * remaningLength;
                redCorners.Add(seperationPoint);
                subdivideAndRenderLine(lineRendererRed, redCorners.ToArray(), 2);
                return;

            }
        }
    subdivideAndRenderLine(lineRendererRed, redCorners.ToArray(), 2);
    }
    private Vector3[] revert(Vector3[] vec)
    {
        Vector3[] result = new Vector3[vec.Length];
        for (int i = 0; i < vec.Length; i++)
        {
            result[i] = vec[vec.Length - 1 - i];
        }
        return result;

    }

    private void subdivideAndRenderLine(LineRenderer lineRenderer, Vector3[] corners, float divideDistance)
    {
        List<Vector3> list = new List<Vector3>();
        for (int i = 0; i < corners.Length-1; i++)
        {
            list.Add(corners[i]);
            float distance = Vector3.Distance(corners[i],corners[i+1]);
            // If the distance between the two corners is to small, so further points are created
            if (distance > divideDistance)
            {
                // Otherwise we check how many now corners we want to draw.
                int additaionalPoints = (int)(distance / divideDistance);
                // To avoid one part of the Line which is smaller then the rest do not use divideDistance
                float distanceOfOnePoint = distance / (additaionalPoints + 1);
                Vector3 distanceToNextPoint = Vector3.Normalize((corners[i + 1] - corners[i])) * distanceOfOnePoint;
                for (int j = 0; j < additaionalPoints; j++)
                {
                    list.Add(getClosestPointOnNavMesh(corners[i] + (j+1) * distanceToNextPoint));
                }

            }
        }
        // The last point has to be added manually
        list.Add(corners[corners.Length - 1]);
        lineRenderer.positionCount = list.Count;
        lineRenderer.SetPositions(list.ToArray());
    }


    private Vector3 getClosestPointOnNavMesh(Vector3 point)
    {
        UnityEngine.AI.NavMeshHit hit;
        if (!UnityEngine.AI.NavMesh.SamplePosition(point, out hit, 0.5f, NavMesh.AllAreas))
        {
            UnityEngine.AI.NavMesh.SamplePosition(point, out hit, 1, NavMesh.AllAreas);
        }

        return hit.position;
    }

    public void UpdateDrawPath(Vector3[] pathCorners, Vector3[] reachablePathCorners)
    {
        DrawPath(pathCorners, currentDestination,reachablePathCorners);
    }
    

    public void hidePath()
    {
        lineRendererGreen.enabled = false;
        lineRendererRed.enabled = false;
    }
    public void unHidePath()
    {
        lineRendererGreen.enabled = true;
        lineRendererRed.enabled = true;
    }

    public void DestroyPath()
    {
        lineRendererGreen.positionCount = 0;
        lineRendererRed.positionCount = 0;
    }


}

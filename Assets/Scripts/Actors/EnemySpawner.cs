﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

public class EnemySpawner : MonoBehaviour, TwistingSerializable {
    public bool SpawnedLastTime = false;
    public float SpawnRange = 1.5f;

    public Enemy Spawn(Enemy enemyPrefab)
    {
        Vector2 randomModifier = Random.insideUnitCircle * SpawnRange;

        return Instantiate(enemyPrefab, transform.position + new Vector3(randomModifier.x, 0, randomModifier.y), Quaternion.identity);
    }


    public JObject SaveData { get; set; }
    
    public void OnSave()
    {
        SaveData = new JObject();
        SaveData.Add("Position", JsonConvert.SerializeObject(new Vector3JSon(transform.position)));
        SaveData.Add("SpawnedLastTime", SpawnedLastTime);
        SaveData.Add("SpawnRange", SpawnRange);
    }

    public void LoadData()
    {
        if (SaveData == null)
            return;

        Vector3JSon positionJson = JsonConvert.DeserializeObject<Vector3JSon>((string)SaveData["Position"]);
        transform.position = positionJson.GetVector3();

        SpawnedLastTime = (bool)SaveData["SpawnedLastTime"];
        SpawnRange = (float)SaveData["SpawnRange"];
    }
}

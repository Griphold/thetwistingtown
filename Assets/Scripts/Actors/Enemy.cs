﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : NavigatableActor
{
    public delegate void OnEnemyCompleteAction(Enemy enemy);

    public enum Priority
    {
        BUILDINGS, VILLAGERS, ANYTHING
    }

    [Header("Attack Behaviour")]
    public float AttackRange = 1f;
    public int AttackDamage = 1;
    public float BuildingsSeekRange = 50;
    public float VillagerSeekRange = 50;
    public List<Priority> priorities = new List<Priority>();

    [Header("Effects & Animation")]
    public GameObject DeathEffect;

    private Dictionary<Priority, Seeker<Enemy>> seekers = new Dictionary<Priority, Seeker<Enemy>>();

    private bool hasACommand;
    private bool isAnimating;
    private bool isRotatingToTarget;
    private Attackable target;
    private Quaternion targetRotation;

    //animation stuff
    private OnEnemyCompleteAction currentAnimationCallback;

    public override void Start()
    {
        base.Start();

        seekers.Add(Priority.BUILDINGS, new ClosestBuildingSeeker(this));
        seekers.Add(Priority.VILLAGERS, new ClosestVillagerSeeker(this));
        seekers.Add(Priority.ANYTHING, new ClosestAnythingSeeker(this));

        GlobalEnemySpawner.Instance.RegisterEnemy(this);
    }

    public void Update()
    {
        if (hasACommand && isAlive() && agent.enabled)
        {
            UpdateEnergy();
             //todo hacky as fuck, find out why try reach destination isnt working as intended
           
            if (CurrentEnergy <= 0)
            {
                stopAndDisableAgent();
                return;
            }

            // Set Current Energy in Animator
            // replaced with CurrentEnergy prop
            // animator.SetFloat("Energy", CurrentEnergy / MaxEnergy);

            //Lower Walking Speed Based on Energy
            agent.speed = Mathf.Lerp(WalkSpeedTired, WalkSpeed, CurrentEnergy / MaxEnergy);

            //Set Current movement Speed in Animator
            animator.SetFloat("Speed", agent.velocity.magnitude);

            //target was already destroyed
            if(target.gameObject() == null)
            {
                target = null;

                stopAndDisableAgent();

                return;
            }

            if(IsTargetInAttackRange())
            {
                RotateToTarget();
                
                stopAndDisableAgent();

                return;
            }
            

            if (isCloseToDestination())
            {
                stopAndDisableAgent();
                
                return;
            }
        }

        if(isRotatingToTarget)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 5f * Time.deltaTime);

            if(Quaternion.Angle(transform.rotation, targetRotation) < 1f)
            {
                isRotatingToTarget = false;
                
                PlayAnimation("Chop_Enemy", null);
            }
        }
    }

    private void UpdateEnergy()
    {
        float speed = agent.velocity.magnitude;

        CurrentEnergy -= speed * Time.deltaTime;

        if (CurrentEnergy < 0)
        {
            CurrentEnergy = 0;
        }
    }

    private void RotateToTarget()
    {
        if(target == null)
        {
            isRotatingToTarget = false;
            return;
        }

        isRotatingToTarget = true;
        Vector3 targetPos = target.gameObject().transform.position;
        Vector3 myPos = transform.position;

        Vector3 targetPosXZ = new Vector3(targetPos.x, 0, targetPos.z);
        Vector3 myPosXZ = new Vector3(myPos.x, 0, myPos.z);
        Vector3 direction = targetPosXZ - myPosXZ;

        targetRotation = Quaternion.LookRotation(direction, Vector3.up);
    }

    public override void OnEnterRotationState()
    {
        lockAndDeactivateAgent();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        if (GlobalEnemySpawner.Instance)
            GlobalEnemySpawner.Instance.DeregisterEnemy(this);
    }

    public override bool IsBusy()
    {
        return hasACommand || isAnimating || isRotatingToTarget;
    }

    private void SetTarget(Attackable target)
    {
        this.target = target;
    }

    private void GoToTarget()
    {
        if (target == null) return;

        GameObject targetGameObject = target.gameObject();

        int reachedCorners = 0;
        NavMeshPath myPath = new NavMeshPath();
        Vector3 destination = TryReachDestination(targetGameObject.transform.position, out reachedCorners, out myPath);
        NavigateTo(destination);
    }


    private void stopAndDisableAgent()
    {
        agent.SetDestination(transform.position);
        agent.enabled = false;
        animator.SetFloat("Speed", 0);
        hasACommand = false;
    }

    public override void OnEvaluateEnemyAction()
    {
        if (!isAlive())
            return;

        Attackable attackable = SeekNewTarget();
        SetTarget(attackable);
        
        GoToTarget();

        hasACommand = true;
    }

    private Attackable SeekNewTarget()
    {
        foreach (Priority p in priorities)
        {
            Attackable potentialTarget = SeekTargetWithPriority(p);

            if (potentialTarget != null)
                return potentialTarget;
        }

        Debug.Log("No attackables in seek range found");

        return null;
    }

    private Attackable SeekTargetWithPriority(Priority priority)
    {
        Seeker<Enemy> value;
        if (seekers.TryGetValue(priority, out value))
        {
            return value.Seek();
        }

        Debug.LogError("No seeker found for priority " + priority);
        return null;
    }

    /*private bool IsCloseToDestination(float delta)
    {
        //todo calculate in board space
        Vector3 destinationXZ = new Vector3(agent.destination.x, 0, agent.destination.z);
        Vector3 myPositionXZ = new Vector3(agent.transform.position.x, 0, agent.transform.position.z);

        return Vector3.Distance(destinationXZ, myPositionXZ) <= delta;
    }*/

    private bool IsTargetInAttackRange()
    {
        if (target == null)
            return true;

        Transform targetTransform = target.gameObject().transform;
        Vector3 destinationXZ = new Vector3(targetTransform.position.x, 0, targetTransform.position.z);
        Vector3 myPositionXZ = new Vector3(agent.transform.position.x, 0, agent.transform.position.z);

        return Vector3.Distance(destinationXZ, myPositionXZ) <= AttackRange;
    }

    public override void OnHealthChanged(int delta)
    {
        base.OnHealthChanged(delta);

        if (!isAlive())
        {
            PlayAnimation("Die", OnFinishedDeathAnimation);
        }
    }

    //animation stuff
    private void PlayAnimation(string animationName, OnEnemyCompleteAction callback)
    {
        animator.SetTrigger(animationName);
        currentAnimationCallback = callback;
        isAnimating = true;
    }
    
    public void OnCompletedAnimation()
    {
        if(currentAnimationCallback != null)
        {
            currentAnimationCallback(this);
            currentAnimationCallback = null;
        }

        isAnimating = false;
    }

    public void OnDuringAttackAnimation()
    {
        if (target == null || target.gameObject() == null)
            return;

        bool targetDestroyed = target.Attack(AttackDamage);

        if (targetDestroyed)
            target = null;
    }

    private void OnFinishedDeathAnimation(Enemy enemy)
    {
        Destroy(enemy.gameObject);
    }
    
    public void SpawnDeathEffect()
    {
        Instantiate(DeathEffect, transform.position, DeathEffect.transform.rotation, null);
    }

    public override TypeID GetTypeID()
    {
        return TypeID.Enemy;
    }

    //Serialisation
    public override void OnSave()
    {
        base.OnSave();
    }

    public override void LoadData()
    {
        if (SaveData == null)
            return;

        base.LoadData();

        lockToCell(transform.position);
    }

    public abstract class Seeker<T>
    {
        protected T mySeeker;

        public Seeker(T t)
        {
            mySeeker = t;
        }

        public abstract Attackable Seek();
    }

    private class ClosestBuildingSeeker : Seeker<Enemy>
    {
        public ClosestBuildingSeeker(Enemy t) : base(t)
        {
        }

        public override Attackable Seek()
        {
            List<Building> buildingsInRange = TownManager.Instance.GetBuildings<Building>(mySeeker.transform.position, mySeeker.BuildingsSeekRange);

            //ignore build orders and town center
            buildingsInRange.RemoveAll(x => { return x.Type().Equals(Building.TypeID.BuildOrder) || x.Type().Equals(Building.TypeID.TownCenter); });

            if (buildingsInRange.Count > 0)
            {
                return buildingsInRange[0];
            }
            else
            {
                Debug.Log("Enemy did not find any buildings in range to attack");
                return null;
            }

        }
    }

    private class ClosestVillagerSeeker : Seeker<Enemy>
    {
        public ClosestVillagerSeeker(Enemy t) : base(t)
        {
        }

        public override Attackable Seek()
        {
            List<Villager> villagersInRange = TownManager.Instance.GetVillagers(mySeeker.transform.position, mySeeker.VillagerSeekRange);
            if (villagersInRange.Count > 0)
            {
                return villagersInRange[0];
            }
            else
            {
                Debug.Log("Enemy did not find any villagers in range to attack");
                return null;
            }

        }
    }

    private class ClosestAnythingSeeker : Seeker<Enemy>
    {
        private ClosestBuildingSeeker b;
        private ClosestVillagerSeeker v;

        public ClosestAnythingSeeker(Enemy t) : base(t)
        {
            b = new ClosestBuildingSeeker(t);
            v = new ClosestVillagerSeeker(t);
        }

        public override Attackable Seek()
        {
            Attackable closestBuilding = b.Seek();
            Attackable closestVillager = v.Seek();

            if (closestBuilding == null)
            {
                return closestVillager;
            }
            else if (closestVillager == null)
            {
                return closestBuilding;
            }
            else
            {
                float buildingDistance = Vector3.Distance(closestBuilding.gameObject().transform.position, mySeeker.transform.position);
                float villagerDistance = Vector3.Distance(closestVillager.gameObject().transform.position, mySeeker.transform.position);

                if (buildingDistance < villagerDistance)
                {
                    return closestBuilding;
                }
                else
                {
                    return closestVillager;
                }
            }
        }
    }
}
